﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%
	Response.Expires = 0
	'Response.ContentType = "text/xml"
	Response.Charset="utf-8"
	
	RequestUrl = Request.QueryString("RequestUrl")
	if RequestUrl = "" then Response.End()
	IsEncode = Request.QueryString("IsEncode")
	if IsEncode = "" then IsEncode = True
		
	Session.CodePage=65001
	if IsEncode = True then
		Response.Write Server.URLEncode(GetHttpPage(RequestUrl))
	else
		Response.Write GetHttpPage(RequestUrl)
	end if
	Session.CodePage=936
	
	Function GetHttpPage(HttpUrl)
	   On Error Resume Next
		
	   If IsNull(HttpUrl)=True Or HttpUrl="$False$" Then
		  GetHttpPage="$False$"
		  Exit Function
	   End If
	   
	   Dim Http
	   Set Http=Server.CreateObject("MSXML2.ServerXMLHTTP")
	   Http.setTimeouts 3000,3000,3000,3000
	   Http.open "GET",HttpUrl,False
	   Http.Send(NULL)
	   If Http.Readystate<>4 then
		  Set Http=Nothing 
		  GetHttpPage="$False$"
		  Exit function
	   End if

	   'GetHTTPPage=Http.responseText
	   GetHTTPPage=bytesToBSTR2(Http.responseBody,"utf-8")
	   Set Http=Nothing
	   If Err.number<>0 then	  
		  Err.Clear
	   End If
	End Function
	
	Function bytesToBSTR2(strBody,CodeBase)
		dim obj
		set obj = Server.CreateObject("Adodb.Stream")
		obj.type = 1
		obj.mode = 3
		obj.open
		obj.write strBody
		obj.Position = 0
		obj.type = 2
		obj.charset = CodeBase
		bytesToBSTR2 = obj.ReadText 
		obj.close
		set obj = nothing
	End Function	
%>