﻿<?php	
	header("Content-type: text/xml");
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
  header("Last-Modified:".gmdate("D,dMYH:i:s")."GMT" ); 
  header("Cache-Control:no-store,no-cache,must-revalidate");
  header("Cache-Control:post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
	
	$RequestUrl = $_GET['RequestUrl'];
	if($RequestUrl==""){exit;}
	
	$RequestUrl = str_replace('&amp;','&',$RequestUrl);

  if (isset($_GET['IsEncode'])) {
    $IsEncode = $_GET['IsEncode'];
  } 
  else{
    $IsEncode = "";
  }	
	if($IsEncode==""){$IsEncode=true;}
		
  if (isset($_POST['para'])) {
    $para = $_POST['para'];
  } 
  else{
    $para = "";
  }
 
  if($para ==""){ 
	  $c=file_get_contents($RequestUrl); 
  }
  else{
    $context = array();
    $context['http'] = array  
    (  
       'method' => 'POST',  
       'header'=> 'Content-type: application/x-www-form-urlencoded',
       'content' => $para,  
    );  
    $c=file_get_contents($RequestUrl, false, stream_context_create($context));
  }
  
	if($IsEncode==true){
		echo urlencode($c);	
	}
	else{
		echo $c;	
	}	 	
?>