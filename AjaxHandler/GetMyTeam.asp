﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file = "function.asp"-->
<!--#include file = "cmd.asp"-->
<%
	Response.Expires = -1
	Response.ContentType = "text/xml"
	Response.Charset="utf-8"
	Session.CodePage=65001
	Response.Write("<?xml version=""1.0"" encoding=""utf-8""?>")
	Response.Write("<list>")
	Call DataBegin()
	uid = GetSafeStr(Request("uid"))
	sql = "select A.IsBoss,A.IsAdmin,A.IsBlocked,B.TeamID,B.TeamName,B.TeamIcon,B.TeamDescript from tbTeamMember A " &_
		  "inner join tbTeam B on A.TeamID = B.TeamID " &_
		  "where B.IsMini<>1 and A.UserID = " & uid
	oRs.Open sql,oConn,1,1
	If Not(oRs.Bof And oRs.Eof) Then
		oRs.MoveFirst
		While Not oRs.Eof
			Response.Write("<item>")
			Call OutNode("TeamID",oRs("TeamID"))
			Call OutNode("TeamName",oRs("TeamName"))
			Call OutNode("TeamIcon",oRs("TeamIcon"))
			Call OutNode("TeamDescript",oRs("TeamDescript"))
			Call OutNode("IsBoss",oRs("IsBoss"))
			Call OutNode("IsAdmin",oRs("IsAdmin"))
			Call OutNode("IsBlocked",oRs("IsBlocked"))
			Response.Write("</item>")
			oRs.MoveNext
		Wend
	else
		Response.Write("<item>")
		Call OutNode("TeamID","-1")
		Call OutNode("TeamName","")
		Call OutNode("TeamIcon","")
		Call OutNode("TeamDescript","")
		Call OutNode("IsBoss","")
		Call OutNode("IsAdmin","")
		Call OutNode("IsBlocked","")
		Response.Write("</item>")	
	End If
	oRs.Close()
	Set oRs = Nothing
	Call DataEnd()
	Response.Write("</list>")
%>