﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file = "function.asp"-->
<!--#include file = "cmd.asp"-->
<%
	Session.CodePage = 65001
	Response.Expires = -1
	Response.ContentType = "text/xml"
	Response.Charset="utf-8"
	Response.Write("<?xml version=""1.0"" encoding=""utf-8""?>")
	Response.Write("<list>")
	Call DataBegin()	
	dim TeamID 
	TeamID = Request.QueryString("tid")	
	'Call CheckTeamMember(Session("UserID"),TeamID) '只允许群内成员查询
	sql = "select A.TeamID,A.TeamNick,A.TeamUserStatus,A.IsBoss,IsAdmin,B.UserID,B.UserNick,B.UserStatus from tbTeamMember A " &_
		  "inner join tbUsers B on A.UserID = B.UserID " &_
		  "where A.TeamID='" & TeamID & "'"
	oRs.Open sql,oConn,1,1
	If Not(oRs.Bof And oRs.Eof) Then
		oRs.MoveFirst
		While Not oRs.Eof
			Response.Write("<item>")
			Call OutNode("TeamID",oRs("TeamID"))
			Call OutNode("IsBoss",oRs("IsBoss"))
			Call OutNode("IsAdmin",oRs("IsAdmin"))
			Call OutNode("UserID",oRs("UserID"))
			Call OutNode("UserNick",oRs("TeamNick"))
			Call OutNode("UserStatus",oRs("TeamUserStatus"))
			Response.Write("</item>")
			oRs.MoveNext
		Wend
	else
		Response.Write("<item>")
		Call OutNode("TeamID","-1")
		Call OutNode("IsBoss","")
		Call OutNode("IsAdmin","")
		Call OutNode("UserID","")
		Call OutNode("UserNick","")
		Call OutNode("UserStatus","")
		Response.Write("</item>")			
	End If
	oRs.Close()
	Set oRs = Nothing
	Call DataEnd()
	Response.Write("</list>")
%>