﻿using System;
using System.Collections.Generic;
using System.Web;
using WebIM.Web.Chat.Channels;
using MethodWorx.AspNetComet.Core;

namespace WebIM.Web.Ashx
{
    /// <summary>
    /// Summary description for ChatRoom
    /// </summary>
    public class ChatRoom : IHttpHandler
    {
        private string jsonStr = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "Application/json";
            context.Response.Charset = "utf-8";

            string op = context.Request.Form["op"].ToString().Trim();

            switch (op)
            {
                case "GetInChatRoom":
                    GetInChatRoom(context);
                    break;
                case "KillCometClient":
                    KillCometClient(context);
                    break;
                default:
                    jsonStr = "{\"rst\":\"-2001\"}";
                    break;
            }
            context.Response.Write(jsonStr);
        }

        private void GetInChatRoom(HttpContext context)
        {
            string usernick = context.Request.Form["usernick"].ToString();

            try
            {
                if (String.IsNullOrEmpty(usernick.Trim()))
                {
                    jsonStr = "{\"rst\":\"-1\",\"msg\":\"昵称不能为空\"}";
                }
                else
                {
                    DefaultChannelHandler.StateManager.InitializeClient(usernick, usernick, usernick, 10, 5);
                    jsonStr = "{\"rst\":\"0\"}";
                }
            }
            catch (CometException ce)
            {
                if (ce.MessageId == CometException.CometClientAlreadyExists)
                {
                    //  ok the comet client already exists, so we should really show
                    //  an error message to the user
                    jsonStr = "{\"rst\":\"-2\",\"msg\":\"昵称已被占用，请更换\"}";
                }
            }
        }

        private void KillCometClient(HttpContext context)
        {
            string usernick = context.Request["usernick"].ToString();

            try
            {
                if (String.IsNullOrEmpty(usernick.Trim()))
                {
                    jsonStr = "{\"rst\":\"-1\",\"msg\":\"关闭\"}";
                }
                else
                {
                    DefaultChannelHandler.StateManager.KillIdleCometClient(usernick);
                    jsonStr = "{\"rst\":\"0\"}";
                }
            }
            catch (CometException ce)
            {
                if (ce.MessageId == CometException.CometClientDoesNotExist)
                {
                    //  ok the comet client already exists, so we should really show
                    //  an error message to the user
                    jsonStr = "{\"rst\":\"-2\",\"msg\":\"用户不存在\"}";
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}