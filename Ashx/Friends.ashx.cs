﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using WebIM.Common;

namespace WebIM.Web.Ashx
{
    /// <summary>
    /// Summary description for Friends
    /// </summary>
    public class Friends : IHttpHandler
    {
        private string jsonStr = "";

        public void ProcessRequest(HttpContext context)
        {
           context.Response.ContentType = "Application/json";
            context.Response.Charset = "utf-8";

            string op = context.Request.Form["op"].ToString().Trim();

            switch (op)
            {
                case "GetOnlineUsers":
                    GetOnlineUsers(context);
                    break;
                case "GetUser":
                    GetUser(context);
                    break;
                default:
                    jsonStr = "{\"rst\":\"-2001\"}";
                    break;
            }
            context.Response.Write(jsonStr);
        }

        private void GetOnlineUsers(HttpContext context)
        {
            int pagesize = int.Parse(context.Request.Form["pagesize"]);
            int pageindex = int.Parse(context.Request.Form["pageindex"]);
            string strGender = context.Request.Form["strGender"].ToString();
            string strAge = context.Request.Form["strAge"].ToString();
            string strCountry = context.Request.Form["strCountry"].ToString();
            string strCity = context.Request.Form["strCity"].ToString();
            string strRegion = context.Request.Form["strRegion"].ToString();
            int count;

            DataTable dt = BLL.IM.GetOnlineUsers(pagesize, pageindex, strGender, strAge, strCountry, strCity, strRegion, out count).Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["nick"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["nick"].ToString());
                dt.Rows[i]["signature"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["signature"].ToString());
                dt.Rows[i]["city"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["city"].ToString());
                dt.Rows[i]["region"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["region"].ToString());
                dt.Rows[i]["province"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["province"].ToString());
            }

            if (dt.Rows.Count > 0)
            {
                jsonStr = Function.Instance.DtToJSON(dt);
                jsonStr = jsonStr.Remove(jsonStr.Length - 1, 1) + ",\"recordcount\":\"" + count + "\"}";
            }
            else
            {
                jsonStr = "{\"recordcount\":\"" + count + "\"}";
            }
        }

        private void GetUser(HttpContext context)
        {
            string strAccount = context.Request.Form["strAccount"].ToString();
            string strName = context.Request.Form["strName"].ToString();
            DataTable dt = BLL.IM.GetUsers(strAccount, strName).Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["nick"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["nick"].ToString());
                dt.Rows[i]["signature"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["signature"].ToString());
                dt.Rows[i]["city"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["city"].ToString());
                dt.Rows[i]["region"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["region"].ToString());
                dt.Rows[i]["province"] = Function.Instance.MySqlDBEncode(dt.Rows[i]["province"].ToString());
            }
            if (dt.Rows.Count> 0)
            {
                jsonStr = Function.Instance.DtToJSON(dt);
                jsonStr = jsonStr.Remove(jsonStr.Length - 1, 1) + ",\"recordcount\":\"" + dt.Rows.Count + "\"}";
            }
            else
            {
                jsonStr = "{\"recordcount\":\"0\"}";
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}