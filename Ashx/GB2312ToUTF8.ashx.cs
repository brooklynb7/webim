﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebIM.Web.Ashx
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    public class GB2312ToUTF8 : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Expires = 0;
            context.Response.ContentType = "text/xml";
            context.Response.Charset = "UTF-8";
            context.Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            context.Response.Write("<list>");
            context.Response.Write("<item>");

            context.Session.CodePage = 936;
            string AString = context.Request.QueryString["AString"].ToString();

            context.Session.CodePage=65001;
            string encodeAString = context.Server.HtmlEncode(AString);
            context.Response.Write("<s>");
            context.Response.Write(encodeAString);
            context.Response.Write("</s>");
            context.Response.Write("</item>");
            context.Response.Write("</list>");
            //Response.BinaryWrite encodeAString
            context.Session.CodePage = 936;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
