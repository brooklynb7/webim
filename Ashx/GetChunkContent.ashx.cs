﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Net;

namespace WebIM.Web.Ashx
{
    /// <summary>
    /// Summary description for GetChunkContent
    /// </summary>
    public class GetChunkContent : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Expires = -1;
            context.Response.ContentType = "text/xml";
            context.Response.Charset = "utf-8";

            string url = context.Request["RequestUrl"].ToString();
            System.Net.WebRequest request = System.Net.WebRequest.Create(url);
          
            if (context.Request.Form["para"]!=null)
            {
                byte[] data = Encoding.UTF8.GetBytes(context.Request["para"].ToString());
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                Stream newStream = request.GetRequestStream();
                // 发送数据 
                newStream.Write(data, 0, data.Length);
                newStream.Close(); 
            }
            try
            {
                System.Net.WebResponse response = request.GetResponse();
                Stream respStream = response.GetResponseStream();

                byte[] bts = new Byte[5000];
                respStream.Read(bts, 0, bts.Length);
                string rststring = System.Text.Encoding.GetEncoding("utf-8").GetString(bts, 0, bts.Length).Trim().Replace("\0", "");
                context.Response.Write(rststring);
            }
            catch
            {
                context.Response.Write("False");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}