﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebIM.Web.Ashx
{
    /// <summary>
    /// 后台处理在线用户数获取
    /// </summary>
    public class GetOnlineCount : IHttpHandler
    {
        public void ProcessRequest(HttpContext context) 
        {
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            context.Response.ContentType = "text/plain";

            context.Response.Write(BLL.IM.GetOnlineCount().ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
