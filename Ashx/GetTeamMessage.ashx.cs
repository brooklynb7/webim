﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

namespace WebIM.Web.Ashx
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    public class GetTeamMessage : IHttpHandler
    {
        private string xmlStr = "";
        private int count = 0;
        private string teamname = "";

        public void ProcessRequest(HttpContext context)
        {          
            context.Response.Expires = -1;
            context.Response.ContentType = "text/xml";
            context.Response.Charset = "utf-8";
            xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><list>";

            string op = context.Request["op"].ToString().Trim();

            switch (op)
            {
                case "GetMiniTeamChatMessage":
                    GetMiniTeamChatMessage(context);//获取MINI聊天室消息
                    break;
                case "GetMiniTeamChatNewMessage":
                    GetMiniTeamChatNewMessage(context);//获取MINI聊天室最新消息
                    break;
                case "AddUserIntoMiniTeamChat":
                    AddUserIntoMiniTeamChat(context);//用户进入MINI聊天室
                    break;
                case "AddGuestIntoMiniTeamChat":
                    AddGuestIntoMiniTeamChat(context);//游客进入MINI聊天室
                    break;
                case "GetMiniTeamMember":
                    GetMiniTeamMember(context);//获取MINI聊天室成员
                    break;
                case "AddMiniTeamMesssage":
                    AddMiniTeamMesssage(context);//添加MINI聊天室聊天记录
                    break;
                default:
                    xmlStr = "";
                    break;
            }
            xmlStr += "</list>";
            context.Response.Write(xmlStr);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void GetMiniTeamChatMessage(HttpContext context)
        {
            int tid = Convert.ToInt32(context.Request["tid"].ToString());

            DataView dv = BLL.TeamMessage.GetMiniTeamMessageByTeamID(tid);

            for (int i = (dv.Count - 1); i >= 0; i--)
            {
                xmlStr += "<item>";
                xmlStr += "<From>" + dv[i]["FromId"].ToString() + "</From>";
                xmlStr += "<FromNick>" + dv[i]["UserNick"].ToString() + "</FromNick>";
                xmlStr += "<To>" + dv[i]["ToID"].ToString() + "</To>";
                xmlStr += "<Content>" + dv[i]["MsgContent"].ToString() + "</Content>";
                xmlStr += "<Type>" + dv[i]["TypeID"].ToString() + "</Type>";
                xmlStr += "<IsConfirm>" + dv[i]["IsConfirm"].ToString() + "</IsConfirm>";
                xmlStr += "<AddTime>" + Convert.ToDateTime(dv[i]["MsgAddTime"]).ToString("HH:mm") + "</AddTime>";
                xmlStr += "</item>";
            }
            string lastmid = "0";
            if (dv.Count > 0)
            {
                lastmid = dv[0]["MID"].ToString();
            }
            xmlStr += "<lastMID>" + lastmid.ToString() + "</lastMID>";
        }
        

        private void GetMiniTeamChatNewMessage(HttpContext context)
        {
            int tid = Convert.ToInt32(context.Request["tid"].ToString());
            int lmid = Convert.ToInt32(context.Request["lastmid"].ToString());

            DataView dv = BLL.TeamMessage.GetMiniTeamNewMessage(tid, lmid);

            for (int i = (dv.Count - 1); i >= 0; i--)
            {
                xmlStr += "<item>";
                xmlStr += "<From>" + dv[i]["FromId"].ToString() + "</From>";
                xmlStr += "<FromNick>" + dv[i]["UserNick"].ToString() + "</FromNick>";
                xmlStr += "<To>" + dv[i]["ToID"].ToString() + "</To>";
                xmlStr += "<Content>" + dv[i]["MsgContent"].ToString() + "</Content>";
                xmlStr += "<Type>" + dv[i]["TypeID"].ToString() + "</Type>";
                xmlStr += "<IsConfirm>" + dv[i]["IsConfirm"].ToString() + "</IsConfirm>";
                xmlStr += "<AddTime>" + Convert.ToDateTime(dv[i]["MsgAddTime"]).ToString("HH:mm") + "</AddTime>";
                xmlStr += "</item>";
            }
            string lastmid = lmid.ToString();
            if (dv.Count > 0)
            {
                lastmid = dv[0]["MID"].ToString();
            }
            xmlStr += "<lastMID>" + lastmid.ToString() + "</lastMID>";
        }

        private void AddUserIntoMiniTeamChat(HttpContext context)
        {
            int tid = Convert.ToInt32(context.Request.Form["tid"].ToString());
            int userid = Convert.ToInt32(context.Request.Form["userid"].ToString());
            string nick = context.Request.Form["nick"].ToString();
            BLL.TeamMessage.AddUserIntoMiniTeamChat(tid, userid, nick,out teamname);
            xmlStr += "<teamname>" + teamname.ToString() + "</teamname>";
        }

        private void AddGuestIntoMiniTeamChat(HttpContext context)
        {
            int tid = Convert.ToInt32(context.Request.Form["tid"].ToString());
            xmlStr += "<guestid>" + BLL.TeamMessage.AddGuestIntoMiniTeamChat(tid, out teamname).ToString() + "</guestid><teamname>" + teamname.ToString() + "</teamname>";
        }

        private void GetMiniTeamMember(HttpContext context)
        {
            int tid = Convert.ToInt32(context.Request.QueryString["tid"].ToString());
            int userid = Convert.ToInt32(context.Request.QueryString["userid"].ToString());
            int gcount, mcount;
            DataView dv = BLL.TeamMessage.GetMiniTeamMemberByTeamID(tid, userid, out mcount, out gcount, out count);

            for (int i = 0; i < dv.Count; i++)
            {
                xmlStr += "<item>";
                xmlStr += "<UserID>" + dv[i]["UserID"].ToString() + "</UserID>";
                xmlStr += "<TeamNick>" + dv[i]["TeamNick"].ToString() + "</TeamNick>";
                xmlStr += "<TeamGender>" + dv[i]["TeamGender"].ToString() + "</TeamGender>";
                xmlStr += "<IsGuest>" + dv[i]["IsGuest"].ToString() + "</IsGuest>";
                xmlStr += "</item>";
            }
            xmlStr += "<gcount>" + gcount.ToString() + "</gcount>";
            xmlStr += "<mcount>" + mcount.ToString() + "</mcount>";
            xmlStr += "<count>" + count.ToString() + "</count>";
        }

        private void AddMiniTeamMesssage(HttpContext context)
        {
            int from = Convert.ToInt32(context.Request.Form["fromid"].ToString());
            int toid = Convert.ToInt32(context.Request.Form["toid"].ToString());
            int typeid = Convert.ToInt32(context.Request.Form["typeid"].ToString());
            string content = context.Request.Form["content"].ToString();

            BLL.TeamMessage.AddMiniTeamMesssage(from, toid, content, typeid);
        }
    }
}
