﻿using System.Runtime.Serialization;

namespace WebIM.Web.Chat.Channels
{
    [DataContract(Name = "cm")]
    public class ChatMessage
    {
        [DataMember(Name = "f")]
        private string from;
        [DataMember(Name = "m")]
        private string message;
        [DataMember(Name = "t")]
        private string time;

        public string From
        {
            get { return this.from; }
            set { this.from = value; }
        }

        public string Message
        {
            get { return this.message; }
            set { this.message = value; }
        }

        public string Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
    }
}