﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using MethodWorx.AspNetComet.Core;

namespace WebIM.Web.Chat.Channels
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ChatService
    {
        // Add [WebGet] attribute to use HTTP GET
        [OperationContract]
        public void SendMessage(string clientPrivateToken, string message)
        {
            try
            {
                ChatMessage chatMessage = new ChatMessage();

                //
                //  get who the message is from
                CometClient cometClient = DefaultChannelHandler.StateManager.GetCometClient(clientPrivateToken);

                //  get the display name
                chatMessage.From = cometClient.DisplayName;
                chatMessage.Message = message;
                chatMessage.Time = DateTime.Now.ToString("HH:mm");

                DefaultChannelHandler.StateManager.SendMessage("ChatMessage", chatMessage);
            }
            catch(Exception ee)
            {
                throw new Exception(ee.Message);
            }

            // Add your operation implementation here
            return;
        }
    }
}
