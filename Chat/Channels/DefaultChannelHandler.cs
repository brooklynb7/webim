﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;

using MethodWorx.AspNetComet.Core;
using System.Diagnostics;


namespace WebIM.Web.Chat.Channels
{
    public class DefaultChannelHandler : IHttpAsyncHandler
    {
        private static CometStateManager stateManager;

        static DefaultChannelHandler()
        {
            //  Initialize 
            stateManager = new CometStateManager(new InProcCometStateProvider());

            stateManager.ClientInitialized += new CometClientEventHandler(stateManager_ClientInitialized);
            stateManager.ClientSubscribed += new CometClientEventHandler(stateManager_ClientSubscribed);
            stateManager.IdleClientKilled += new CometClientEventHandler(stateManager_IdleClientKilled);
        }              

        public static void GetMemberList()
        {
            CometClient[] cc = stateManager.GetCometClient();
            string x = "";
            for (int i = 0; i < cc.Length; i++)
            {
                x += "<li><a title='" + cc[i].DisplayName + "'>" + cc[i].DisplayName + "</a></li>";
            }

            ChatMessage cm = new ChatMessage();
            cm.From = "Member";
            cm.Message = x;
            stateManager.SendMessage("GetMember", cm);
        }

        public static void GetMemberNum()
        {
            CometClient[] cc = stateManager.GetCometClient();          

            ChatMessage cm = new ChatMessage();
            cm.From = "MemberCount";
            cm.Message = cc.Length.ToString();
            stateManager.SendMessage("GetMemberCount", cm);
        }

        static void stateManager_IdleClientKilled(object sender, CometClientEventArgs args)
        {            
            //  ok, write a message saying we have timed out
            Debug.WriteLine("Client Killed: " + args.CometClient.DisplayName);
            //  send a chat message
            ChatMessage cm = new ChatMessage();

            cm.From = "系统消息";
            cm.Message = args.CometClient.DisplayName + " 退出了聊天室";

            stateManager.SendMessage("SysMessage", cm);

            GetMemberList();
            GetMemberNum();
        }
        
        static void stateManager_ClientInitialized(object sender, CometClientEventArgs args)
        {            
            //  ok, write a message saying we have timed out
            Debug.WriteLine("Client Initialized: " + args.CometClient.DisplayName);
            //  send a chat message
            ChatMessage cm = new ChatMessage();

            cm.From = "系统消息";
            cm.Message = args.CometClient.DisplayName + " 进入了聊天室";

            stateManager.SendMessage("SysMessage", cm);

            GetMemberList();
            GetMemberNum();
        }

        static void stateManager_ClientSubscribed(object sender, CometClientEventArgs args)
        {
            //  ok, write a message saying we have timed out
            Debug.WriteLine("Client Subscribed: " + args.CometClient.DisplayName);
        }

        #region IHttpAsyncHandler Members

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            return stateManager.BeginSubscribe(context, cb, extraData);
        }

        public void EndProcessRequest(IAsyncResult result)
        {
            stateManager.EndSubscribe(result);
        }

        #endregion

        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            throw new NotImplementedException();
        }

        public static CometStateManager StateManager
        {
            get { return stateManager; }
        }

        #endregion
    }
}