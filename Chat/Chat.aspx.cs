﻿using System;
using MethodWorx.AspNetComet.Core;
using WebIM.Web.Chat.Channels;

namespace WebIM.Web.Chat
{
    public partial class Chat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CometStateManager.RegisterAspNetCometScripts(this);
            DefaultChannelHandler.GetMemberList();
            DefaultChannelHandler.GetMemberNum();
        }
    }
}