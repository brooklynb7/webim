﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebIM.Web.Chat.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <title>ChatRoom</title>
    <link type="text/css" href="../css/WebIM.css" rel="Stylesheet" />
    <script src="../script/common/jquery.js" type="text/javascript"></script>
    <script src="../script/common/common.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
       <script type="text/javascript">
           document.domain = "sdo.com";
           if (getQueryString("user") != "") {
               GetInChatRoom(getQueryString("user"));
           }
           function GetInChatRoom(usernick) {
               $.ajax({
                   type: "POST",
                   cache: false,
                   url: "/Ashx/ChatRoom.ashx?&v=" + Math.random(),
                   data: "op=GetInChatRoom&usernick=" + encodeURIComponent(usernick),
                   dataType: "json",
                   success: function (data) {
                       var rst = data.rst;
                       var rstInfo = "";
                       if (rst == 0) {
                           window.parent.gChatRoomNick = usernick;
                           window.parent.Other.SetCookie("WebIM_ChatRoom", usernick);
                           window.location.href = "chat.aspx?&v=" + Math.random() + "&username=" + encodeURIComponent(usernick);
                       }
                       else {
                           $('#altMsg').html(data.msg);
                       }
                   },
                   error: function (request, status) {
                       $('#altMsg').html(data.msg);
                   }
               });
           }

           function onKeyPress(e) {
               var keyCode = null;

               if (e.which)
                   keyCode = e.which;
               else if (e.keyCode)
                   keyCode = e.keyCode;

               if (keyCode == 13) {
                   GetInChatRoom($('#usernameTxb').val());
                   return false;
               }
               return true;
           }
           /*$(document).ready(function () {
               if (getQueryString("user") != "") {
                   GetInChatRoom(getQueryString("user"));
               }
           });*/
           
    </script>
    <div id="miniChat_Loading" style="color: gray; height: 320px; position: absolute;
        background-color: White; width: 604px; display: block; padding: 5px 0 0 5px">
        <font color="red">请先填写昵称，再进入聊天室。</font><br />
        <br />
        昵称:<input type="text" id="usernameTxb" maxlength="20" /><span id="altMsg" style="color: red"></span>
        <br />
        <br />
        <input type="button" id="enterBtn" value="进入聊天室" />
    </div>
    <script>
        $('#enterBtn').click(function () {
            GetInChatRoom($('#usernameTxb').val());
        });
        $('#usernameTxb').keypress(function (event) {
            return onKeyPress(event);
        });
    </script>
    </form>
</body>
</html>
