﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFriendAnswer.aspx.cs" Inherits="WebIM.Web.Friends.AddFriendAnswer" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>无标题页</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>
    <script type="text/javascript" src="Script/CommonJS.js"></script>
    <style type="text/css">
        div
        {
            line-height: 20px;
        }
    </style>
    <script type="text/javascript">
        document.domain = getQueryString("domain");

        function BindData() {
            //ShowGroup("", "");
            $("#lbUser").html("<div class='title'>" + getQueryString("nick") + "</div>" + getQueryString("uid"));
            $("#lbQuestion").html(getQueryString("question"));

            var gender = getQueryString("gender");
            var pic = GetGenderPic(gender);
            $("#imgHead").attr("src", "../Images/friends/" + pic);

            $("#hidden3").val(getQueryString("uid"));
        }

        function SendRequest() {
            var uid = $("#hidden3").val();
            var answerMsg = $("#txtAnswer").val();
            var gid = $("#ddlGroup").val();
            window.parent._webIM.WebService.sendAddFriendAnswer(uid, answerMsg, gid);

            Cancel();
        }

        if (window.attachEvent) {
            window.attachEvent("onload", BindData);
        }
        else if (window.addEventListener) {
            window.addEventListener("load", BindData, false);
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            您将添加以下好友：
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <img id="imgHead" src="../Images/friends/big_female.gif" />
                    </td>
                    <td>
                        <div id="lbUser" >
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            对方需要您回答一下验证问题：
            <div id="lbQuestion">
            </div>
            <input type="text" maxlength="50" id="txtAnswer" />
        </div>
        <!--div style=" margin-top:5px">
            请选择分组：
            <select id="ddlGroup">
            </select>
            <a href="javascript:void(0)" onclick="return AddGroup();">+新建分组</a>
        </div-->
        <div>
            <input type="button" class="button" onclick="SendRequest();return false;" value="确认" />
            <input type="button" class="button" onclick="return Cancel();" value="关闭" /></div>
    </div>
    <input type="hidden" id="hidden3" value="[uid]" />
    </form>
</body>
</html>
