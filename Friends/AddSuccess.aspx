﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddSuccess.aspx.cs" Inherits="WebIM.Web.Friends.AddSuccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>
    <script type="text/javascript" src="Script/CommonJS.js"></script>
    <style type="text/css">
        div
        {
            line-height: 20px;
        }
    </style>
    <script type="text/javascript">
        document.domain = getQueryString("domain");

        function BindData() {
            ShowGroup("", "");
            $("#lbUser").html("您已成功添加<br/><div class='title'>" + getQueryString("nick") + "</div>(" + getQueryString("uid") + ")成为好友");
            var gender = getQueryString("gender");
            var pic = GetGenderPic(gender);
            $("#imgHead").attr("src", "../Images/friends/" + pic);

            $("#hidden3").val(getQueryString("uid"));
        }

        function Confirm() {
            var uid = $("#hidden3").val();
            var gid = $("#ddlGroup").val();
            window.parent._webIM.Data.editUserGroup(uid, gid);

            Cancel();
        }

        function StartTalk() {
            window.parent._webIM.CMD.openChatWindow($("#hidden3").val(), true);
            Cancel();
        }
        if (window.attachEvent) {
            window.attachEvent("onload", BindData);
        }
        else if (window.addEventListener) {
            window.addEventListener("load", BindData, false);
        } 
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table>
                <tr>
                    <td>
                        <img id="imgHead" src="../Images/friends/big_female.gif" />
                    </td>
                    <td>
                        <div id="lbUser"></div>
                    </td>
                </tr>
            </table>
        </div>
        <!--div class="trDiv">
            备注[可选]：<asp:TextBox ID="txtBeizhu" runat="server"></asp:TextBox>
        </div-->
        <div>
            请选择分组：
            <select id="ddlGroup">
            </select>
            <a href="javascript:void(0)" onclick="return AddGroup();">+新建分组</a>
        </div>
        <div>
            <input type="button" class="button" onclick="return Confirm();" value="完成" />
            <input type="button" class="button" onclick="return StartTalk();" value="发起会话" /></div>
    </div>
    <input type="hidden" id="hidden3" value="[uid]" />
    </form>
</body>
</html>
