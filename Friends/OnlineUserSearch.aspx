﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineUserSearch.aspx.cs"
    Inherits="WebIM.Web.Friends.OnlineUserSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/city.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>    
    <script type="text/javascript" src="script/commonjs.js"></script>    
    <script type="text/javascript">
        if (getQueryString("domain") == "") {
           // location.href = "http://webim.sdo.com";
        }

        document.domain = getQueryString("domain");

        function test() {
            var num, city1, i;
            num = $("#DropShengfen").get(0).selectedIndex;  //获取选中省份的索引值
            city1 = city[num].split(",");  //获取相应的地区城市列表
            $("#DropDiqu").html("");
            for (var i = 0; i < city1.length; i++) {
                $("#DropDiqu").append("<option value='" + city1[i] + "'>" + city1[i] + "</option>");
            }
        }
                       
        function search(pageindex) {
            var strGender = $("#ddlGender").val();
            var strAge = $("#ddlAge").val();
            var strCountry = $("#ddlCounttry").val();
            var strCity = $("#DropShengfen").val();
            var strRegion = $("#DropDiqu").val();
            if (strRegion = '[请选择]') {
                strRegion = '';
            }
            $("#userList").html('<img src ="../images/Loading.gif"/><font style="color:red">Loading......</font>');
            $.ajax({
                type: "POST",
                cache: false,
                url: "/Ashx/Friends.ashx",
                data: "op=GetOnlineUsers&pagesize=5&pageindex=" + pageindex + "&strGender=" + strGender +
                      "&strAge=" + strAge + "&strCountry=" + strCountry + "&strCity=" + strCity + "&strRegion=" + strRegion,
                dataType: "json",
                success: function (data) {
                    if (data.recordcount > 0) {
                        var list = data.Table;
                        var str = "";
                        var pic = "";
                        var gender = "";
                        $.each(list, function (i, item) {
                            pic = GetGenderPic(item.gender);
                            gender = GetGender(item.gender);
                            str += '<div onmouseover="$(this).css(\'background-color\',\'#F2F2F2\')" onmouseout="$(this).css(\'background-color\',\'#FFFFFF\')" ' +
                                   '     style="height:57px;clear:both; margin-bottom: 2px">' +
                                   '  <div style="float: left; padding-top: 8px; padding-left: 5px"><img src="../Images/friends/' + pic + '" /></div>' +
                                   '  <div style="margin-left: 55px; padding-top: 5px">' +
                                   '     <div style="height:15px">' +
                                   '          <div style="float: left;"><a href="javascript:void(0)" onclick="ViewInfo(\'' + item.UserID + '\',\'' + item.nick + '\',\'' + item.age + '\' ,\'' + item.gender + '\' , \'' + item.signature + '\')">' + item.nick + '(' + item.UserID + ')</a></div>' +
                                   '          <div style="float: right;">' +
                                   '             <a href="javascript:void(0)" onclick="ViewInfo(\'' + item.UserID + '\',\'' + item.nick + '\',\'' + item.age + '\' ,\'' + item.gender + '\' , \'' + item.signature + '\')">查看资料</a>&nbsp;&nbsp;&nbsp' +
                                   '             <a href="javascript:void(0)" onclick="AddFriend(\'' + item.UserID + '\')">加为好友</a></div><br />' +
                                   '     </div>' +
                                   '     <div style="padding-top:2px;">' +
                                   '           ' + gender + ' ' + item.age + '岁 ' + item.province + ' ' + item.city + '</div>' +
                                   '     <div style="padding-top:2px;">' +
                                   '          ' + item.signature + '</div>' +
                                   '  </div>' +
                                   '</div>';
                        });
                        $("#userList").html(str);
                        MakePageNav(data.recordcount, 5, pageindex, "toUserListPage");
                    }
                    else {
                        $("#userList").html("<font color='red'>没有符合条件的结果哦！</font>");
                    }
                },
                error: function (request, status) {
                    $("#userList").html("<font color='red'>出错啦！请联系管理员哦！</font>");
                }
            });
        }
        //控制前端分页显示
        function toUserListPage(page) {
            search(page);
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <select id="ddlGender">
                        <option value="">性别</option>
                        <option value="男">男</option>
                        <option value="女">女</option>
                        <option value="保密">保密</option>
                    </select>
                    <select id="ddlAge">
                        <option value="">年龄</option>
                    </select>
                    <select id="ddlCounttry">
                        <option value="">国家</option>
                        <option value="中国">中国</option>
                    </select>
                    <select id="DropShengfen" onchange="test();">
                        <option value="">[请选择]</option>
                    </select>
                    <select id="DropDiqu">
                        <option value="">[请选择]</option>
                    </select>&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp; <a href="javascript:void(0)" onclick="toUserListPage(1);">查找</a>
                </td>
            </tr>
        </table>
    </div>
    <div style="margin-top:5px;padding: 2px 2px 2px 2px; border: solid 1px gray;
        height: 293px" id="userList">
    </div>
    <div id="pageNav" class="pageNav" style="float: left">
    </div>
    <div style="float: right">
        <input type="button" class="button" onclick="window.location.href='commonsearch.aspx?domain=sdo.com'"
            value="精确查找好友" /></div>
    </form>
    <script type="text/javascript">
        for (var i = 0; i < province.length; i++) {
            $("#DropShengfen").append("<option value='" + province[i] + "'>" + province[i] + "</option>");
        }
        for (var i = 1; i < 100; i++) {
            $("#ddlAge").append("<option value='" + i + "'>" + i + "</option>");
        }

        function AddFriend(ID) {
            window.parent._webIM.WebService.addFriend(ID);
        }
        function ViewInfo(ID, nick, age, gender, sign) {
            gender = GetGender(gender);
            window.parent._webIM.WebService.showUserProfile(ID, nick, age, gender, sign);
        }
        toUserListPage(1);
    </script>
</body>
</html>
