﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestConfirm.aspx.cs"
    Inherits="WebIM.Web.Friends.RequestConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>
    <script type="text/javascript" src="Script/CommonJS.js"></script>
    <style type="text/css">
        div
        {
            line-height: 20px;
        }
    </style>
    <script type="text/javascript">
        document.domain = getQueryString("domain");

        function SetValue() {
            $("#lbUser").html(getQueryString("nick") + "<br />" + getQueryString("uid"));
            var gender = getQueryString("gender");
            var pic = GetGenderPic(gender);
            $("#imgHead").attr("src", "../Images/friends/" + pic);

            $("#hidden3").val(getQueryString("uid"));
        }


        if (window.attachEvent) {
            window.attachEvent("onload", SetValue);
        }
        else if (window.addEventListener) {
            window.addEventListener("load", SetValue, false);
        } 
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table>
                <tr>
                    <td>
                        <img id="imgHead" src="../Images/friends/big_female.gif" />
                    </td>
                    <td>
                        <div id="lbUser">
                        </div>
                        已经加您为好友了
                    </td>
                </tr>
            </table>
        </div>
        <!--div>
            附加信息：
        </div>
        <div>
            <textarea id="txtInfo"> </textarea>
        </div>
        <div>
            <div>
                <input type="radio" id="radio1" name="radConfirm" runat="server" value="0" checked="checked" />同意并添加对方为好友
                <input type="radio" id="radio2" name="radConfirm" runat="server" value="1" />同意
                <input type="radio" id="radio3" name="radConfirm" runat="server" value="2" />拒绝
            </div>
            <div>
                <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
                <input type="checkbox" id="checkbox1" />
                拒绝再接受此人请求
            </div>
        </div-->
        <div>
            <input type="button" class="button" onclick="return Cancel();" value="加为好友" />
            <input type="button" class="button" onclick="return Cancel();" value="取消" /></div>
    </div>
    <input type="hidden" id="hidden3" value="[uid]" />
    </form>
</body>
</html>
