﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestSend.aspx.cs" Inherits="WebIM.Web.Friends.RequestSend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>
    <script type="text/javascript" src="Script/CommonJS.js"></script>
    <script type="text/javascript">
        document.domain = getQueryString("domain");
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <div style="float:left;">
                <img id="imgHead" src="../Images/friends/big_sendinfo.gif" />
            </div>
            <div style="padding: 6px 0 0 10px; margin-left:40px">
                您的好友添加请求已发送成功，<br />
                正在等待对方确认。
            </div>
        </div>
        <div style="clear:both;padding-top: 5px; float:right">
            <input type="button" style="" class="button" onclick="return Cancel();" value="关闭" />
        </div>
    </div>
    </form>
</body>
</html>
