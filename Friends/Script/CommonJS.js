﻿// JScript File
function _GetRadioGroupChecked(groupName)
{
    var inputs = document.getElementsByTagName("INPUT");
    for(var i=0;i<inputs.length;i++)
    {
        if(inputs[i].type == "radio" && inputs[i].name == groupName && inputs[i].checked)
        {
            return inputs[i].value;
        }
    }
}

//关闭窗口
function Cancel() {
    window.parent._webIM.WebService.closeForm(getQueryString("Type"), getQueryString("Id"));
    return false;
}

//根据编码获取性别
function GetGender(gender) {
    var rst;
    gender = gender == 255 ? 0 : gender; 
    switch (gender) {
        case '0':
        case 0:
            rst = "女";
            break;
        case '1':
        case 1:
            rst = "男";
            break;
        default:
            rst = "保密";
            break;
    }
    return rst;
}

//根据编码获取性别图片
function GetGenderPic(gender) {
    var rst;
    gender = gender == 255 ? 0 : gender;
    switch (gender) {
        case '0':
        case 0:
            rst = "big_female.gif";
            break;
        case '1':
        case 1:
            rst = "big_male.gif";
            break;
        default:
            rst = "big_no.gif";
            break;
    }
    return rst;
}

//添加分组选项          
function AddGroupOption() {
    var group = window.parent._webIM.WebService.getGroup();
    var j = 0;
    for (var i = 0; i < group.length; i++) {
        var g = group[i];
        if (g.ID == 2 || g.ID == 1) {
            continue;
        }
        $("#ddlGroup").append("<option value='" + g.ID + "'>" + g.Name + "</option>");
        j++;
    }
}

//显示分组选项
function ShowGroup(id, name) {   
    $("#ddlGroup").html("");
    AddGroupOption();
    if (name != "") {
        var count = $("#ddlGroup option").length;
        for (var i = 0; i < count; i++) {
            if ($("#ddlGroup").get(0).options[i].text == name) {
                $("#ddlGroup").get(0).options[i].selected = true;
                break;
            }
        }
        // $("#ddlGroup option[text='" + name + "']").attr("selected", true);
    }
    return;
}

function AddGroup() {
    window.parent._webIM.CMD.addGroup(ShowGroup);
    return false;
}