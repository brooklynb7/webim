﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="WebIM.Web.Friends.UserInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>无标题页</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>
    <script type="text/javascript" src="Script/CommonJS.js"></script>
    <style type="text/css">
        div
        {
            line-height: 20px;
        }
    </style>
    <script type="text/javascript">
        document.domain = getQueryString("domain");

        function BindData() {
            $("#nick").html(getQueryString("nick") || "");
            $("#gender").html(getQueryString("gender"));
            //alert(getQueryString("sign"));
            $("#sign").html(getQueryString("sign"));
            $("#province").html(getQueryString("Province") || "");
            $("#city").html(getQueryString("City") || "");
            $("#age").html(getQueryString("age") || "");
        }

        if (window.attachEvent) {
            window.attachEvent("onload", BindData)
        }
        else if (window.addEventListener) {
            window.addEventListener("load", BindData, false);
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            昵称：<span id="nick">nikc</span></div>
        <div>
            性别：<span id="gender">sd</span></div>
        <div>
            年龄：<span id="age">sd</span></div>
        <div>
            省份：<span id="province">sd</span></div>
        <div>
            城市：<span id="city">sd</span></div>
        <div>
            签名：<span id="sign">sd</span></div>
        <div>
            <input type="button" class="button" onclick="return Cancel();" value="关闭" /></div>
    </div>
    </form>
</body>
</html>
