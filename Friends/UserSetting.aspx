﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSetting.aspx.cs" Inherits="WebIM.Web.Friends.UserSetting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Css/page.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/common/jquery.js"></script>
    <script type="text/javascript" src="../script/common/city.js"></script>
    <script type="text/javascript" src="../script/common/common.js"></script>
    <script type="text/javascript" src="script/commonjs.js"></script>
    <script type="text/javascript">
        document.domain = getQueryString("domain");

        function BindCity() {
            var num, city1, i;
            num = $("#DropShengfen").get(0).selectedIndex;  //获取选中省份的索引值
            city1 = city[num].split(",");  //获取相应的地区城市列表
            $("#DropDiqu").html("");
            for (var i = 0; i < city1.length; i++) {
                $("#DropDiqu").append("<option value='" + city1[i] + "'>" + city1[i] + "</option>");
            }
        }

        function Show() {
            $('#divQuestion').css("display", "block");
        }
        function Hide() {
            $('#divQuestion').css("display", "none");
        }

        function BindData() {
            var data = window.parent._webIM.WebService.getProfile();
            $("#txtNick").val(data.UserNick);
            $("#ddlGender").val(data.UserGender);
            $("#txtQianming").val(data.UserSign);

            var data1 = window.parent._webIM.WebService.getProfileEx();
            if (data1.local_province != "") {
                $("#DropShengfen").val(data1.local_province);
            }
            BindCity();
            if (data1.local_city != "") {
                $("#DropDiqu").val(data1.local_city);
            }
            $("#ddlAge").val(data1.age);
            switch (parseInt(data1.auth_type)) {
                case 0:
                    $("#radio3").attr("checked", true);
                    break;
                case 1:
                    $("#radio4").attr("checked", true);
                    break;
                case 2:     
                    $("#radio5").attr("checked", true);
                    break;
                case 3:
                    $("#radio6").attr("checked", true);
                    Show();
                    break;
                default:
                    $("#radio3").attr("checked", true);
                    break;
            }            

            $("#txtQuestion").val(data1.question);
            $("#txtAnswer").val(data1.answer);
        }

        function Save() {
            var _nick = $("#txtNick").val() || "";
            if (_nick == "") {
                alert("昵称不能为空");
                return false;
            }
            if (_nick.replace(/(^\s*)/g, "") == "") {
                alert("昵称不能为空");
                return false;
            }
            if (getLength(_nick) > 20) {
                alert("长度不能超过20");
                return false;
            }
            var _gender = $("#ddlGender").val();
            var _age = $("#ddlAge").val();
            var _local_province = $("#DropShengfen").val();
            if (_local_province == '[请选择]') {
                _local_province = '';
            }
            var _local_city = $("#DropDiqu").val();
            if (_local_city == '[请选择]') {
                _local_city = '';
            }
            var _sign = $("#txtQianming").val() || "";
            var _auth_type = _GetRadioGroupChecked("radConfirm");
            var _question = $("#txtQuestion").val();
            var _answer = $("#txtAnswer").val();
            window.parent._webIM.WebService.updateUserProfile(_nick, _gender, _age, _local_province, _local_city, _sign, _auth_type, _question, _answer);

            return Cancel();
        }
       

        if (window.attachEvent) {
            window.attachEvent("onload", BindData)
        }
        else if (window.addEventListener) {
            window.addEventListener("load", BindData, false);
        } 
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    昵称：
                </td>
                <td colspan="3">
                    <input id="txtNick" maxlength="20" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    性别：
                </td>
                <td>
                    <select id="ddlGender" onchange="">
                        <option value="2" selected="selected">保密</option>
                        <option value="1">男</option>
                        <option value="0">女</option>
                    </select>
                </td>
                <td>
                    年龄：
                </td>
                <td>
                    <select id="ddlAge">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    省份：
                </td>
                <td>
                    <select id="DropShengfen" onchange="BindCity();">
                        <option value="">[请选择]</option>
                    </select>
                </td>
                <td>
                    城市：
                </td>
                <td>
                    <select id="DropDiqu">
                        <option value="">[请选择]</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    签名：<br />
                    <textarea id="txtQianming"></textarea>
                </td>
            </tr>
            <tr style="display: none;">
                <td colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                    <input type="radio" id="radio1" name="radMessage" value="0" />接受所有人消息
                </td>
                <td colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                    <input type="radio" id="radio2" name="radMessage" value="1" />只接受好友消息
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <input type="radio" id="radio3" name="radConfirm" value="0" checked="checked" onclick="Hide()" />允许任何人加为好友
                    <br />
                    <input type="radio" id="radio4" name="radConfirm" value="1" onclick="Hide()" />
                    需要身份验证加为好友
                    <br />
                    <input type="radio" id="radio5" name="radConfirm" value="2" onclick="Hide()" />
                    不允许任何人加为好友
                    <br />
                    <input type="radio" id="radio6" name="radConfirm" value="3" onclick="Show()" />
                    回答问题加为好友
                    <div id="divQuestion" style="display: none;">
                        <div>
                            设置问题<input id="txtQuestion" type="text" />
                        </div>
                        <div>
                            设置答案<input id="txtAnswer" type="text" />
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <input type="button" class="button" onclick="return Save();" value="确定" />
                    <input type="button" class="button" onclick="return Cancel();" value="取消" />
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="hidden1" value="[type]" />
    <input type="hidden" id="hidden2" value="[id]" />
    <script type="text/javascript">
        for (var i = 0; i < province.length; i++) {
            $("#DropShengfen").append("<option value='" + province[i] + "'>" + province[i] + "</option>");
        }
        for (var i = 1; i < 100; i++) {
            $("#ddlAge").append("<option value='" + i + "'>" + i + "</option>");
        }
    </script>
    </form>
</body>
</html>
