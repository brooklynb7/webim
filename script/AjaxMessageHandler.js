//**********************************
//*消息处理器
//**********************************

var AjaxMessageHandler = function()
{
	var uniqueInstance = null;
	var _imInstance = null;
	
	var _OnlineUserSearchUrl = "WebIMPage/addfriend.asp?v="+Math.random(); //在线好友搜索地址
	
	function Constructor(imInstance)
	{
		_imInstance = imInstance;
		return{	
			//登录
			setUserLogin:function(UserName,pass,us,clientVer,cb)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=0","us="+us+"&UserName="+UserName+"&pass="+pass,cb);
			},
			//登出
			setUserLogout:function()
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=2");
			},			
			//返回个人信息的User对象
			getMyUserInfo:function()
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetMyInfo.asp",
					function(o)
					{
						if(!o)return;
						if(!$T(o.responseXML,"list"))return;
						_imInstance.Profile = _imInstance.Common.getUserFromXml($T(o.responseXML,"list").item(0))[0];
					});
			},	
			//返回个人配置
			getMyConfig:function()
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetMyConfig.asp",
					function(o)
					{
						if(!o)return;
						if(!$T(o.responseXML,"list"))return;
						_imInstance.Config =_imInstance.Common.getConfigFromXml($T(o.responseXML,"list").item(0));
					});
			},	
			//返回好友列表的User对象数组
			getMyFriendList:function(cb)
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetMyFriend.asp",
					function(o)
					{
						if(!o)return;					
						if(!$T(o.responseXML,"list"))return;
						_imInstance.Friend =_imInstance.Common.getUserFromXml($T(o.responseXML,"list").item(0));
						if(cb)cb(o);
					});
			},				
			//返回好友信息的User对象
			getFriendUserInfo:function(uid,cb)
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetUserInfo.asp?id="+uid,cb);
			},
			//返回分组列表的Group对象数组
			getMyGroupList:function(cb)
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetMyGroup.asp",
					function(o)
					{
						if(!o)return;
						if(!$T(o.responseXML,"list"))return;
						_imInstance.Group = _imInstance.Common.getGroupFromXml($T(o.responseXML,"list").item(0));
						if(cb)cb(o);
					});
			},				
			//返回群列表的Team对象数组
			getMyTeamList:function(cb)
			{				
				new Ajax().get(gTeamServer + "/AjaxHandler/GetMyTeam.asp",
					function(o)
					{
						if(!o)return;						
						if(!$T(o.responseXML,"list"))return;
						_imInstance.Team = _imInstance.Common.getTeamFromXml($T(o.responseXML,"list").item(0));						
						if(cb)cb(o);
					});
			},
			//返回指定群的成员列表数组对象
			getMyTeamUserList:function(tid,cb)
			{			
				new Ajax().get(gServerUrl + "/AjaxHandler/GetTeamUser.asp?tid=" + tid,
					function(o)
					{
						if(!o)return;
						if(!$T(o.responseXML,"list"))return;
						_imInstance.TeamUser =_imInstance.Common.getTeamUserFromXml($T(o.responseXML,"list").item(0));
						if(cb)cb(o);
					});
			},
			//返回消息列表msg对象数组
			getMyMsgList:function(cb)
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetMyMsg.asp?code="+_imInstance.Sys.Code,cb);
			},
			//返回群消息列表msg对象数组
			getMyTeamMsgList:function(cb)
			{
				new Ajax().get(gServerUrl + "/AjaxHandler/GetMyTeamMsg.asp?code="+_imInstance.Sys.Code,cb);
			},
			//修改用户Profile
			setUserProfile:function()
			{
				var data = "UserAvatar="+_imInstance.Profile.UserAvatar;
				data+="&usernick="+_imInstance.Profile.UserNick.escapeEx();
				data+="&usersign="+_imInstance.Profile.UserSign.escapeEx();
				data+="&userstatus="+_imInstance.Profile.UserStatus;
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=4",data);
			},
			//发送消息
			sendMessage:function(msg)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=3","from="+msg.From+"&to="+msg.To+"&content="+msg.Content.escapeEx()+"&type="+msg.Type);
			},
			//发送群消息
			sendTeamMessage:function(msg)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=13","from="+msg.From+"&to="+msg.To+"&content="+msg.Content.escapeEx()+"&type="+msg.Type);
			},	
			//接受添加好友请求
			acceptAddFriend:function(uid,cb)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=5","to="+uid,cb);
			},
			//接受加入群组请求
			acceptJoinTeam:function(tid,uid,cb)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=15","tid="+tid+"&uid="+uid,cb);
			},
			//删除好友
			deleteFriend:function(uid)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=6","to="+uid);
			},
			//修改屏蔽状态
			blockFriend:function(uid,status)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=7","to="+uid+"&s="+status);
			},
			//修改好友昵称
			editCustomName:function(uid,name)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=8","to="+uid+"&n="+name.escapeEx());
			},
			//修改好友分组
			editUserGroup:function(uid,gid)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=12","id="+uid+"&gid="+gid);
			},
			//添加组
			addGroup:function(gname,cb)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=9","n="+gname.escapeEx(),cb);
			},
			//删除
			delGroup:function(gid,cb)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=10","id="+gid,cb);
			},
			//修改
			editGroup:function(gid,gname)
			{
				new Ajax().post(gServerUrl + "/AjaxHandler/service.asp?t=11","id="+gid+"&n="+gname.escapeEx());
			},
			getOnlineUserSearchUrl:function()
			{
				return _OnlineUserSearchUrl;
			}
		}
	}
	
	return{
		IsInstance:function(imInstance)
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor(imInstance);
			}
			return uniqueInstance;
		}
	}	
}();