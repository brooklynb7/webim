﻿//**********************************
//*消息处理
//**********************************
var _imInstance = Comet.IsInstance().getIMInstance();

function OnKickUser(reason) {
    try {
        if (gDebugLog) { _imInstance.Logger.WriteLog(new Date().toLocaleString() + ": Receive OnKickUser."); }
        _imInstance.KeepLive.stopKeepLive();
        var hint = reason == 0 ? "由于网络原因，服务器中断连接，您需要重新登录。" : "帐号已在其它地方登录，您被迫下线。";
        _imInstance.Common.showAlert(hint, "提示", _imInstance.CMD.logoutWebIM);
    } catch (error) { }
}

function OnKeepalive() {
    if (gDebugLog) { _imInstance.Logger.WriteLog(new Date().toLocaleString() + ": Receive OnKeepalive Package."); }
    _imInstance.Sys.TimeOutCount = 0;
}

function OnLogin(sid, code, uid, name, gender,signature,server) {
    //更新至指定服务器
	if(server){_imInstance.ServerUrl = "http://" + decodeURI(server) + ":" + gConnServerPort;}
    name = decodeURI(_imInstance.Common.gb2312ToUTF8(name)) || uid; 
    signature = decodeURI(_imInstance.Common.gb2312ToUTF8(signature));
    gender = gender == 255 ? 0 : gender;

    var _LoginResultXml = _imInstance.XmlGenerate.getLoginResultXml("list", ["code", code],
																		["uid", uid],
																		["name", name],
																		["gender", gender],
																		["signature", signature]);
    var avatar=gender;     
    //保存部分个人信息
    var _MyUserInfoXml = _imInstance.XmlGenerate.getMyInfoXml(["f", avatar],    //Avatar
														      ["id", uid],          //uid
															  ["n", name],          //昵称
															  ["e", ""],            //电邮
															  ["sn", signature],    //签名
															  ["s", "1"],           //状态
															  ["g", ""],            //保留
															  ["b", ""],            //保留
															  ["cn", name],
															  ["u", gender]); //性别
    _imInstance.Profile = _imInstance.Common.getUserFromXml($T(_MyUserInfoXml, "list").item(0))[0];
    _imInstance.CMD.loginCallBack(sid,_LoginResultXml);
}

function OnGroupList() {
    if (arguments.length < 1) return;

    if (!(_imInstance.Group)) { _imInstance.Group = []; }
    var name;
    for (var i = 1; i < arguments.length; i = i + 2) {
        try { name = decodeURIComponent(_imInstance.Common.gb2312ToUTF8(arguments[i + 1])); } catch (error) { name = decodeURIComponent(arguments[i + 1]) };
        var id = arguments[i];

        if (!_imInstance.Common.getGroupById(id))//防止重复
            _imInstance.Group.add(new _imInstance.Model.Group(false, name, parseInt(id)));
        _imInstance.Common.updateGroup(null, name, id);
    }
    if (_imInstance.Group.length < 1) _imInstance.Group.add(new _imInstance.Model.Group(true, "默认", 1));
}

function OnGetTeamOrder(order) {
    order = decodeURI(_imInstance.Common.gb2312ToUTF8(order)) || "0,1,2";
    _imInstance.GroupOrder = order.split(",");
}

function OnFriendList() {
    //alert(arguments[1]+"|"+arguments[2]+"|"+arguments[3]+"|"+arguments[4]);
    if (arguments.length < 1) return;

    if (!(_imInstance.Friend)) { _imInstance.Friend = []; }
    for (var i = 1; i < arguments.length; i = i + 4) {
        var id = arguments[i];
        var usernick = null;
        var gender = 1;
        var face = null;
        var groupid = arguments[i + 1];
        var email = null;
        var sign = "";
        var status = arguments[i + 3];
        var isblocked = false;
        var customname = decodeURI(arguments[i + 2]);

        if (!_imInstance.Common.getUserFromArr(id, _imInstance.Friend))//防止重复
            _imInstance.Friend.add(new _imInstance.Model.User(face, id, usernick, email, sign, parseInt(status), parseInt(groupid), isblocked, customname, gender));
    }
}

function OnFriendInfo() {
    if ((arguments.length < 1) || (!(_imInstance.Friend))) return;

    //补充用户信息
    _imInstance.FriendLoadedCount += parseInt(arguments[0]);
    if (!(_imInstance.Friend)) { _imInstance.Friend = []; }
    for (var i = 1; i < arguments.length; i = i + 4) {
        var id = arguments[i];
        var usernick = decodeURI(_imInstance.Common.gb2312ToUTF8(arguments[i + 1]));
        var gender = parseInt(arguments[i + 2]); if (gender==255) { gender = 0; }
        var face = _imInstance.StyleManager.getBigDefaultHeadPath(gender);
        var groupid = null;
        var email = id.toString();
        var sign = decodeURI(_imInstance.Common.gb2312ToUTF8(arguments[i + 3]));
        var status = null;
        var isblocked = null;
        var customname = null;

        _imInstance.Common.updateUserInfo(face, id, usernick, email, sign, status, groupid, isblocked, customname, gender);
    }

    _imInstance.EventQueue.ProcessEvent(id, gEventID.eiAddFriendSuccess, { dst: id, UserNick: usernick, Gender: gender });
    _imInstance.EventQueue.ProcessEvent(id, gEventID.eiAddFriendAuther, { dst: id, UserNick: usernick, Gender: gender });
    _imInstance.EventQueue.ProcessEvent(id, gEventID.eiAddFriendAutherSuccess, { dst: id, UserNick: usernick, Gender: gender });
    _imInstance.EventQueue.ProcessEvent(id, gEventID.eiAuthAcceptedAddReq, null);

    if (_imInstance.FriendLoadedCount == _imInstance.Friend.length) { _imInstance.CMD.renderMyFriend(); };
}

function OnGetFriendAlias(result, dst, alias, mobile, mail_adress, more_info) {
    _imInstance.Common.updateUserInfo(null, dst, alias, null, null, null, null, null, null, null);
	//_imInstance.CMD.renderMyFriend();
}

function OnFriendListStatus() {
    if ((arguments.length < 1) || (!(_imInstance.Friend))) return;

    for (var i = 1; i < arguments.length; i = i + 2) {
        _imInstance.Common.updateUserStatus(arguments[i], parseInt(arguments[i + 1]));
    }
    //if(_imInstance.FriendLoadedCount==_imInstance.Friend.length){_imInstance.CMD.renderMyFriend();};
}

function ConvertSedToDate(sed) {
    var startyear = 1970;
    var startmonth = 1;
    var startday = 1;

    var d, s;
    var sep = ":";
    d = new Date(1970, 1, 1, 8, 0, 0);
    d.setMilliseconds(sed);
    s = d.getHours().toString().padLeft("0", 2) + ":" + d.getMinutes().toString().padLeft("0", 2) + ":" + d.getSeconds().toString().padLeft("0", 2);

    return s;
}

function OnMessage(Type, From, To, Status, AddTime, Content) {
    if (Status != gUserStatus.usOffline) { if (_imInstance.Common.updateUserStatus(From, parseInt(Status))) { _imInstance.CMD.renderMyFriend(); } };
	if (Status == gUserStatus.usHide){_imInstance.Common.updateUserStatus(From, gUserStatus.usOnline);} //收到隐身好友的消息，就将其改为在线
    Content = decodeURIComponent(_imInstance.Common.gb2312ToUTF8(Content));    
    Content = _imInstance.XmlGenerate.parseIMMsg(Content).enCodeHTML();
    var _MyMessageXml = _imInstance.XmlGenerate.getMyMsgXml(["From", From],
														["To", To],
														["Content", Content],
														["Type", Type],
														["Status", Status],
														["IsConfirm", 2],
														["AddTime", ConvertSedToDate(AddTime * 1000)/*_imInstance.IMDateTime.format(AddTime)*/]);
    _imInstance.CMD.OnRecvMessages(_MyMessageXml);
}

function OnAddFriend(result, dst, status, attchtext) {
    switch (parseInt(result)) {
        case gAddFriendResult.ADD_FRIEND_SUCCESS:
            {
                if (!_imInstance.Common.getUserFromArr(dst, _imInstance.Friend))//防止重复
                    _imInstance.Friend.add(new _imInstance.Model.User(null, dst, null, dst, dst, parseInt(status), 0, 0, null, null));
                var _MySysXml = _imInstance.XmlGenerate.getMySysXml(["From", dst],
															["To", _imInstance.Profile.UserID],
															["Content", decodeURI(_imInstance.Common.gb2312ToUTF8(attchtext))],
															["Type", gMessagesType.mtAddFriend],
															["Code", result],
															["IsConfirm", 2],
															["AddTime", _imInstance.IMDateTime.format(new Date().toLocaleString())]);
                _imInstance.CMD.OnRecvMessages(_MySysXml);
                break;
            }
        case gAddFriendResult.ADD_FRIEND_AUTHER:
        case gAddFriendResult.ADD_FRIEND_ANSWER:
        case gAddFriendResult.ADD_FRIEND_FORBIDDEN:
        case gAddFriendResult.ADD_FRIEND_EXIST:
        case gAddFriendResult.ADD_FRIEND_EXCEED:
        case gAddFriendResult.ADD_FRIEND_TOOFAST:
        case gAddFriendResult.ADD_FRIEND_SELF:
        case gAddFriendResult.ADD_FRIEND_BLACKLIST:
            {
                var _MySysXml = _imInstance.XmlGenerate.getMySysXml(["From", dst],
															["To", _imInstance.Profile.UserID],
															["Content", decodeURI(_imInstance.Common.gb2312ToUTF8(attchtext))],
															["Type", gMessagesType.mtAddFriend],
															["Code", result],
															["IsConfirm", 2],
															["AddTime", _imInstance.IMDateTime.format(new Date().toLocaleString())]);
                _imInstance.CMD.OnRecvMessages(_MySysXml);
                break;
            }
        case gAddFriendResult.ADD_FRIEND_NO_UIN:
            {
                break;
            }
        case gAddFriendResult.ADD_FRIEND_UNKNOWN:
            {
                break;
            }
    }
}

function OnModifyTeamInfo(result, type, group_id, group_name) {
    switch (parseInt(result)) {
        case 0:
            {
                switch (parseInt(type)) {
                    case 0: //add
                        {
                            _imInstance.Group.add(new _imInstance.Model.Group(false, decodeURIComponent(_imInstance.Common.gb2312ToUTF8(group_name)), parseInt(group_id)));
                            break;
                        }
                    case 1: //delete
                        {
                            _imInstance.Common.deleteGroup(group_id);
                            break;
                        }
                    case 2: //modify
                        {
                            _imInstance.Common.updateGroup(null, group_id, decodeURIComponent(_imInstance.Common.gb2312ToUTF8(group_name)));
                            break;
                        }
                }
            }
    }
    _imInstance.CMD.renderMyFriend();
}

function OnChangeTeam(result) {
    if (parseInt(result) == 0) { _imInstance.EventQueue.ProcessEvent(null, gEventID.eiEditUserGroup, null); return };
    _imInstance.Common.showAlert("不能将好友移动到此分组。", "提示");
}

function OnModifyUsrInfo(result) { }

function OnModifyFriendAlias() {

}

function OnFriendChangeStatus(uid, status) {
    _imInstance.Common.updateUserStatus(uid, status);
    _imInstance.CMD.renderMyFriend();
    _imInstance.CMD.refreshChatWindowHint(uid);
}

function OnFriendMottoChange() {
    if (arguments.length < 2) return;

    var uid = arguments[0];
    for (var i = 2; i < arguments.length; i = i + 2) {
        UpdateUserProfile(uid, arguments[i], arguments[i + 1]);
    }
    _imInstance.CMD.renderMyFriend();
}

function UpdateUserProfile(uid, k, v) {
    v = decodeURI(_imInstance.Common.gb2312ToUTF8(v));
    switch (parseInt(k)) {
        case gFixValue.fvNick:
            {
                _imInstance.Common.updateUserInfo(null, uid, v, null, null, null, null, null, v, null);
                break;
            }
        case gFixValue.fvSign:
            {
                _imInstance.Common.updateUserInfo(null, uid, null, null, v, null, null, null, null, null);
                break;
            }
    }
}

function OnDelFriend(dst, result) {
    var _MySysXml = _imInstance.XmlGenerate.getMySysXml(["From", _imInstance.Profile.UserID],
													["To", dst],
													["Content", ""],
													["Type", gMessagesType.mtDelFriend],
													["IsConfirm", 2],
													["AddTime", _imInstance.IMDateTime.format(new Date().toLocaleString())]);
    _imInstance.CMD.OnRecvMessages(_MySysXml);
}

function OnGetDetailInfo(dst, type, result, age, birthday_type, birthday_month, birthday_day, home_region, home_city, home_province,
						local_region, local_city, local_province, college, company, carrier, home_page, e_mail, mobile_num, telphone,
						auth_type, question, answer) {
    switch (parseInt(result)) {
        case 0:
            {
                local_city = decodeURI(_imInstance.Common.gb2312ToUTF8(local_city));
                local_province = decodeURI(_imInstance.Common.gb2312ToUTF8(local_province));
                switch (parseInt(type)) {
                    case 0:  //自己的详情
                        var _UserDetailXml = _imInstance.XmlGenerate.getUserDetailXml(["id", dst],
																				["age", age],
																				["birthday_type", birthday_type],
																				["birthday_month", birthday_month],
																				["birthday_day", birthday_day],
																				["home_region", home_region],
																				["home_city", home_city],
																				["home_province", home_province],
																				["local_region", local_region],
																				["local_city", local_city],
																				["local_province", local_province],
																				["college", decodeURI(_imInstance.Common.gb2312ToUTF8(college))],
																				["company", decodeURI(_imInstance.Common.gb2312ToUTF8(company))],
																				["carrier", carrier],
																				["home_page", home_page],
																				["e_mail", e_mail],
																				["mobile_num", decodeURI(_imInstance.Common.gb2312ToUTF8(mobile_num))],
																				["telphone", decodeURI(_imInstance.Common.gb2312ToUTF8(telphone))],
																				["auth_type", auth_type],
																				["question", decodeURI(_imInstance.Common.gb2312ToUTF8(question))],
																				["answer", decodeURI(_imInstance.Common.gb2312ToUTF8(answer))]);
                        _imInstance.ProfileEx = _imInstance.Common.getProfileExFromXml($T(_UserDetailXml, "list").item(0))[0];
                        break;
                    case 1: //好友的详情
                        {
                            //注意：此处因为无法知道回应是对应哪个请求，因此必须处理所有有可能在等待回应的请求					
                            _imInstance.EventQueue.ProcessEvent(dst, gEventID.eiAddFriendSuccess, dst);
                            _imInstance.EventQueue.ProcessEvent(dst, gEventID.eiAddFriendAuther, dst);
                            _imInstance.EventQueue.ProcessEvent(dst, gEventID.eiQueryOnlineUserInfo, { uid: dst, City: local_city, Province: local_province });
                            break;
                        }
                }
            }
        default: { break; };
    }
}

function OnMyConfig() {

}