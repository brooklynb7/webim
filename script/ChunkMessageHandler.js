//**********************************
//*消息处理器
//**********************************

var ChunkMessageHandler = function () {
    var uniqueInstance = null;
    var _imInstance = null;
    var _iframeDiv = null;

    var ProcessUrl =
	{
	    OnlineUserSearchUrl: "/Friends/OnlineUserSearch.aspx", //在线好友搜索
	    AddFriendRequestUrl: "/Friends/AddFriendRequest.aspx", //发送认证请求页面
	    AddFriendAnswerUrl: "/Friends/AddFriendAnswer.aspx", //发送认证答案页面
	    RequestConfirmUrl: "/Friends/RequestConfirm.aspx", //发送认证请求页面
	    RequestSendUrl: "/Friends/RequestSend.aspx", //显示添加请求已发送
	    AddFriendSuccessUrl: "/Friends/AddSuccess.aspx", //添加好友成功
	    UserSettingUrl: "/Friends/UserSetting.aspx", //用户设置页面
	    ShowUserInfoUrl: "/Friends/UserInfo.aspx",    //显示用户详情      
	    AddedUrl: "/Friends/RequestConfirm.aspx"    //被添加确认页面  
	}

    function Constructor(imInstance) {
        _imInstance = imInstance;
        return {
            //登录
            setUserLogin: function (UserName, uid, pass, us, cVer, cb) {
                //第一个连接，必须使用iframe，以便于建立长连接	
				Comet.IsInstance(_imInstance).sendRequest(_imInstance.ServerUrl + "/login?account=" + UserName + "&uid=" + uid + "&password=" + pass + "&status=" + us + "&ver=" + cVer + "&domain=" + gRunerDomain + "&v=" + Math.random(), cb);
            },
            //登出
            setUserLogout: function () {

            },
            //返回个人信息的User对象
            getMyUserInfo: function () { },
            //返回个人配置
            getMyConfig: function () {
                var _MyConfigXml = _imInstance.XmlGenerate.getMyConfigXml(["DisType", "1"],
																		  ["OrderType", "2"],
																		  ["ChatSide", "2"],
																		  ["TeamChatSide", "1"],
																		  ["MsgSendKey", "2"],
																		  ["MsgShowTime", "1"],
																		  ["UserPower", "0"],
																		  ["AutoShowMsg", "2"]);
                _imInstance.Config = _imInstance.Common.getConfigFromXml($T(_MyConfigXml, "list").item(0));
            },
            //返回好友列表的简要信息
            getMyFriendList: function (cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/FriendList?uid=" + _imInstance.Profile.UserID + "&version=0" + "&v=" + Math.random() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回好友信息的User对象
            getFriendUserInfo: function (uid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/GetUsrInfo?uid=" + _imInstance.Profile.UserID + "&num=1&uid0=" + uid + "&v=" + Math.random() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回好友信息的扩展详情
            getFriendDetailInfo: function (uid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/GetDetailUsrInfo?uid=" + _imInstance.Profile.UserID + "&dst=" + uid + "&type=1&v=" + Math.random() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回好友的全部信息
            getFriendUserFullInfo: function (uid, cb) {
                this.getFriendUserInfo(uid, cb);
                this.getFriendDetailInfo(uid, cb);
            },
            //返回分组列表的Group对象数组
            getMyGroupList: function (cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/TeamList?uid=" + _imInstance.Profile.UserID + "&version=0" + "&v=" + Math.random() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回群列表的Team对象数组
            getMyTeamList: function (cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: gDeployPath + gAjaxProxyNoEncodeUrl + gTeamServer + "/AjaxHandler/GetMyTeam.asp?uid=" + _imInstance.Profile.UserID,
                    callback: (function (o) {
                        if (!o) return;
                        if (!$T(o.responseXML, "list")) return;
                        _imInstance.Team = _imInstance.Common.getTeamFromXml($T(o.responseXML, "list").item(0));
                        if (cb) cb(o);
                    }),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回指定群的成员列表数组对象
            getMyTeamUserList: function (tid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: gDeployPath + gAjaxProxyNoEncodeUrl + gTeamServer + "/AjaxHandler/GetTeamUser.asp?tid=" + tid,
                    callback: (function (o) {
                        if (!o) return;
                        if (!$T(o.responseXML, "list")) return;
                        _imInstance.Common.addTeamUserFromXml($T(o.responseXML, "list").item(0));
                        if (cb) cb(o);
                    }),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回消息列表msg对象数组
            getMyMsgList: function (cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: gDeployPath + gAjaxProxyNoEncodeUrl + gTeamServer + "/AjaxHandler/GetMyMsg.asp?uid=" + _imInstance.Profile.UserID + "&code=" + _imInstance.Sys.Code,
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //用户进入MINI聊天室
            addUserIntoMiniTeamUsers: function (tid, userid, nick, cb) {
                _imInstance.ReqQueue.add({ method: 'POST',
                    uri: gDeployPath + "/Ashx/GetChunkContent.ashx?IsEncode=0&RequestUrl=" + gTeamServer + "/AjaxHandler/GetTeamMessage.ashx",
                    params: "para=" + encodeURIComponent("op=AddUserIntoMiniTeamChat&tid=" + tid + "&userid=" + userid + "&nick=" + encodeURIComponent(nick)),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //用户退出聊天室
            killCometClient: function (nick, cb) {
                _imInstance.ReqQueue.add({ method: 'POST',
                    uri: gDeployPath + "/Ashx/GetChunkContent.ashx?IsEncode=0&RequestUrl=" + gTeamServer + "/Ashx/ChatRoom.ashx",
                    params: "para=" + encodeURIComponent("op=KillCometClient&usernick=" + encodeURIComponent(nick)),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //返回我的详细资料
            getMyDetailInfo: function (cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/GetDetailUsrInfo?uid=" + _imInstance.Profile.UserID + "&dst=" + _imInstance.Profile.UserID + "&type=0&v=" + Math.random() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //获取好友别名
            getFriendsAlias: function () {
                //将投递全部好友的查询请求
                for (var i = 0; i < _imInstance.Friend.length; i++) {
                    var u = _imInstance.Friend[i];
                    if ((u) || (u.UserID != -1)) {
                        _imInstance.ReqQueue.add({ method: 'GET', //"POST"
                            uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/GetFriendAlias?uid=" + _imInstance.Profile.UserID + "&dst=" + u.UserID + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                            retry: 1,
                            timeout: 3000
                        });
                        _imInstance.ReqQueue.flush();
                    }
                }
                _imInstance.CMD.renderMyFriend();
            },
            //添加好友
            addFriend: function (uid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/AddFriend?left=" + _imInstance.Profile.UserID + "&right=" + uid + "&flag=1" + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //添加好友至黑名单
            blacklistFriend: function (uid, cb) {
                EventQueue.IsInstance().RegisterEvent(uid, gEventID.eiAddFriendSuccess,
					function (Data) {
					    _imInstance.CMD.renderMyFriend();
					});
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/AddFriend?left=" + _imInstance.Profile.UserID + "&right=" + uid + "&flag=4" + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //发送添加好友请求
            sendAddFriendRequest: function (uid, requestMsg) {
                //添加好友是向对方发送一条Type为96的普通消息
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/SendMsg?type=" + gMessagesType.MSG_AUTH_REQ + "&src=" + _imInstance.Profile.UserID + "&dst=" + uid + "&content=" + requestMsg.escapeEx() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //回答对方认证问题
            sendAddFriendAnswer: function (uid, answer) {
                //添加好友是向对方发送一条Type为99的普通消息
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/SendMsg?type=" + gMessagesType.MSG_AUTH_ANSWER + "&src=" + _imInstance.Profile.UserID + "&dst=" + uid + "&content=" + answer.escapeEx() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改用户签名
            updateUserSign: function () {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyUsrInfo?uid=" + _imInstance.Profile.UserID + "&num=1&key0=2&value0=" + _imInstance.Profile.UserSign.escapeEx("GB2312") + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改用户状态
            changeUserStatus: function (us) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ChangeStatus?uid=" + _imInstance.Profile.UserID + "&status=" + us + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改用户Profile
            updateUserProfile: function () {
                if (arguments.length < 3) return;
                if ((arguments.length % 2) != 0) return;

                /*var PostUrl = gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyUsrInfo?uid="+_imInstance.Profile.UserID+"%26num="+(arguments.length / 2);
                var j = 0;
                for (var i = 0; i < arguments.length; i=i+2)
                {
                PostUrl+=("&key"+j+"="+arguments[i]+"&value"+j+"="+arguments[i+1]).escapeEx();
                j++;
                }*/

                //由于服务器只支持一次发送五个字段，以下代码共修改9个字段，分两次发送
                var PostUrl = gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyUsrInfo?uid=" + _imInstance.Profile.UserID + "%26num=5" + "%26sid=" + _imInstance.ServerSessionID;
                var j = 0;
                for (var i = 0; i < 10; i = i + 2) {
                    PostUrl += ("%26key" + j + "=" + arguments[i] + "%26value" + j + "=" + String(arguments[i + 1]).escapeEx("GB2312"));
                    j++;
                }
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: PostUrl,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();

                j = 0;
                var PostUrl = gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyUsrInfo?uid=" + _imInstance.Profile.UserID + "%26num=4" + "%26sid=" + _imInstance.ServerSessionID;
                for (var i = 10; i < arguments.length; i = i + 2) {
                    PostUrl += ("%26key" + j + "=" + arguments[i] + "%26value" + j + "=" + String(arguments[i + 1]).escapeEx("GB2312"));
                    j++;
                }
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: PostUrl,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //发送消息
            sendMessage: function (msg) {
                _imInstance.ReqQueue.add({ method: 'POST',
                    uri: gDeployPath + gAjaxProxyUrl + (_imInstance.ServerUrl + "/SendMsg?type=" + msg.Type + "%26src=" + msg.From + "%26dst=" + msg.To + "%26content=" +
                    _imInstance.XmlGenerate.getIMMsgXml(msg.Content).escapeEx() + "%26flag=0%26sid=" + _imInstance.ServerSessionID),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //发送群消息
            sendTeamMessage: function (msg) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: gDeployPath + gAjaxProxyNoEncodeUrl + gTeamServer + "/AjaxHandler/service.asp?t=13%26from=" + msg.From + "%26to=" + msg.To + "%26content=" + msg.Content.escapeEx() + "%26type=" + msg.Type + "%26sid=" + _imInstance.ServerSessionID,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },

            //取得离线消息
            getOfflineMessage: function (num) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/GetOfflineMsg?uid=" + _imInstance.Profile.UserID + "&num=" + num + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //接受添加好友请求
            acceptAddFriend: function (uid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/SendMsg?type=" + gMessagesType.MSG_AUTH_ACCEPTED_ADD_REQ + "&src=" + _imInstance.Profile.UserID + "&dst=" + uid + "&content=" + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //接受加入群组请求
            acceptJoinTeam: function (tid, uid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: _imInstance.ServerUrl + "/AjaxHandler/service.asp?t=15&tid=" + tid + "&uid=" + uid,
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //删除好友
            deleteFriend: function (uid) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/DelFriend?left=" + _imInstance.Profile.UserID + "&right=" + uid + "&flag=1" + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改屏蔽状态
            blockFriend: function (uid, status) {
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: _imInstance.ServerUrl + "/AjaxHandler/service.asp?t=7&to=" + uid + "&s=" + status,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改好友昵称
            editCustomName: function (uid, name) {
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyFriendAlias?uid=" + _imInstance.Profile.UserID + "&dst=" + uid + "&key=1&value=" + name + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改好友分组
            editUserGroup: function (uid, gid) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ChangeTeam?uid=" + _imInstance.Profile.UserID + "&dst=" + uid + "&groupid=" + gid + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //添加组
            addGroup: function (gname, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyTeamInfo?uid=" + _imInstance.Profile.UserID + "&type=0&groupid=" + (_imInstance.Data.genGroupId()) + "&groupname=" + gname.escapeEx() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //删除
            delGroup: function (gid, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyTeamInfo?uid=" + _imInstance.Profile.UserID + "&type=1&groupid=" + gid + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //修改
            editGroup: function (gid, gname, cb) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/ModifyTeamInfo?uid=" + _imInstance.Profile.UserID + "&type=2&groupid=" + gid + "&groupname=" + gname.escapeEx() + "&sid=" + _imInstance.ServerSessionID).escapeURL(),
                    callback: cb,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            //设置分组顺序
            setGroupOrder: function (order) {
                _imInstance.ReqQueue.add({ method: 'GET',
                    uri: (gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/SetTeamOrder?uid=" + _imInstance.Profile.UserID + "&sid=" + _imInstance.ServerSessionID + "&szorders=" + order).escapeURL(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            updateMyPosition: function (p1, p2, status) {
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: gTeamServer + "/AjaxHandler/updatePosition.asp?uid=" + _imInstance.Profile.UserID + "&nick=" + _imInstance.Profile.UserNick + "&p1=" + p1 + "&p2=" + p2 + "&status=" + status,
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            enterTeam: function (p1, p2, us, usernick) {
                this.updateMyPosition(p1, p2, us);
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: gTeamServer + "/AjaxHandler/enterTeam.asp?uid=" + _imInstance.Profile.UserID + "&p1=" + p1 + "&p2=" + p2 + "&us=" + us + "&nick=" + usernick.escapeEx(),
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
            },
            keepLive: function () {
                _imInstance.ReqQueue.add({ method: 'GET', //POST
                    uri: gDeployPath + gAjaxProxyUrl + _imInstance.ServerUrl + "/Keepalive?uid=" + _imInstance.Profile.UserID + "%26sid=" + _imInstance.ServerSessionID,
                    callback: function () { if (gDebugLog) { _imInstance.Logger.WriteLog(new Date().toLocaleString() + ": Sent OnKeepalive Package."); } },
                    retry: 1,
                    timeout: 3000
                });
                _imInstance.ReqQueue.flush();
                if (gDebugLog) { _imInstance.Logger.WriteLog(new Date().toLocaleString() + ": Sending OnKeepalive Package."); }
            },
            getProcessUrl: function (UrlName) {
                return gPageServer + ":" + gPageServerPort + ProcessUrl[UrlName];
            }
        }
    }

    return {
        IsInstance: function (imInstance) {
            if (uniqueInstance == null) {
                uniqueInstance = Constructor(imInstance);
            }
            return uniqueInstance;
        }
    }
} ();