﻿var Comet = function () {
    var uniqueInstance = null;
    var _ScriptPath = null;
    var _imInstance = null;
    var _handlerScript = null;
    var _ConnectionHTML = null;
    var _iConnectionFrame = null;
    var _iFrameID = "ConnectionFrame";
    var handlerScript = "/script/ChunkMessageHandleCallBack.js?Ver=" + gVer;
    var _RequestUrl = "";

    function linkHandlerScript(_parent, cb) {
        CreateScript(_parent, _ScriptPath + handlerScript, cb);
    }

    function Constructor(imInstance) {
        _imInstance = imInstance;
        return {
            initialize: function (cb) {
                linkHandlerScript(window.document, cb); //需要提前载入处事脚本，防止回调命令已经到达而处理脚本未就绪
                var userAgent = navigator.userAgent.toLowerCase();
                if (/msie/.test(userAgent) && !/opera/.test(userAgent)) {
                    // For IE browsers		
                    _ConnectionHTML = new ActiveXObject("htmlfile");
                    _ConnectionHTML.open();
                    _ConnectionHTML.write("<html>");
                    _ConnectionHTML.write("<script>document.domain = '" + gRunerDomain + "';<\/script>");
                    //_ConnectionHTML.write("<script type='text/javascript' src='" + gScriptServer + "/script/WebIMConst.js?Ver=" + gVer +"'><\/script>");//引入常量
                    //_ConnectionHTML.write("<script type='text/javascript' src='" + gScriptServer + "/script/WebIMBaseHelper.js?Ver=" + gVer +"'><\/script>");//需要用到通用函数	
                    //_ConnectionHTML.write("<script type='text/javascript' src='" + gScriptServer + "/script/StyleManager.js?Ver=" + gVer +"'><\/script>");//引用样式管理器	
                    _ConnectionHTML.write("</html>");
                    _ConnectionHTML.close();
                    _iConnectionFrame = _ConnectionHTML.createElement("iframe");
                    _iConnectionFrame.id = _iFrameID;
                    _iConnectionFrame.style.visibility = 'hidden';
                    _ConnectionHTML.appendChild(_iConnectionFrame);
                    _ConnectionHTML.parentWindow.Comet = Comet;
                    CreateScript(_ConnectionHTML, gScriptServer + "/script/WebIMConst.js?Ver=" + gVer); //引入常量
                    CreateScript(_ConnectionHTML, gScriptServer + "/script/StyleManager.js?Ver=" + gVer); //引用样式管理器
                    CreateScript(_ConnectionHTML, gScriptServer + "/script/WebIMBaseHelper.js?Ver=" + gVer); //需要用到通用函数
                    linkHandlerScript(_ConnectionHTML, cb);
                }

                if (/mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent)) {
                    // For Firefox browser										
                    _ConnectionHTML = window;
                    var _iFrameDiv = WebElement.New('div', '_iFrameDiv', '');
                    with (_iFrameDiv.style) {
                        left = top = "-100px";
                        height = width = "1px";
                        visibility = "hidden";
                        display = 'none';
                    }
                    _iConnectionFrame = WebElement.New('iframe', _iFrameID, '');
                    _iConnectionFrame.src = '';
                    _iConnectionFrame.onunload = this.onUnload();
                    WebElement.Add(_iFrameDiv, _iConnectionFrame);
                    WebElement.Add(_iFrameDiv);
                }

                if (/webkit/.test(userAgent) || /opera/.test(userAgent)) {
                    //for other browsers
                    _ConnectionHTML = window;
                    _iConnectionFrame = document.createElement('iframe');
                    _iConnectionFrame.setAttribute('id', _iFrameID);
                    _iConnectionFrame.setAttribute('src', _RequestUrl);
                    with (_iConnectionFrame.style) {
                        position = "absolute";
                        left = top = "-100px";
                        height = width = "1px";
                        visibility = "hidden";
                    }
                    document.body.appendChild(_iConnectionFrame);
                }
            },

            sendRequest: function (_Url, cb) {
                _RequestUrl = _Url;
                _iConnectionFrame.src = _RequestUrl;
            },

            getIMInstance: function () { return _imInstance; },
            getConnection: function () { return _Connection; },
            onUnload: function () {
                if (_iConnectionFrame) {
                    _iConnectionFrame.src = '#';
                    //_iConnectionFrame = null; // release the iframe to prevent problems with IE when reloading the page
                }
            }
        }
    }

    return {
        IsInstance: function (imInstance, dpath) {
            if (uniqueInstance == null) {
                uniqueInstance = Constructor(imInstance);
                _ScriptPath = dpath;
                //uniqueInstance.initialize();				
            }
            return uniqueInstance;
        }
    }
} ();