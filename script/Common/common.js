﻿/*----------------------------------------
通用的分页页码显示脚本
----------------------------------------*/
function getPageCount(records, pagesize) {
    var pages = Math.floor(records / pagesize);

    if (records % pagesize > 0)
        pages++;
    return pages;
}
function MakePageNav(recordcount, pagesize, currentpage, func, pageNavDiv) {
    if (pageNavDiv == "" || pageNavDiv == null)
        pageNavDiv = "pageNav";
    var pagecount = getPageCount(recordcount, pagesize);
    if (pagecount == 1) {
        document.getElementById(pageNavDiv).innerHTML = "";
        return;
    }
    var pagecountforshow = 10;
    var page = document.getElementById(pageNavDiv);
    page.innerHTML = "";
    var midpage = parseInt(pagecountforshow / 2);
    var startpage = 1;
    if (currentpage > midpage) {
        startpage = currentpage - midpage;
    }
    if (startpage + pagecountforshow - 1 > pagecount) {
        startpage = pagecount - pagecountforshow + 1;
    }
    if (startpage < 1) {
        startpage = 1;
    }
    var endPage = startpage + pagecountforshow - 1;
    if (endPage > pagecount) {
        endPage = pagecount;
    }
    if (startpage != 1) {
        var firstpage = document.createElement("a");
        firstpage.href = "javascript:" + func + "(1);";
        firstpage.innerHTML = "«";
        firstpage.title = "到首页";
        page.appendChild(firstpage);
    }
    for (var i = startpage; i <= endPage; i++) {
        if (i == currentpage) {
            var cpage = document.createElement("span");
            cpage.innerHTML = i;
            page.appendChild(cpage);
        }
        else {
            var other = document.createElement("a");
            other.href = "javascript:" + func + "(" + i + ");"
            other.innerHTML = i;
            page.appendChild(other);
        }
    }
    if (endPage != pagecount) {
        var lastpage = document.createElement("a");
        lastpage.href = "javascript:" + func + "(" + pagecount + ");"
        lastpage.innerHTML = "»";
        lastpage.title = "到尾页";
        page.appendChild(lastpage);
    }

    var clearDiv = document.createElement("div");
    clearDiv.style.clear = "both";
    page.appendChild(clearDiv);
    return page;
}

//获取字符串汉字长度
function getLength(content) {
    var l = 0;
    var a = content.split("");
    for (var i = 0; i < a.length; i++) {
        if (a[i].charCodeAt(0) < 299) {
            l++;
        }
        else {
            l += 2;
        }
    }
    return l;
}

///根据url获取参数值
function getQueryString(name) {
    var reg = new RegExp("(^|\\?|&)" + name + "([^&]*)(\\s|&|$)", "i");
    var value = "";
    if (reg.test(location.href)) {
        value = decodeURIComponent(RegExp.$2.replace(/\+/g, " "));
        value = value.substr(1, value.length - 1);
    }
    return value;
}