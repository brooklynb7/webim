//**********************************
//*回调事件队列
//**********************************
function IMEvent(_id,_type,_cb,_bfdata,_afdata) 
{
	this.Id = _id;
	this.Type = _type;
	this.Callback = _cb;
	this.BeforeData = _bfdata;
	this.AfterData = _afdata;
}

var EventQueue = function()
{	
	var uniqueInstance = null;
	var _EventList = new Array(); //回调事件列表
	
	function Constructor()
	{
		return{
			GetEvent:function(Id,Type)//根据用户ID找到对应回调事件
			{
				var obj = null;
				for(var i = 0 ;i < _EventList.length;i++)
				{
					if(_EventList[i].Type==Type)
					{
						if(null!=Id) 
						{							
							if(_EventList[i].Id==Id){obj = _EventList[i];}
						}else{ //如果Id为null，则只匹配Type
							obj = _EventList[i];
						}
						break;
					}
				}
				return obj;
			},
			  
			RegisterEvent:function(_id,_type,_cb,_bfdata,_afdata)
			{
				//同一id和type，只允许注册一个事件
				for(var i = 0 ;i < _EventList.length;i++)
				{	
					if((_EventList[i].Id==_id)&&(_EventList[i].Type==_type))
					{
						_EventList[i].Callback==_cb;
						_EventList[i].BeforeData==_bfdata;
						_EventList[i].AfterData==_afdata;
						return;
					}				
				}
				var _IMEvent = new IMEvent(_id,_type,_cb,_bfdata,_afdata);
				_EventList.add(_IMEvent);
			},
			  
			RemoveEvent:function(Id,Type)
			{
				var _IMEvent = this.GetEvent(Id,Type);
				_EventList.remove(_IMEvent);
			},
			  
			GetEventList:function(){return _EventList;},
			ProcessEvent:function(Id,Type,Data)
			{				
				var _IMEvent = this.GetEvent(Id,Type);				
				if(_IMEvent)
				{
					_IMEvent.Callback(Data);
					this.RemoveEvent(Id,Type);
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	
	return{
		IsInstance:function()
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
			}
			return uniqueInstance;
		}
	}
}();