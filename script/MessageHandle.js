//**********************************
//*消息处理器
//**********************************

var MessageHandle = function()
{
	var uniqueInstance = null;
	var _MessageList = new Array(); //消息列表
	
	function Constructor()
	{
		GetUserMessageByUserID = function(UserID)//根据用户ID找到对应的头像
		{
			var obj = null;
			for(var i = 0 ;i < _MessageList.length;i++)
			{
				if(_MessageList[i].To==UserID)
				{
					obj = _MessageList[i];
					break;
				}
			}
			return obj;
		},
		  
		AcceptMessage = function(msg)
		{
			var _IMMessage = new IMMessage(msg.Type,msg.From,msg.To,msg.Content,msg.IsConfirm,msg.AddTime);
			_MessageList.add(_IMMessage);
		},
		  
		RemoveMessage = function(msg)
		{
			_IMMessage.remove(msg);
		},
		  
		GetMessageList = function(){return _MessageList;}
	}
	
	return{
		IsInstance:function()
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
			}
			return uniqueInstance;
		}
	}	
}();