//**********************************
//*消息队列
//**********************************
function IMMessage(_type,_code,_from,_to,_content,_isconfirm,_addtime,_processed) 
{
	this.Type = _type;
	this.Code = _code;
	this.From = _from;
	this.To = _to;
	this.Content = _content;
	this.IsConfirm = _isconfirm;
	this.AddTime = _addtime;
	this.Processed = _processed;
}

var MessageQueue = function()
{	
	var uniqueInstance = null;
	var _MessageList = new Array(); //消息列表
	
	function Constructor()
	{
		return{
		GetUserMessageByUserID:function(UserID)//根据用户ID找到对应的头像
		{
			var obj = null;
			for(var i = 0 ;i < _MessageList.length;i++)
			{
				if(_MessageList[i].To==UserID)
				{
					obj = _MessageList[i];
					break;
				}
			}
			return obj;
		},
		  
		AcceptMessage:function(msg)
		{
			_MessageList.add(new IMMessage(msg.Type,msg.Code,msg.From,msg.To,msg.Content,msg.IsConfirm,msg.AddTime,false));
		},
		  
		RemoveMessage:function(msg)
		{
			_MessageList.remove(msg);
		},
		
		ProcessMessage:function(msg)
		{
			for(var i = 0 ;i < _MessageList.length;i++)
			{							
				if(_MessageList[i]==msg)
				{		
					_MessageList[i].Processed=true;
					break;
				}
			}			
		},		
		
		ClearProcessedMessage:function()
		{
			for(var i = 0 ;i < _MessageList.length;i++)
			{
				if(_MessageList[i].Processed==true)
				{
					_MessageList.remove(_MessageList[i]);
					i--;
				}
			}			
		},
		
		ClearMessageByType:function(type)
		{
			for(var i = 0 ;i < _MessageList.length;i++)
			{
				if(_MessageList[i].Type==type)
				{
					_MessageList.remove(_MessageList[i]);
					i--;
				}
			}			
		},	
		
		ClearLastMessage:function(msg)
		{
			for(var i = 0 ;i < _MessageList.length;i++)
			{
				if(_MessageList[i].Type==msg.Type&&_MessageList[i].Code==msg.Code&&
				   _MessageList[i].From==msg.From&&_MessageList[i].To==msg.To&&
				   _MessageList[i].AddTime==msg.AddTime)
				{
					_MessageList.remove(_MessageList[i]);
					break;
				}
			}			
		},		
		  
		GetMessageList:function(){return _MessageList;}
		}	
	}
	
	return{
		IsInstance:function()
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
			}
			return uniqueInstance;
		}
	}
}();