//**********************************
//*窗体管理器
//**********************************
var StyleManager = function() 
{
	var uniqueInstance = null;
	var _DeployPath = null;
	
	this.ResPath = ["/Images/style/01/","/Images/style/02/"];
	this.BigDefaultHeadPath = ["/UserAvatar/female.gif","/UserAvatar/male.gif","/UserAvatar/no.gif"];
	this.ChatUserHeadPath = ["/Images/style/01/ChatUser0.gif", "/Images/style/01/ChatUser1.gif", "/Images/style/01/ChatUser2.gif"];
	this.EmotionPath = ["/Emotion/emo_0/"];
	
	function Constructor()
	{
		return{	
			getStyleResPath:function(styleID)
			{	
				return _DeployPath+ResPath[styleID-1];
			},
			getBigDefaultHeadPath:function(gender)
			{
				if (gender==255){gender=1;}
				return _DeployPath+BigDefaultHeadPath[gender];
			},
			getChatUserHeadPath:function(gender)
			{
				if (gender==255){gender=1;}
				return _DeployPath+ChatUserHeadPath[gender];		
			},
			getEmotionPath:function(styleID)
			{
				return 	EmotionPath[styleID-1];
			}
		}
	}
	
	return{
		IsInstance:function(dpath)
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
				_DeployPath = dpath;
			}
			return uniqueInstance;
		}
	}	
}();
