﻿function CreateLink(File) {
    var new_element;
    head = document.getElementsByTagName('head').item(0);
    new_element = document.createElement("link");
    new_element.setAttribute("type", "text/css");
    new_element.setAttribute("rel", "stylesheet");
    new_element.setAttribute("href", File);
    void (head.appendChild(new_element));
}
CreateLink("../css/WebChat.css");
var webchat_AllContent = "";
webchat_AllContent =
'<div id="webChatDiv"> '+
'    <div id="webChat_c" class="fixed-bottom fixed-right c"> '+
'        <div id="popMsg" style="display: none"></div> '+
'        <div id="miniChat" style="display: block"> '+
'            <div class="chatView" style="display: block" id="miniChat_chatView"> '+
'                <ul style="margin-top: 0px"> '+
'                    <li id="miniChat_view"> ' +
'                        <img src="../Images/webchat/Loading.gif" /><span style="padding-left:5px">loading......</span> '+
'                    </li> '+
'                </ul> '+
'            </div> '+
'            <div class="miniBar" id="miniChat_miniBar" style="display: block"> '+
'                <a title="收起聊天室" href="javascript:closeMiniBar();" class="Indent_bt"></a> '+
'                <div class="logo" onclick="showChat();"></div>         '+
'                <span title="关闭" class="close" onclick="closeMiniBar();" onmouseout="this.className=\'close\'" onmousedown ="this.className=\'close closeDown\'" '+
'                 onmouseup ="this.className=\'close closeHover\'" onmouseover="this.className=\'close closeHover\'"></span> '+
'                <span title="最大化" class="zoom" onclick="showChat();" onmouseout="this.className=\'zoom\'" onmousedown ="this.className=\'zoom zoomDown\'" '+
'                 onmouseup ="this.className=\'zoom zoomHover\'"   onmouseover="this.className=\'zoom zoomHover\'"></span> '+
'                <div onmouseover="this.className=\'pp_ico pp_icoHover\'" onmouseout="this.className=\'pp_ico\'" onmousedown ="this.className=\'pp_ico pp_icoDown\'" '+
'                 onmouseup ="this.className=\'pp_ico pp_icoHover\'" class="pp_ico"></div> '+
'                <h4 style="margin: 0px" onclick="showChat();" id="miniBar_roomName">聊天室</h4> '+
'            </div> '+
'            <a href="javascript:void(0)" class="del" id="miniChat_del" style="display: block;" onclick="closeMiniChatView();"></a> '+
'            <div class="Expand_Box" id="miniChat_expand" style="display: none;"> '+
'                <a class="Expand_bt expandbt_tp1" onclick="showChat();" title="展开聊天室" style="display: block;"></a> '+
'            </div>                  '+
'        </div>          '+
'        <div id="webChatRoom" style="height: 340px; width: 395px; /*left: -393px;*/ top: -343px; display: none">   '+
'             <div id="webChatRoom_header" style="cursor: move">   '+
'                    <div class="logo"></div>     '+
'                <span title="关闭" onclick="closeChat();" class="setClose" onmouseover="this.className=\'setClose setCloseHover\'" onmouseout="this.className=\'setClose\'"     '+
'                                  onmousedown="this.className=\'setClose setCloseDown\'" onmouseup="this.className=\'setClose setCloseHover\'"></span>                       '+
'              <span title="最小化" class="setMini" onclick="showMiniBar();" onmouseover="this.className=\'setMini setMiniHover\'" onmouseout="this.className=\'setMini\'"    '+
'                            onmousedown="this.className=\'setMini setMiniDown\'" onmouseup="this.className=\'setMini setMiniHover\'"></span>  '+
'             <div class="beforeLogin">       '+
'                <span onmouseover="document.getElementById(\'webChatRoom_header\').style.cursor =\'default\'" onclick="showChangeName()"    '+
'                              onmouseout ="document.getElementById(\'webChatRoom_header\').style.cursor =\'move\'" id="webChat_uNick">    '+
'                              <input type="text" maxlength="10" style="display: none;">   '+
'                              <u></u></span>    '+
'           </div>                     '+
'     </div>                  '+
'      <div id="webChat_container" style="overflow: hidden;display:none">   '+
'          <div class="chat">                    '+
'            <div class="groupMember" style="height: 275px;">   '+
'                 <div class="infoLinkWrapper">            '+
'                             <a target="_blank" href="http://et.sdo.com">     '+
'                                 官网</a>        '+
'                         </div>             '+
'                        <div class="groupTitle">     '+
'                             <span id="chatRoom_count">1</span>人参与聊天</div>  '+
'                        <div class="memberList" style="height: 240px;">         '+
'                            <h3 class="dropDown" onclick="changeDrop(this);">  '+
'                                游客<span id="chatRoom_gCount">(3)</span></h3>    '+
'                             <ul id="memberList_guest">         '+
'                            </ul>               '+
'                        </div>          '+
'                    </div>           '+
'                    <div class="chatArea">        '+
'                         <div class="chatBody" style="height: 239px">           '+
'                             <ul id="chatArea_chatView" style="height: 239px">     '+
'                             </ul>                                                 '+
'                            <div id="webChat_alerMsgCon" class="alerMsgCon" style="display: none;">     '+
'                                <div class="alertMessage">                     '+
'                                    <p>                        '+
'                                        <a href="javascript:void(0);" class="del" onclick="document.getElementById(\'webChat_alerMsgCon\').style.display=\'none\'"></a><span class="icon"></span>请输入内容后发送。</p>      '+
'                                </div>    '+
'                            </div>      '+
'                        </div>         '+
'                        <div>           '+
'                            <div class="chatInput">  '+
'                                <input  id="webChat_content" type="text" onkeypress="return webChat_sendMsg_onKeyPress(event)" maxlength="140" onfocus="if(this.value==\'请在这里输入内容\')this.value=\'\'" value="请在这里输入内容" />     ' +
'                            </div>           '+
'                        </div>          '+
'                        <div class="buttonArea">   '+
'                           <span class="send" onclick="webChat_sendMsg();" title="点击发送消息" onmousedown="this.className=\'sendDown\'" onmouseup="this.className=\'sendHover\'"   '+
'                                 onmouseover="this.className=\'sendHover\'" onfocus="this.value=this.value" onmouseout="this.className=\'\'">发送</span>    '+
'                       </div>    '+
'                   </div>        '+
'               </div>         '+
'           </div>         '+
'          <div id="webChat_loading" style="width: 395px; height: 340px; display: block;">      '+
'              <p>               '+
'                  <img src="../Images/webchat/Loading.gif"></img> 正在加载,请稍候...</p>  '+
'          </div>   '+
'      </div>  '+
'   </div>    '+
'</div>';
//document.write(webchat_AllContent);

var webchat_headerDiv = document.getElementById("webChatRoom_header");
var webchat_div = document.getElementById("webChatRoom");
var webchat_orig_x = parseInt(webchat_div.style.left) - document.body.scrollLeft;
var webchat_orig_y = parseInt(webchat_div.style.top) - document.body.scrollTop;

function closeMiniChatView() {
    document.getElementById("miniChat_del").style.display = 'none';
    document.getElementById("miniChat_chatView").style.display = 'none';
}
function closeMiniBar() {
    document.getElementById("miniChat_del").style.display = 'none';
    document.getElementById("miniChat_chatView").style.display = 'none';
    document.getElementById("miniChat_miniBar").style.display = 'none';
    document.getElementById("miniChat_expand").style.display = 'block';
}
function showChat() {
    //webchat_div.setAttribute("style", "left:" + webchat_orig_x + "px;top:" + webchat_orig_y + "px");
    document.getElementById("webChatRoom").style.display = 'block';
    document.getElementById("miniChat_del").style.display = 'none';
    document.getElementById("miniChat_chatView").style.display = 'none';
    document.getElementById("miniChat_miniBar").style.display = 'none';
    document.getElementById("miniChat_expand").style.display = 'none';
    document.getElementById("chatArea_chatView").scrollTop = document.getElementById("chatArea_chatView").scrollHeight;
}
function closeChat() {
    webchat_div.style.top = null;
    webchat_div.style.left = null;
    document.getElementById("miniChat_expand").style.display = 'block';
    document.getElementById("webChatRoom").style.display = 'none';
}
function showMiniBar() {
    webchat_div.style.top = null;
    webchat_div.style.left = null;
    document.getElementById("miniChat_del").style.display = 'block';
    document.getElementById("miniChat_chatView").style.display = 'block';
    document.getElementById("miniChat_miniBar").style.display = 'block';
    document.getElementById("webChatRoom").style.display = 'none';
}
function showChangeName() {
    var nick = document.getElementById("webChat_uNick").getElementsByTagName("u").item(0);
    var nick_input = document.getElementById("webChat_uNick").getElementsByTagName("input").item(0);
    nick.style.display = "none";
    if (nick_input.style.display == 'none') {
        nick_input.value = nick.innerHTML;
    }
    nick_input.style.display = "block";
}
function closeChangeName() {
    var nick = document.getElementById("webChat_uNick").getElementsByTagName("u").item(0);
    var nick_input = document.getElementById("webChat_uNick").getElementsByTagName("input").item(0);
    nick.style.display = "none";
    if (nick_input.style.display == 'none') {
        nick_input.value = nick.innerHTML;
    }
    nick_input.style.display = "block";
}
function changeDrop(obj) {
    if (obj.className == 'dropDown') {
        obj.className = 'dropRight';
        document.getElementById('memberList_guest').style.display = 'none';
    }
    else {
        obj.className = 'dropDown';
        document.getElementById('memberList_guest').style.display = 'block';
    }
}

document.domain = "sdo.com";
var webchat_sid = null;
var webchat_userNick = null;
var webchat_userCount = null;
var webchat_keepAlive = null;
var webchat_server = "webim2.sdo.com"; //"222.73.62.154";

/*服务器推送代码--开始*/

//进入聊天室
function OnGuestEnterRoom(Result, RoomID, UserID, UserNick, SessionID) {
    //Result:0 成功,-1失败
    if (Result == 0) {
        webchat_sid = SessionID;
        webchat_userNick = decodeURIComponent(UserNick);
        //document.getElementById("miniBar_roomName").innerHTML = RoomID + "聊天室";
        document.getElementById("webChat_uNick").getElementsByTagName("u").item(0).innerHTML = webchat_userNick;
        document.getElementById("webChat_container").style.display = "block";
        document.getElementById("webChat_loading").style.display = "none";
        webchat_keepAlive = setInterval(function () {webChat_KeepAlive() }, 10000);
    }
    else {
        return;
    }
}

//获取聊天室数量、成员
function OnRoomMemberList(RoomId, Count) {
    document.getElementById("miniChat_view").innerHTML = Count + '人正在参与聊天';
    document.getElementById("chatRoom_count").innerHTML = Count;
    document.getElementById("chatRoom_gCount").innerHTML = "(" + Count + ")";
    if (arguments.length > 2) { //[UserID,UserNick, UserGender]
        var str = "";
        for (var i = 2; i < arguments.length - 2; i = (i + 3)) {
            var c = "available ";
            if (decodeURIComponent(arguments[i + 1]) == webchat_userNick) {
                c += "self";
            }
            str += '<li onmouseover="this.className=\'listHover\'" onmouseout="this.className=\'\'" class=""><a title="' + decodeURIComponent(arguments[i + 1]) + '" href="javascript:void(0)" class="' + c + '">' + decodeURIComponent(arguments[i + 1]) + '</a></li>';
        }
        document.getElementById("memberList_guest").innerHTML = str;
    }
}

//用户进入通知
function OnUserEnterRoom(RoomID, Count, UserID, UserNick, UserGender) {
    document.getElementById("chatRoom_count").innerHTML = Count;
    document.getElementById("chatRoom_gCount").innerHTML = "(" + Count + ")";
    document.getElementById("memberList_guest").innerHTML +=
    '<li class=""><a title="' + decodeURIComponent(UserNick) + '" href="javascript:void(0)" class="available">' + decodeURIComponent(UserNick) + '</a></li>';
}

//用户离开通知
function OnUserLeaveRoom(RoomID, Count, UserID, UserNick) {
    document.getElementById("chatRoom_count").innerHTML = Count;
    document.getElementById("chatRoom_gCount").innerHTML = "(" + Count + ")";
    var gList = document.getElementById("memberList_guest");
    for (var i = 0; i < gList.childNodes.length; i++) {
        if (gList.childNodes.item(i).firstChild.innerHTML == decodeURIComponent(UserNick)) {
            gList.removeChild(gList.childNodes.item(i));
        }
    }
}

//接收消息
function OnRoomMessage(RoomID, From, SendTime, MsgContent) {
    var sendTimeArray = decodeURIComponent(SendTime).split(':');
    var sendTime = sendTimeArray[0] + ":" + sendTimeArray[1];
    var from = decodeURIComponent(From);
    var receiveMsg = document.createElement("li");
    var msg = decodeURIComponent(MsgContent);
    var c = "visitor";
    if (from == webchat_userNick) {
        c = "self";
    }
    receiveMsg.innerHTML = '<div><a><span class="' + c + '">[' + sendTime + '] ' + from + ':</span></a><p>' + msg + '</p></div>';
    document.getElementById("chatArea_chatView").appendChild(receiveMsg);
    document.getElementById("chatArea_chatView").scrollTop = document.getElementById("chatArea_chatView").scrollHeight;
    document.getElementById("miniChat_view").innerHTML = '<div><strong class="visitor">[' + sendTime + '] ' + from + ':</strong></div><div><span>' + msg + '</span></div>';
}
//接收系统消息
function OnRoomSystemMessage(RoomID, SendTime, MsgContent) {
    document.getElementById("chatArea_chatView").innerHTML += '<li class="chatTips"><b></b><em>友情提示：' + decodeURIComponent(MsgContent) + '。</em></li>';
    document.getElementById("chatArea_chatView").scrollTop = document.getElementById("chatArea_chatView").scrollHeight;
}

//接收更名消息
function OnChangeNick(RoomID, Result, OldNick, NewNick) {
    alert(Result);
}

/*服务器推送代码--结束*/

/*向服务器发送请求--开始*/

//发送消息
function webChat_sendMsg() {
    var text = document.getElementById("webChat_content").value;
    if (text == '' || text == '请在这里输入内容') {
        document.getElementById("webChat_alerMsgCon").style.display = "block";
        setTimeout(function () { document.getElementById("webChat_alerMsgCon").style.display = "none"; }, 1500);
        return;
    }
    else {
        var new_element = document.createElement("script");
        new_element.setAttribute("type", "text/javascript");
        new_element.setAttribute("chartset", "utf-8");
        new_element.setAttribute("src", "http://" + webchat_server + ":9090/SendCRMessage?msg=" + encodeURIComponent(text) + "&sid=" + webchat_sid + "&v=" + Math.random());
        document.body.appendChild(new_element);
        document.getElementById("webChat_content").value = '';
        setTimeout(function () { document.body.removeChild(new_element) }, 5000);
    }
}

//发送更名请求
function webChat_changeName() {
    var text = document.getElementById("webChat_uNick").getElementsByTagName("input").item(0).value;
    if (text == '') {
          return;
    }
    else {
        var new_element = document.createElement("script");
        new_element.setAttribute("type", "text/javascript");
        new_element.setAttribute("chartset", "utf-8");
        new_element.setAttribute("src", "http://" + webchat_server + ":9090/ChangeNick?nick="+encodeURIComponent(text)+"&sid=" + webchat_sid + "&v=" + Math.random());
        document.body.appendChild(new_element);
        setTimeout(function () { document.body.removeChild(new_element) }, 5000);
    }     
}

function webChat_KeepAlive() {
    var new_element = document.createElement("script");
    new_element.setAttribute("type", "text/javascript");
    new_element.setAttribute("chartset", "utf-8");
    new_element.setAttribute("src", "http://" + webchat_server + ":9090/KeepLive?sid=" + webchat_sid + "&v=" + Math.random());
    document.body.appendChild(new_element);
    setTimeout(function () { document.body.removeChild(new_element) }, 5000);
}

/*向服务器发送请求--结束*/

//按回车发送消息
function webChat_sendMsg_onKeyPress(e) {
    var keyCode = null;

    if (e.which)
        keyCode = e.which;
    else if (e.keyCode)
        keyCode = e.keyCode;

    if (keyCode == 13) {
        webChat_sendMsg();
        return false;
    }
    return true;
}

function webChat_changeName_onKeyPress(e) {
    var keyCode = null;

    if (e.which)
        keyCode = e.which;
    else if (e.keyCode)
        keyCode = e.keyCode;

    if (keyCode == 13) {
        webChat_changeName();
        return false;
    }
    return true;
}

//window.onerror = function () { return true; };
document.write('<iframe src="http://' + webchat_server + ':9090/Guest?cid=1&rid=1000&domain=sdo.com&v=' + Math.random() + '" style="display: none" height="0" width="0" scrolling="no"></iframe>');

//拖动聊天框
webchat_headerDiv.onmousedown = function (e) {
    e = e || window.event;
    //左键才可拖动
    if (e.which == null)
        button = (e.button < 2) ? "LEFT" : ((e.button == 4) ? "MIDDLE" : "RIGHT"); // IE case
    else
        button = (e.which < 2) ? "LEFT" : ((e.which == 2) ? "MIDDLE" : "RIGHT"); // All others

    if (button != "LEFT") {
        return false;
    }
    var x = e.clientX + document.body.scrollLeft - webchat_div.offsetLeft;
    var y = e.clientY + document.body.scrollTop - webchat_div.offsetTop;
    //设置捕获范围
    if (webchat_div.setCapture) {
        webchat_div.setCapture();
    }
    else if (window.captureEvents) {
        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
    }
    document.onmousemove = function (e) {
        e = e || window.event;
        var xx = e.clientX + document.body.scrollLeft - x;
        var yy = e.clientY + document.body.scrollTop - y;
        // if (xx < -394) xx = -394;
        if (yy > -343) yy = -343;

        var ax, ay;
        if (typeof (document.compatMode) != 'undefined' && document.compatMode != 'BackCompat') {
            ax = document.documentElement.clientWidth;
        }
        else if (typeof (document.body) != 'undefined') {
            ax = document.body.clientWidth;
        }
        if (typeof (document.compatMode) != 'undefined' && document.compatMode != 'BackCompat') {
            ay = document.documentElement.clientHeight;
        }
        else if (typeof (document.body) != 'undefined') {
            ay = document.body.clientHeight;
        }
        //if (xx > ax) xx = ax-5;
        if (yy > ay) yy = ay;

        webchat_div.style.left = xx + "px";
        webchat_div.style.top = yy + "px";
        webchat_orig_x = parseInt(webchat_div.style.left) - document.body.scrollLeft;
        webchat_orig_y = parseInt(webchat_div.style.top) - document.body.scrollTop;
    };
    document.onmouseup = function () {
        if (webchat_div.releaseCapture)
            webchat_div.releaseCapture();
        else if (window.captureEvents)
            window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
        document.onmousemove = null;
        document.onmouseup = null;
    }
}