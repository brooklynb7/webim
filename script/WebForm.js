//**********************************
//*窗体对象类
//**********************************
window.zIndex = 100;
window.cWindow = "winMain0";

function Chat() {
    this.ContentUrl = gUITempletServer + "/UITemplete/WebChat.htm";
    this.Show = function () {
        var d = document.createElement("div");
        new Ajax().get(gDeployPath + gAjaxProxyNoEncodeUrl + this.ContentUrl, function (o) {
            d.innerHTML = o.responseText;
            document.body.appendChild(d);
        });
    }
}

function SideBar() {
    this.Type = 6;
    this.FormID = gFormSideBar;
    this.Width = 276;
    this.Height = 30;
    this.Top = -2;
    this.Left = 400;
    this.Content = "";           //窗体内容
    this.ContentUrl = gUITempletServer + "/UITemplete/SideBar.html";        //给出网址，Ajax去获取
    this.Moveable = !!0;         //是否允许拖动
    this.ShowCorner = !!0;         //是否显示圆角
    this.OnPaint = null; //可定制的重绘参数，回传4个参数：内容区宽,高,UserID,Type
    this.OnLoaded = null; //窗口加载完毕回调，回传2个参数：UserID,Type
    this.Expanded = !!1;  //展开状态
    var _div = null;
    var _me = this;
    this.Show = function() {
        if (!$("windowContainer")) {//最外层div
            var divCover = WebElement.New("div", "windowCover", "wWindowCover");
            //var divBorder = WebElement.New("div", "windowBorder", "wWindowBorder");
            var divSound = WebElement.New("div", "divSound", "wElemHidden");
            var hideInput = WebElement.New("input", "inputHack", "wElemHidden");
            var divContainerBorder = WebElement.New("div", "windowContainerBorderS", "wContainerBorder");
            var divContainer = WebElement.New("div", "windowSideBarContainer", "wContainerBorder"); //最外层div

            divCover.oncontextmenu = function() { return !!0 };
            WebElement.Add(divContainer, hideInput, divSound, divCover);
            WebElement.Add(divContainerBorder, divContainer);
            WebElement.Add(divContainerBorder);
            //WebElement.Add(divContainer);					
        }
        _div = WebElement.New("div", "winSideBar" + this.FormID, "wSideBarMain");
        var oDivCon = WebElement.New("div", "", "wMainSideBarContent");
        _div.oncontextmenu = function() { return !!0; };
        if (_me.Moveable) {
            _div.onmousedown = function(e) {
                if (_me.Type != 3 && _me._minStatus) return;

                e = e || window.event;
                //左键才可拖动
                if (e.which == null)
                    button = (e.button < 2) ? "LEFT" : ((e.button == 4) ? "MIDDLE" : "RIGHT"); // IE case
                else
                    button = (e.which < 2) ? "LEFT" : ((e.which == 2) ? "MIDDLE" : "RIGHT"); // All others

                if (button != "LEFT") {
                    return false;
                }

                _me.chaLeft = Evt.Left(e) - _me.Left;
                _me.chaTop = Evt.Top(e) - _me.Top;
                //设置捕获范围
                if (_div.setCapture) {
                    _div.setCapture();
                } else if (window.captureEvents) {
                    window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                }

                document.onmousemove = function(e) {
                    _div.style.left = (_me.Left = Evt.Left(e) - _me.chaLeft) + "px";
                    _div.style.top = (_me.Top = (Evt.Top(e) - _me.chaTop) < 0 ? 0 : Evt.Top(e) - _me.chaTop) + "px";
                };
                document.onmouseup = function() {
                    //取消捕获范围
                    if (_div.releaseCapture) {
                        _div.releaseCapture();
                    } else if (window.captureEvents) {
                        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                    }
                    document.onmousemove = document.onmouseup = null;
                };
            };
        }
        var oDivMainContent = WebElement.New("div", "winMainContentId" + this.FormID + "Type" + this.Type, "wWindowContent");
        WebElement.Add(oDivCon, oDivMainContent);
        WebElement.Add(_div, oDivCon);
        if (_me.ShowCorner) {
            WebElement.Add(_div,
				_div.appendChild(WebElement.New("div", "", "wRound_lt")),
				_div.appendChild(WebElement.New("div", "", "wRound_rt")),
				_div.appendChild(WebElement.New("div", "", "wRound_lb")),
				_div.appendChild(WebElement.New("div", "", "wRound_rb")));
        }

        WebElement.Add("windowSideBarContainer", _div); //显示内容

        gWindowManager.GetWindowsList().add({
            id: this.FormID,
            type: this.Type,
            isMin: this._minStatus,
            win: _me
        });
        gWindowManager.ListWindows();

        if (this.ContentUrl != "") {
            new Ajax().get(gDeployPath + gAjaxProxyNoEncodeUrl + this.ContentUrl, function(o) {
                oDivMainContent.innerHTML = o.responseText.replace(/\[id\]/img, _me.FormID).replace(/\[type\]/img, _me.Type).replace(/\[dpath\]/img, gImgServer); //更新元素id				
                if (_me.OnPaint) _me.OnPaint(_me.Width, _me.Height, _me.FormID, _me.Type);
                if (_me.OnLoaded) _me.OnLoaded(_me.FormID, _me.Type);
            });
        }
        else {
            oDivMainContent.innerHTML = this.Content.replace(/\[id\]/img, this.FormID).replace(/\[type\]/img, this.Type).replace(/\[dpath\]/img, gImgServer); //更新元素id
            if (this.OnPaint) this.OnPaint(this.Width, this.Height, this.FormID, this.Type);
            if (this.OnLoaded) this.OnLoaded(this.FormID, this.Type);
        }
    };
    this.MoveLayer = function() {
        var ds = _div.style;
        ds.position = "absolute";
        ds.top = WebElement.Height() - _me.Height - 2 + "px";
        ds.left = WebElement.Width() - _me.Width + "px";
        ds.width = Other.Even(_me.Width) + "px";
        ds.height = Other.Even(_me.Height + 2) + "px";
        ds.zIndex = ++window.zIndex;
    };
    this.Switch = function() {
        if (_me.Expanded) {
            WebElement.Hid($("divOnlines"));
            WebElement.Hid($("divSplit1"));
            WebElement.Hid($("divJoin"));
            WebElement.Hid($("divSplit2"));
            WebElement.Hid($("divAddFriend"));
            WebElement.Hid($("divSplit3"));
            _me.Width = 30;
            $("imgSwitch").src = gStyleManager.getStyleResPath(gStyle) + "sidebar_expand.gif";
            _me.Expanded = false;
        }
        else {
            WebElement.Show($("divOnlines"));
            WebElement.Show($("divSplit1"));
            WebElement.Show($("divJoin"));
            WebElement.Show($("divSplit2"));
            WebElement.Show($("divAddFriend"));
            WebElement.Show($("divSplit3"));
            _me.Width = 276;
            $("imgSwitch").src = gStyleManager.getStyleResPath(gStyle) + "sidebar_collapse.gif";
            _me.Expanded = true;
        }
        _me.MoveLayer();
    }
}

function WebForm() {
    this.Type = 1;             //窗体类型1Chat|2Main|4Team
    this.FormID = 0;           //用户ID,窗体为1时必须传
    this.Top = 20;
    this.Left = 40;
    this.Width = 500;
    this.Height = 300;
    this.Title = "";
    this.TitleXOffset = 0;       //标题显示X偏移量
    this.TitleHeight = null;     //标题栏高度
    this.Icon = "";
    this.MinimizeIcon = null;
    this.DefaultMRight = false;  //是否显示网页默认右键菜单
    this.Content = "";           //窗体内容
    this.ContentUrl = "";        //给出网址，Ajax去取
    this.TitleBGUrl = "";        //标题栏背景图标
    this.MinWidth = 150;         //最小宽度
    this.MinHeight = 150;        //最小高度
    this.Resizeable = 1;         //是否允许改变大小
    this.Moveable = 1;         //是否允许拖动
    this.CanControl = 1;         //是否显示控制栏
    this.ShowCorner = 0;         //是否显示圆角	
    this.ControlButtons = ["Minimize", "Close"]; //标题栏按钮
    this.MiniScollTop = 0;
    this.MinimizeButton = null;
    this.CloseButton = null;
    this._minStatus = !!0;
    this.Position = gFormPosition.fpCustomer; //窗体位置
    this.OnPaint = null; //重绘
    this.OnBeforeClose = null; //关闭前回调
    this.OnClose = null; //窗口关闭回调，在窗口关闭时调用此函数
    this.OnLoaded = null; //窗口加载完毕回调	
    var IsInit = false;
    var IsModal = false;       //是否模态窗体
    var ModalCover = null;    //模态窗体遮罩层
    var _div = null;
    var _me = this;
    this.id = function() { return "win" + this.TypeStr + this.FormID; };
    this.div = function() { return $(_me.id()) };
    this.OuterMostDiv = function()//最外层div
    {
        return $("windowContainerType" + _me.Type + "Id" + _me.FormID).parentElement ||
			   $("windowContainerType" + _me.Type + "Id" + _me.FormID).parentNode;
    };
    this.Show = function () {
        _me.OnCreate();

        _me.TypeStr = gWindowManager.GetWindowType(this.Type); //取得要调用的窗体内容Url
        if (this.Content == "" && this.ContentUrl == "") {
            this.ContentUrl = gUITempletServer + "/UITemplete/" + _me.TypeStr + ".htm";
        }
        _div = WebElement.New("div", _me.id(), "w" + _me.TypeStr);
        WebElement.Add(_div, WebElement.New("div", "selectFix", "selectFix", "<iframe frameborder='0' style='background-color:#FFF;width:100%;height:100%;filter: alpha(opacity=0);-moz-opacity:0;'></iframe>"));
        if (!_me.DefaultMRight) { _div.oncontextmenu = function () { return !!0; }; }
        _div.onmousedown = function () { _me.Focus(); };
        var oDivCon = WebElement.New("div", "", "wContent");
        var oDivTitle = WebElement.New("div", _div.id + "Title", "w" + _me.TypeStr + "Title");
        if (_me.TitleBGUrl != "") { oDivTitle.style.backgroundImage = "url(" + _me.TitleBGUrl + ")"; }
        if (_me.TitleHeight) { oDivTitle.style.height = _me.TitleHeight + "px"; };
        oDivTitle.onselectstart = function () { return !!0; };
        var oDivIcon = WebElement.New("div", "win" + _me.TypeStr + "Icon" + this.FormID, "wIcon", "<img id=Icon" + _me.TypeStr + "Icon" + this.FormID + " src=" + this.Icon + " />");
        var oDivText = WebElement.New("div", "win" + _me.TypeStr + "Text" + this.FormID, "wText", this.Title);
        oDivText.style.paddingLeft = 23 + _me.TitleXOffset + "px";
        if (this.Moveable) {
            oDivText.style.cursor = "move";
            oDivText.onmousedown = function (e) {
                if (_me.Type != 3 && _me._minStatus) return;

                e = e || window.event;
                //左键才可拖动
                if (e.which == null)
                    button = (e.button < 2) ? "LEFT" : ((e.button == 4) ? "MIDDLE" : "RIGHT"); // IE case
                else
                    button = (e.which < 2) ? "LEFT" : ((e.which == 2) ? "MIDDLE" : "RIGHT"); // All others

                if (button != "LEFT") {
                    return false;
                }

                _me.chaLeft = Evt.Left(e) - _me.Left;
                _me.chaTop = Evt.Top(e) - _me.Top;
                //设置捕获范围
                if (_div.setCapture) {
                    _div.setCapture();
                } else if (window.captureEvents) {
                    window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                }

                document.onmousemove = function (e) {
                    var _left = (_me.Left = Evt.Left(e) - _me.chaLeft) + WebElement.scrollWidth();
                    if (_left + _me.Width > WebElement.Width()) _left = WebElement.Width() - _me.Width - 5;
                    _div.style.left = _left + "px";
                    _div.style.top = (_me.Top = (Evt.Top(e) - _me.chaTop) < 0 ? 0 : Evt.Top(e) - _me.chaTop) + WebElement.scrollHeight() + "px";
                };
                document.onmouseup = function () {
                    //取消捕获范围
                    if (_div.releaseCapture) {
                        _div.releaseCapture();
                    } else if (window.captureEvents) {
                        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                    }
                    document.onmousemove = document.onmouseup = null;
                };
            };
        }
        oDivText.ondblclick = function () { _me.Minimize(); };
        WebElement.Add(oDivTitle, oDivIcon, oDivText);
        if (this.CanControl) {
            var _sControl = '';
            for (var i = 0; i < _me.ControlButtons.length; i++) {
                switch (_me.ControlButtons[i]) {
                    case "Minimize":
                        {
                            _sControl += "<div class=\"wControlMin\" onmouseup=\"this.scrollTop='0'\" onmousedown=\"this.scrollTop='36'\" onmouseover=\"this.scrollTop='18'\" onmouseout=\"this.scrollTop='0'\"><img id=\"ControlMM" + _me.FormID + "Type" + _me.Type + "\" src=\"" + gImgServer + "/Images/MainControlMin.gif\" title=\"最小化\"/></div>";
                            break;
                        }
                    case "Close":
                        {
                            _sControl += "<div class=\"wControlClose\" onmouseup=\"this.scrollTop='0'\" onmousedown=\"this.scrollTop='36'\" onmouseover=\"this.scrollTop='18'\" onmouseout=\"this.scrollTop='0'\"><img src=\"" + gImgServer + "/Images/" + _me.TypeStr + "ControlClose.gif\" title=\"关闭\"/></div>";
                            break;
                        }
                    default: return;
                }
            }
            var oDivControl = WebElement.New("div", "win" + _me.TypeStr + "Control" + this.FormID, "wControl", _sControl);
            WebElement.Add(oDivTitle, oDivControl);
            var btnControls = $T(oDivControl, "img");
            for (var i = 0; i < _me.ControlButtons.length; i++) {
                switch (_me.ControlButtons[i]) {
                    case "Minimize":
                        {
                            _me.MinimizeButton = btnControls[i];
                            _me.MinimizeButton.onclick = function () { _me.Minimize(); };
                            break;
                        }
                    case "Close":
                        {
                            _me.CloseButton = btnControls[i];
                            _me.CloseButton.onclick = function () { _me.Close(); };
                            break;
                        }
                    default: return;
                }
            }
        }
        var oDivMainContent = WebElement.New("div", "winMainContentId" + this.FormID + "Type" + this.Type, "wWindowContent");
        _me.MainContent = oDivMainContent.id;
        oDivMainContent.style.backgroundColor = "#efefef";
        oDivMainContent.style.height = (this.Height - 30) + "px";
        if (_me.TitleHeight) { oDivMainContent.style.height = (this.Height - _me.TitleHeight - 2) + "px"; };
        WebElement.Add(oDivCon, oDivTitle, oDivMainContent);
        if (this.ShowCorner) {
            WebElement.Add(_div,
				_div.appendChild(WebElement.New("div", "", "wRound_lt")),
				_div.appendChild(WebElement.New("div", "", "wRound_rt")),
				_div.appendChild(WebElement.New("div", "", "wRound_lb")),
				_div.appendChild(WebElement.New("div", "", "wRound_rb")));
        }
        if (this.Resizeable) {
            var oDiv5 = WebElement.New("div", "ResizeButtonId" + _me.FormID + "Type" + _me.Type, "wResize");
            oDiv5.onmousedown = function (e) {
                if (_me._minStatus) return;
                var bs = $("windowResizeCover").style;
                bs.top = _me.Top + "px";
                bs.left = _me.Left + "px";
                bs.width = _me.Width + "px";
                bs.height = _me.Height + "px";
				bs.zIndex = window.zIndex + 2;
                WebElement.Show("windowResizeCover");
                document.body.style.cursor = "SE-resize";
                //设置捕获范围
                if (_div.setCapture) {
                    _div.setCapture();
                } else if (window.captureEvents) {
                    window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                }
                document.onmousemove = function (e) {
                    if ((Evt.Top(e) - _me.Top) > _me.MinHeight) bs.height = (Evt.Top(e) - _me.Top) + "px";
                    if ((Evt.Left(e) - _me.Left) > _me.MinWidth) bs.width = (Evt.Left(e) - _me.Left) + "px";
                };
                document.onmouseup = function () {
                    _me.Height = Other.Even(bs.height);
                    _me.Width = Other.Even(bs.width);
                    $(_me.MainContent).style.height = (_me.Height - 28) + "px";
                    if (_me.OnPaint) _me.OnPaint(_me.Width, _me.Height - 28, _me.FormID, _me.Type);
                    _div.style.height = _me.Height + "px";
                    _div.style.width = _me.Width + "px";
                    document.body.style.cursor = "";

                    //取消捕获范围
                    if (_div.releaseCapture) {
                        _div.releaseCapture();
                    } else if (window.captureEvents) {
                        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                    }
                    document.onmousemove = document.onmouseup = null;
                    WebElement.Hid("windowResizeCover");
                };
            };
            WebElement.Add(_div, oDiv5);
        }
        var oDivLoading = WebElement.New("div", "win" + _me.TypeStr + "Loading" + this.FormID, "wLoading", "加载中...");
        WebElement.Add(_div, oDivCon, oDivLoading);

        switch (_me.Position) {
            case gFormPosition.fpWindowCenter:
                {
                    _me.Left = ((WebElement.Width() / 2) - _me.Width / 2);
                    //_me.Top = WebElement.Top() + (((WebElement.Height()/2) - _me.Height/2) - 50); //上偏移50像素，以便于视觉效果上的最佳居中
                    _me.Top = (((WebElement.Height() / 2) - _me.Height / 2) - 50);
                    break;
                }
        }
        var ds = _div.style;
        ds.position = "absolute";
        ds.left = this.Left + WebElement.scrollWidth() + "px";
        ds.top = this.Top + WebElement.scrollHeight() + "px";
        ds.width = Other.Even(this.Width) + "px";
        ds.height = Other.Even(this.Height) + "px";
        ds.zIndex = ++window.zIndex;
        if (window.cWindow != _div.id) oDivTitle.style.color = "#999";
        WebElement.Add("windowContainerType" + _me.Type + "Id" + _me.FormID, _div);
        gWindowManager.GetWindowsList().add({
            id: this.FormID,
            type: this.Type,
            isMin: this._minStatus,
            win: _me
        });
        gWindowManager.ListWindows();

        if (this.ContentUrl != "") {
            this.ShowLoading();
            new Ajax().get(gDeployPath + gAjaxProxyNoEncodeUrl + this.ContentUrl, function (o) {
                oDivMainContent.innerHTML = o.responseText.replace(/\[id\]/img, _me.FormID).replace(/\[type\]/img, _me.Type).replace(/\[dpath\]/img, gImgServer); //更新元素id
                _me.HideLoading();
                if (_me.OnPaint) _me.OnPaint(_me.Width, _me.Height - 28, _me.FormID, _me.Type);
                if (_me.OnLoaded) _me.OnLoaded(_me.FormID, _me.Type);
            });
        }
        else {
            oDivMainContent.innerHTML = this.Content.replace(/\[id\]/img, this.FormID).replace(/\[type\]/img, this.Type).replace(/\[dpath\]/img, gImgServer); //更新元素id
            if (this.OnPaint) this.OnPaint(this.Width, this.Height - 28, this.FormID, this.Type);
            if (this.OnLoaded) this.OnLoaded(this.FormID, this.Type);
        }
        //duma.BeautifyScrollBar.init();
    };
    this.OnCreate = function () {
        if (!IsInit) //最外层div
        {
            IsInit = !!1;
            _me.ModalCover = WebElement.New("div", "windowCoverType" + _me.Type + "Id" + _me.FormID, "wWindowCover");
			WebElement.Add(WebElement.New("div", "windowResizeCover", "wWindowBorder"));
            //_me.ModalCover.oncontextmenu = function(){return !!0};
            WebElement.Add(WebElement.Add(WebElement.New("div", "windowContainerBorder", "wContainerBorder"), //最外层div      			
            WebElement.Add(WebElement.New("div", "windowContainerType" + _me.Type + "Id" + _me.FormID, "wContainer"), 
            				WebElement.New("input", "inputHack", "wElemHidden"),
				WebElement.New("div", "divSound", "wElemHidden"), _me.ModalCover)));						
        }
    };
    this.ShowModal = function() {
        _me.IsModal = !!1;
        _me.Show();
        _me.BlockUI();
    };
    this.setIcon = function(IconUrl) {
        $("Icon" + _me.TypeStr + "Icon" + _me.FormID).src = gImgServer + "/Images/" + IconUrl;
    };
    this.setTitle = function(Title) {
        $("win" + _me.TypeStr + "Text" + _me.FormID).innerHTML = Title;
    };
    this.Minimize = function () {
        var ds = _div.style;       
        if (!_me._minStatus) {            
            if (_me.MinimizeIcon) { $("Icon" + _me.TypeStr + "Icon" + this.FormID).src = _me.MinimizeIcon; };
            if (_me.Resizeable) { WebElement.Hid($("ResizeButtonId" + _me.FormID + "Type" + _me.Type)); };
            $("ControlMM" + _me.FormID + "Type" + _me.Type).src = gImgServer + "/Images/MainControlRestore.gif";
            WebElement.Hid("winMainContentId" + this.FormID + "Type" + this.Type);
            $T("win" + _me.TypeStr + "Control" + this.FormID, "img")[0].title = "还原";
            ds.height = "24px";
            ds.width = "166px";
        }
        else {
            if (_me.Icon) { $("Icon" + _me.TypeStr + "Icon" + this.FormID).src = _me.Icon; };
            if (_me.Resizeable) { WebElement.Show($("ResizeButtonId" + _me.FormID + "Type" + _me.Type)); };
            $("ControlMM" + _me.FormID + "Type" + _me.Type).src = gImgServer + "/Images/MainControlMin.gif";
            WebElement.Show("winMainContentId" + this.FormID + "Type" + this.Type);
            $T("win" + _me.TypeStr + "Control" + this.FormID, "img")[0].title = "最小化";
            ds.height = _me.Height + "px";
            ds.width = _me.Width + "px";
            ds.top = _me.Top + "px";
            ds.left = _me.Left + "px";
            if (_me.Type == 5) {
                $("wMiniTeamChatViewId" + _me.FormID + "Type5Div").scrollTop = $("wMiniTeamChatViewId" + _me.FormID + "Type5Div").scrollHeight;
            }
        }
        _me._minStatus = !_me._minStatus;
        gWindowManager.GetWindow(_me.FormID, _me.Type).isMin = _me._minStatus;
        gWindowManager.ListWindows();
    };
    this.Close = function (p) {
        if (_me.FormID == gFixFormID.fiChatRoom) { _webIM.Data.killCometClient(gChatRoomNick); }
        if (_me.IsModal) { _me.IsModal = !!0; _me.UnBlockUI(); }
        if (this.OnBeforeClose) { if (!this.OnBeforeClose()) { return } }; //回调若返回true,则将关闭过程中断。
        gWindowManager.GetWindowsList().remove(gWindowManager.GetWindow(_me.FormID, _me.Type));
        WebElement.Del(_me.OuterMostDiv());
        WebElement.Del(_div); //close
        delete _me;
        gWindowManager.ListWindows();
        //if (gMiniTeamChatInterval != 0) { clearInterval(gMiniTeamChatInterval); gMiniTeamChatInterval = 0; }
        if ($("inputHack")) $("inputHack").select();
        if (this.OnClose) this.OnClose(this.FormID, this.Type, p);
    };
    this.Focus = function() {
        if (window.cWindow != _div.id) {
            $(window.cWindow + "Title") && ($(window.cWindow + "Title").style.color = "#999");
            if (_me.IsModal)
                _div.style.zIndex = ++window.zIndex;
            else
                _me.OuterMostDiv().style.zIndex = ++window.zIndex;
        }
        window.cWindow = _div.id;
        $(_div.id + "Title").style.color = "#000";  
        _webIM.Common.cancelCheckNewsShow(); 
        document.title = gBrowserTitle;
    };
    this.ShowLoading = function() {
        if (this._minStatus) return;
        WebElement.Show("win" + _me.TypeStr + "Loading" + this.FormID);
    };
    this.HideLoading = function() {
        WebElement.Hid("win" + _me.TypeStr + "Loading" + this.FormID);
    };
    this.BlockUI = function() {
        if (_me.ModalCover) {
            var cs = _me.ModalCover.style;
            cs.display = "block";
			cs.textAlign = "left";
            cs.height = WebElement.screenHeight() + 20 + "px"; //高度增加20，保证覆盖住整个高度
            cs.width = WebElement.screenWidth() + "px";
            cs.zIndex = ++window.zIndex;
            _me.OuterMostDiv().style.zIndex = ++window.zIndex;
            window.cWindow = _me.OuterMostDiv().id;
        }
    };
    this.UnBlockUI = function() {
        WebElement.Hid(_me.ModalCover);
    }
    this.Flash = function() {
        if (window.cWindow != _div.id || _me._minStatus) {
            var oTitle = $("win" + _me.TypeStr + "Text" + this.FormID);
            var times = 15;
            var flag = !0;
            var intId = setInterval(function() {
                oTitle.innerHTML = flag ? _me.Title : "<span style='color:red;font-weight:bold'>" + _me.Title + "</span>";
                flag = !flag;
                times -= 1;
                if (times < 1) {
                    clearInterval(intId);
                    oTitle.innerHTML = _me.Title;
                }
            }, 500);
        }
    };
}