﻿var gVer = "1.2";
var CommunicationCore = { ccAjax: 0, ccChunk: 1 };
var gUseCommunication = CommunicationCore.ccChunk; //默认通讯模式

var getHost = function(Url)
{
	var Host = null;
	if((typeof(Url) == "undefined") || null == Url){Url = window.location.href;}
	var RegEx = /.*\:\/\/([^\/]*).*/;
	var Match = Url.match(RegEx);
	if((typeof(Match) != "undefined") && null != Match){Host = Match[1];}
	return "http://" + Host;
}

var SLD = function(Url)
{
	var Host = null;
	if((typeof(Url) == "undefined") || null == Url){Url = window.location.href;}
	var RegEx = /(\w+\.\w+)(?=(\/|$))/i;
	var Match = Url.match(RegEx);
	if((typeof(Match) != "undefined") && null != Match){Host = Match[0];}
	return Host;	
}

var script = document.getElementById("WebIMScript");
var gScriptServer = getHost(script.src);
var gUITempletServer = gScriptServer;
var gImgServer = gScriptServer;
var gRunerDomain = SLD(getHost());
document.domain = gRunerDomain;
var DeployPath = script.getAttribute("DeployPath");
var gConnServer = script.getAttribute("connServer");
var gPageServer = script.getAttribute("pageServer");
var Platform = script.getAttribute("Platform");
var gStyle = script.getAttribute("Style");
var _scriptCount = 0;
var _scriptLoadedCount = 0;

CreateLink(document, gScriptServer + "/css/WebIM.css");
CreateLink(document, gScriptServer + "/css/WebChat.css");
//CreateLink(document,gScriptServer + "/css/scrollbar.css");
//CreateScript(document,gScriptServer + "/script/ScrollBar.js?Ver="+gVer);
CreateScript(document, gScriptServer + "/script/WebIMConst.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/CometFix.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/EventQueue.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/XHRQueue.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/WebIMBaseHelper.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/WebIMHelper.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/StyleManager.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/WindowManager.js?Ver=" + gVer);
CreateScript(document, gScriptServer + "/script/WebForm.js?Ver=" + gVer);
switch (gUseCommunication) {           
    case CommunicationCore.ccAjax: CreateScript(document, gScriptServer + "/script/AjaxMessageHandler.js?Ver=" + gVer); break;
    case CommunicationCore.ccChunk: CreateScript(document, gScriptServer + "/script/ChunkMessageHandler.js?Ver=" + gVer); break;
}
CreateScript(document, gScriptServer + "/script/MessageQueue.js?Ver=" + gVer);

function CreateScript(RootDocument, File, cb) {
    _scriptCount += 1;
    var new_element;
    head = RootDocument.getElementsByTagName('head').item(0);
    new_element = RootDocument.createElement("script");
    new_element.setAttribute("type", "text/javascript");
    new_element.setAttribute("src", File);
    void (head.appendChild(new_element));
    SetScriptOnLoad(new_element, cb);
}

function CreateLink(RootDocument, File) {
    var new_element;
    head = RootDocument.getElementsByTagName('head').item(0);
    new_element = RootDocument.createElement("link");
    new_element.setAttribute("type", "text/css");
    new_element.setAttribute("rel", "stylesheet");
    new_element.setAttribute("href", File); 
    void (head.appendChild(new_element));
}

function SetScriptOnLoad(_script, cb) {
    if (!/*@cc_on!@*/0) {
        //if not IE   
        //Firefox2、Firefox3、Safari3.1+、Opera9.6+ support js.onload   
        _script.onload = function() { ScriptOnloadComplete(cb); }
    }
    else {
        //IE6、IE7 support js.onreadystatechange   
        _script.onreadystatechange = function() {
            if (_script.readyState == 'loaded' || _script.readyState == 'complete') { ScriptOnloadComplete(cb); }
        }
    }
}

function ScriptOnloadComplete(cb) {
    _scriptLoadedCount += 1;
    if (cb) cb();
    if (_scriptLoadedCount == _scriptCount) { StartWebIM(gStyle); }
}

var _webIM = null;

function StartWebIM() {
    if (!$("windowContainerBorderS")) {
        gDeployPath = DeployPath;
        new WebIM().Initialize();
    }
}

function WebIM() {
    var _me = (_webIM = this);
    var _CommunicationCore = gUseCommunication;
	this.ServerUrl = gConnServer + ":" + gConnServerPort;
    this.MessageHandler = null;
    this.EventQueue = EventQueue.IsInstance();
    this.XmlGenerate = XmlGenerate.IsInstance();
    this.IMDateTime = IMDateTime.IsInstance();
    this.StyleManager = gStyleManager = StyleManager.IsInstance(gImgServer);
    this.FriendLoadedCount = 0;
    this.ServerSessionID = null; //服务器长连接会话Id	
    this.ReqQueue = null;  //请求队列
    this.Sys = null;
    this.Profile = null;  //个人资料
    this.PrifileEx = null;  //个人扩展资料
    this.Config = null;  //个人配置
    this.Friend = null;  //好友列表
    this.QUsers = null;  //
    this.Group = null;  //分组信息
    this.GroupOrder = null;  //分组顺序
    this.Team = null;  //群组信息
    this.Win = null;  //窗体对象
    this.Version = "";    //版本号
    //心跳
    this.KeepLive = {
        startKeepLive: function () {
            _me.Sys.KeepLiveTimer = setInterval(
					function () {
					    _me.Data.keepLive();
					    _me.Sys.TimeOutCount++;
					    if (_me.Sys.TimeOutCount >= 5) { _me.KeepLive.keepLiveTimeOut(); }
					},
			_me.Sys.klIntervalTime);
        },
        stopKeepLive: function () {
            clearInterval(_me.Sys.KeepLiveTimer);
            _me.Sys.KeepLiveTimer = null;
        },
        keepLiveTimeOut: function () {
            if (gDebugLog) { _imInstance.Logger.WriteLog(new Date().toLocaleString() + ": keepLiveTimeOut."); }
            _me.KeepLive.stopKeepLive();
            _me.Common.showAlert("由于网络原因，客户端中断连接，您需要重新登录。", "提示", _me.CMD.logoutWebIM);
        }
    };
    //日志相关
    this.Logger = {
        WriteLog: function (ALog) {
            var FSO, File;
            FSO = new ActiveXObject("Scripting.FileSystemObject");
            //File = FSO.CreateTextFile("c:\webim_log.txt",true);
            File = FSO.OpenTextFile("c:\webim_log.txt", 8);
            File.WriteLine(ALog);
            File.Close();
        }
    };
    //回调类
    this.Callback = {
        loginAfter: function () { }
    };
    //辅助
    this.Common = {
        //由GroupID得到Group对象
        getGroupById: function (gid) {
            var g = null;
            for (var i = 0; i < _me.Group.length; i++) {
                if (_me.Group[i].ID == gid) {
                    g = _me.Group[i];
                    break;
                }
            }
            return g;
        },
        //由GroupName得到Group对象
        getGroupByName: function (gname) {
            var g = null;
            for (var i = 0; i < _me.Group.length; i++) {
                if (_me.Group[i].Name == gname) {
                    g = _me.Group[i];
                    break;
                }
            }
            return g;
        },
        //更新分组信息
        updateGroup: function (gid, gname, expand) {
            for (var i = 0; i < _me.Group.length; i++) {
                if (_me.Group[i].ID == gid) {
                    _me.Group[i].Name = gname;
                    if (expand != null) { _me.Group[i].Expand = expand; };
                    return;
                }
            }
        },
        //删除分组信息
        deleteGroup: function (gid) {
            for (var i = 0; i < _me.Group.length; i++) {
                if (_me.Group[i].ID == gid) {
                    _me.Group.remove(_me.Group[i]);
                    return;
                }
            }
        },
        //从xml里拆出User数组对象
        getUserFromXml: function (xml) {
            var users = [];
            var items = $T(xml, 'item');
            if (!items || items.length < 1) return null;
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var face = gStyleManager.getBigDefaultHeadPath(Xml.First(item, "f"));
                var id = parseInt(Xml.First(item, "id"));
                var usernick = Xml.First(item, "n");
                var email = Xml.First(item, "e");
                var sign = Xml.First(item, "sn");
                var status = Xml.First(item, "s");
                var groupid = Xml.First(item, "g");
                var isblocked = Xml.First(item, "b") == "1";
                var customname = Xml.First(item, "cn");
                var gender = Xml.First(item, "u");
                if (!_me.Common.getUserFromArr(id, users))//防止重复
                    users.add(new _me.Model.User(face, id, usernick, email, sign, parseInt(status), parseInt(groupid), isblocked, customname, gender));
            }
            return users;
        },
        //更新Profile数组
        updateProfile: function (_face, _id, _usernick, _email, _sign, _userStatus, _groupId, _isBlocked, _customname, _gender) {
            if (!_me.Profile) return;
            if (_me.Profile.UserID != _id) return; //只允许修改自己的信息
            if (_face != null) _me.Profile.UserAvatar = _face;
            if (_usernick != null) _me.Profile.UserNick = _usernick;
            if (_email != null) _me.Profile.UserEmail = _email;
            if (_sign != null) _me.Profile.UserSign = _sign;
            if (_userStatus != null) _me.Profile.OnlineStatus = _userStatus;
            if (_userStatus != null) _me.Profile.UserStatus = _userStatus;
            if (_groupId != null) _me.Profile.GroupID = _groupId;
            if (_isBlocked != null) _me.Profile.IsBlocked = _isBlocked;
            if (_customname != null) _me.Profile.CustomName = _customname;
            if (_gender != null) _me.Profile.UserGender = _gender;
        },
        //从xml里拆出UserEx数组对象
        getProfileExFromXml: function (xml) {
            var users = [];
            var items = $T(xml, 'item');
            if (!items || items.length < 1) return null;
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var id = parseInt(Xml.First(item, "id"));
                var age = Xml.First(item, "age");
                var birthday_type = Xml.First(item, "birthday_type");
                var birthday_month = Xml.First(item, "birthday_month");
                var birthday_day = Xml.First(item, "birthday_day");
                var home_region = Xml.First(item, "home_region");
                var home_city = Xml.First(item, "home_city");
                var home_province = Xml.First(item, "home_province");
                var local_region = Xml.First(item, "local_region");
                var local_city = Xml.First(item, "local_city");
                var local_province = Xml.First(item, "local_province");
                var college = Xml.First(item, "college");
                var company = Xml.First(item, "company");
                var carrier = Xml.First(item, "carrier");
                var home_page = Xml.First(item, "home_page");
                var e_mail = Xml.First(item, "e_mail") == "1";
                var mobile_num = Xml.First(item, "mobile_num");
                var telphone = Xml.First(item, "telphone");
                var auth_type = Xml.First(item, "auth_type");
                var question = Xml.First(item, "question");
                var answer = Xml.First(item, "answer");
                if (!_me.Common.getUserExFromArr(id, users))//防止重复
                    users.add(new _me.Model.UserDetailInfo(id, age, birthday_type, birthday_month, birthday_day, home_region, home_city, home_province,
													   local_region, local_city, local_province, college, company, carrier, home_page, e_mail, mobile_num,
													   telphone, auth_type, question, answer));
            }
            return users;
        },
        //更新ProfileEx
        updateProfileEx: function (_id, _age, _birthday_type, _birthday_month, _birthday_day, _home_region, _home_city, _home_province,
					 	  _local_region, _local_city, _local_province, _college, _company, _carrier, _home_page, _e_mail, _mobile_num, _telphone,
					 	  _auth_type, _question, _answer) {
            if (!_me.ProfileEx) return;
            if (_me.Profile.UserID != _id) return; //只允许修改自己的信息

            if (_age != null) _me.ProfileEx.age = _age;
            if (_birthday_type != null) _me.ProfileEx.birthday_type = _birthday_type;
            if (_birthday_month != null) _me.ProfileEx.birthday_month = _birthday_month;
            if (_birthday_day != null) _me.ProfileEx.birthday_day = _birthday_day;
            if (_home_region != null) _me.ProfileEx.home_region = _home_region;
            if (_home_city != null) _me.ProfileEx.home_city = _home_city;
            if (_home_province != null) _me.ProfileEx.home_province = _home_province;
            if (_local_region != null) _me.ProfileEx.local_region = _local_region;
            if (_local_city != null) _me.ProfileEx.local_city = _local_city;
            if (_local_province != null) _me.ProfileEx.local_province = _local_province;
            if (_college != null) _me.ProfileEx.college = _college;
            if (_company != null) _me.ProfileEx.company = _company;
            if (_carrier != null) _me.ProfileEx.carrier = _carrier;
            if (_home_page != null) _me.ProfileEx.home_page = _home_page;
            if (_e_mail != null) _me.ProfileEx.e_mail = _e_mail;
            if (_mobile_num != null) _me.ProfileEx.mobile_num = _mobile_num;
            if (_telphone != null) _me.ProfileEx.telphone = _telphone;
            if (_auth_type != null) _me.ProfileEx.auth_type = _auth_type;
            if (_question != null) _me.ProfileEx.question = _question;
            if (_answer != null) _me.ProfileEx.answer = _answer;
        },
        //根据UserID从User数组中返回User对象
        getUserFromArr: function (id, users) {
            for (var i = 0; i < users.length; i++) {
                if (users[i].UserID == id) return users[i];
            }
            return null;
        },
        //根据UserID从User数组中返回UserEx对象
        getUserExFromArr: function (id, users) {
            for (var i = 0; i < users.length; i++) {
                if (users[i].UserID == id) return users[i];
            }
            return null;
        },
        //根据UserID从User数组中更新User对象
        updateUserInfo: function (_face, _id, _usernick, _email, _sign, _userStatus, _groupId, _isBlocked, _customname, _gender) {
            for (var i = 0; i < _me.Friend.length; i++) {
                if (_me.Friend[i].UserID == _id) {
                    if (_face != null) _me.Friend[i].UserAvatar = _face;
                    if (_usernick != null) _me.Friend[i].UserNick = _usernick;
                    if (_email != null) _me.Friend[i].UserEmail = _email;
                    if (_sign != null) _me.Friend[i].UserSign = _sign;
                    if (_userStatus != null) _me.Friend[i].OnlineStatus = _userStatus;
                    if (_userStatus != null) _me.Friend[i].UserStatus = _userStatus;
                    if (_groupId != null) _me.Friend[i].GroupID = _groupId;
                    if (_isBlocked != null) _me.Friend[i].IsBlocked = _isBlocked;
                    if (_customname != null) _me.Friend[i].CustomName = _customname || _usernick;
                    if (_gender != null) _me.Friend[i].UserGender = _gender;
                    return;
                };
            }
        },
        updateUserInfoEx: function (userObj) {
            for (var i = 0; i < _me.Friend.length; i++) {
                if (_me.Friend[i].UserID == userObj.UserID) {
                    _me.Friend[i] = userObj;
                    return;
                }
            }
        },
        //插入一个陌生人资料
        createStranger: function (uid, status) {
            _me.Friend.add(new _me.Model.User(gStyleManager.getBigDefaultHeadPath(0),
										  uid, uid, uid, uid, status, gFixGroups.fgMyStranger, null, uid, 0));
            //投递一个查询，尝试更新此用户信息
            _me.Data.getFriendUserFullInfo(uid);
        },
        //根据UserID从User数组中更新User状态
        updateUserStatus: function (_id, _userStatus) {
            if (!_me.Friend) { return false; }
            for (var i = 0; i < _me.Friend.length; i++) {
                if (_me.Friend[i].UserID == _id) {
                    if (_userStatus != null) _me.Friend[i].UserStatus = _userStatus;
                    return true;
                };
            }
            return false;
        },
        //根据TeamID从Team数组中返回Team对象
        getUserGenderFromUserID: function (uid) {
            for (var i = 0; i < _me.Friend.length; i++) {
                if (_me.Friend[i].UserID == uid) return _me.Friend[i].UserGender;
            }
            return null;
        },
        //从xml里拆出Group数组对象
        getGroupFromXml: function (xml) {
            var groups = [];
            var items = $T(xml, 'item');
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var name = Xml.First(item, "Name");
                var id = Xml.First(item, "ID");
                groups.add(new _me.Model.Group(name, parseInt(id)));
            }
            if (groups.length < 1) groups.add(new _me.Model.Group("默认", 1));
            return groups;
        },
        //根据TeamUserID从TeamUser数组中返回TeamUser对象
        getTeamUserFromArr: function (id, TeamUsers) {
            for (var i = 0; i < TeamUsers.length; i++) {
                if (TeamUsers[i].UserID == id) return TeamUsers[i];
            }
            return null;
        },
        //从xml里拆出TeamUser数组对象
        addTeamUserFromXml: function (xml) {
            if (!(_me.TeamUser)) { _me.TeamUser = []; }
            var items = $T(xml, 'item');
            if (!items || items.length < 1) return;
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var TeamID = Xml.First(item, "TeamID");
                var IsBoss = Xml.First(item, "IsBoss");
                var IsAdmin = Xml.First(item, "IsAdmin");
                var UserID = Xml.First(item, "UserID");
                var UserNick = Xml.First(item, "UserNick");
                var UserStatus = Xml.First(item, "UserStatus");
                if (!_me.Common.getTeamUserFromArr(UserID, _me.TeamUser))//防止重复
                    _me.TeamUser.add(new _me.Model.TeamUser(TeamID, IsBoss, IsAdmin, UserID, UserNick, UserStatus));
            }
        },
        //根据TeamID从Team数组中返回Team对象
        getTeamFromArr: function (id, teams) {
            for (var i = 0; i < teams.length; i++) {
                if (teams[i].TeamID == id) return teams[i];
            }
            return null;
        },
        //从xml里拆出Team对象
        getTeamFromXml: function (xml) {
            var teams = [];
            var items = $T(xml, 'item');
            if (!items || items.length < 1) return null;
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var TeamID = Xml.First(item, "TeamID");
                var TeamName = Xml.First(item, "TeamName");
                var TeamIcon = Xml.First(item, "TeamIcon");
                var TeamDescript = Xml.First(item, "TeamDescript");
                var IsBoss = Xml.First(item, "IsBoss");
                var IsAdmin = Xml.First(item, "IsAdmin");
                var IsBlocked = Xml.First(item, "IsBlocked");
                if (!_me.Common.getTeamFromArr(TeamID, teams))//防止重复
                    teams.add(new _me.Model.Team(TeamID, TeamName, TeamIcon, TeamDescript, IsBoss, IsAdmin, IsBlocked));
            }
            return teams;
        },
        //从xml里拆出Msg对象
        getMsgFromXml: function (xml) {
            var msgs = [];
            var items = $T(xml, 'item');if(!items)return;
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var from = Xml.First(item, "From");
                var fromnick = Xml.First(item, "FromNick");
                var to = Xml.First(item, "To");
                var content = Xml.First(item, "Content");
                var type = Xml.First(item, "Type");
                var code = Xml.First(item, "Code");
                var status = Xml.First(item, "Status");
                var isconfirm = Xml.First(item, "IsConfirm");
                var addtime = Xml.First(item, "AddTime");
                msgs.add(new _me.Model.Msg(from, fromnick, to, content, type, code, status, isconfirm, addtime, false));
            }
            return msgs;
        },
        //从xml里拆出Msg对象
        getMiniMemberFromXml: function (xml) {
            var miniMem = [];
            var items = $T(xml, 'item');
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var UserID = Xml.First(item, "UserID");
                var TeamNick = Xml.First(item, "TeamNick");
                var TeamGender = Xml.First(item, "TeamGender");
                var IsGuest = Xml.First(item, "IsGuest");
                miniMem.add(new _me.Model.MiniTeam(UserID, TeamNick, TeamGender, IsGuest));
            }
            return miniMem;
        },
        //从xml里拆出Config对象
        getConfigFromXml: function (xml) {
            var items = $T(xml, 'item');
            if (!items || items.length < 1) return null;
            var item = items[0];
            var distype = Xml.First(item, "DisType");
            var ordertype = Xml.First(item, "OrderType");
            var chatside = Xml.First(item, "ChatSide");
            var teamchatside = Xml.First(item, "TeamChatSide");
            var msgsendkey = Xml.First(item, "MsgSendKey");
            var msgshowtime = Xml.First(item, "MsgShowTime");
            var userpower = Xml.First(item, "UserPower");
            var defaultpannel = "wMainTeamGroup";
            var autoshowmsg = Xml.First(item, "AutoShowMsg");
            return new _me.Model.Config(parseInt(distype), parseInt(ordertype), parseInt(chatside), parseInt(teamchatside), parseInt(msgsendkey), parseInt(msgshowtime), parseInt(userpower), defaultpannel, parseInt(autoshowmsg));
        },
        //搜索好友
        searchFriendList: function (k) {
            var result = [];
            for (var i = 0; i < _me.Friend.length; i++) {
                var p = _me.Friend[i];
                if (String(p.UserID).indexOfEx(k) > -1 || p.UserNick.indexOfEx(k) > -1 || p.UserSign.indexOfEx(k) > -1 || p.CustomName.indexOfEx(k) > -1)//昵称、邮箱、自定义昵称
                {
                    result.add(p);
                }
            }
            return result;
        },
        //对好友列表排序
        sortFriendList: function () {
            switch (_me.Config.OrderType) {
                case 1: //状态
                    _me.Friend.sort(
					function (a, b) {
					    if (a.UserStatus == b.UserStatus) {
					        return a.UserNick.localeCompare(b.UserNick);
					    }
					    else {
					        var aa = a.UserStatus;
					        var bb = b.UserStatus;
					        if (aa == 1) {
					            aa = 10;
					        }
					        if (bb == 1) {
					            bb = 10;
					        }
					        return (aa < bb) ? 1 : -1;
					    }
					});
                    return;
                case 2: //分组
                    _me.Friend.sort(
					function (a, b) {
					    if (a.GroupID == b.GroupID) {
					        if (a.UserStatus == b.UserStatus) {
					            if ((a.UserNick != null) && (b.UserNick != null)) { return String(a.UserNick).localeCompare(b.UserNick); }
					            if ((a.UserID != null) && (b.UserID != null)) { return String(a.UserID).localeCompare(String(b.UserID)); }
					        }
					        else {
					            var aa = a.UserStatus;
					            var bb = b.UserStatus;
					            if (aa == 1) {
					                aa = 10;
					            }
					            if (bb == 1) {
					                bb = 10;
					            }
					            return (aa < bb) ? 1 : -1;
					        }
					    }
					    else {
					        return a.GroupID < b.GroupID ? 1 : -1;
					    }
					});
                    return;
            }
        },
        sortTeamUserList: function () {
            _me.TeamUser.sort(
			function (a, b) {
			    if (a.UserStatus == b.UserStatus) {
			        return a.UserNick.localeCompare(b.UserNick);
			    }
			    else {
			        return a.UserStatus < b.UserStatus ? -1 : 1;
			    }
			});
            return;
        },
        listItemChange: function (o, evt, t) {
            switch (parseInt(t)) {
                case 1: //click event
                    {
                        if (parseInt(o.getAttribute("us")) == 1) return;
                        _me.Common.HightListItem(o, evt, t);
                        if (_me.Sys["LastObjSel"]) {
                            if (arguments[3]) { _me.Common.UnHightListItem(_me.Sys["LastObjSel"], arguments[3]); }
                            else { _me.Common.UnHightListItem(_me.Sys["LastObjSel"]); }
                        }
                        o.setAttribute("us", t);
                        _me.Sys["LastObjSel"] = o;
                        break;
                    }
                case 2: //mouseover event
                    {
                        if (parseInt(o.getAttribute("us")) != 1) {
                            _me.Common.HightListItem(o, evt, t);
                            o.setAttribute("us", t);
                        }
                        break;
                    }
                case 3: //mouseout event
                    {
                        if (parseInt(o.getAttribute("us")) != 1) {
                            _me.Common.UnHightListItem(o);
                            o.setAttribute("us", t);
                        }
                        break;
                    }
                default: break;
            }
        },
        //高亮选中item
        HightListItem: function (o, evt, i) {
            if (Other.Browser() == "opera") {
                var e = evt || event;
                if ($("btnOpera")) WebElement.Del($("btnOpera"));
                var btn = WebElement.New("input", "btnOpera");
                btn.type = "button";
                var bs = btn.style;
                bs.zIndex = "9999";
                bs.opacity = "0.01";
                bs.height = "25px";
                bs.width = "25px";
                bs.top = (Evt.Top(e) - 5) + "px";
                bs.left = (Evt.Left(e) - 5) + "px";
                bs.position = "absolute";
                WebElement.Add(btn);
            }
            //切换click和hover状态
            o.style.border = "1px solid #d5eec5";
            o.style.backgroundImage = "url(" + gImgServer + "/Images/um" + i + ".gif)";
        },
        //去除高亮选中item
        UnHightListItem: function (o) {
            o.style.border = "1px solid #fff";
            o.style.backgroundImage = "";
            o.setAttribute("us", 3);
            if (arguments[1]) {
                o.style.border = "1px solid " + arguments[1] + "";
            }
        },
        //新消息TITLE提示
        checkNewMsgShow: function (nick) {
            gBlinkId = setInterval(function () {
                document.title = gBlinkSwitch % 2 ? "【　　　】 - 消息来自" + nick : "【新消息】 - 消息来自" + nick;
                gBlinkSwitch++;
            }, 1000);
        },
        cancelCheckNewsShow: function () {
            if (gBlinkId != 0) {
                clearInterval(gBlinkId);
                gBlinkId = 0;
                gBlinkSwitch = 0;
            }
        },
        //右键菜单
        showContextMenu: function (o, evt, t, id) {
            Other.Browser() == "opera" && WebElement.Del($("btnOpera"));
            var e = evt || event;
            if (parseInt(e.button) == 2)//右键菜单
            {
                var uid = o.getAttribute("uid");
                var gid = o.getAttribute("gid");
                var tid = o.getAttribute("tid");
                var cuid = o.getAttribute("cuid");
                var uList = o.getAttribute("uList");
                if (uid) {
                    var u = _me.Common.getUserFromArr(uid, _me.Friend);
                    if (!u) return;
                    var arr = [];
                    arr.add(u.OnlineStatus < 3 ? "<B>发送消息</B>|_webIM.CMD.openChatWindow(" + uid + ",true);" : "<strong>发送离线消息</strong>|_webIM.CMD.openChatWindow(" + uid + ",true);");
                    if ((_me.Config.OrderType == 2) && (u.GroupID != gFixGroups.fgMyStranger)) arr.add("编辑分组|_webIM.CMD.eidtUserGroup(" + uid + ")");
                    arr.add("");
                    if (u.GroupID == gFixGroups.fgMyStranger) arr.add("加为好友|_webIM.Data.editUserGroup(" + uid + "," + gFixGroups.fgMyFriends + ");_webIM.Data.addFriend(" + uid + "," + null + ");");
                    arr.add("");
                    if (u.GroupID != gFixGroups.fgMyBlackList) arr.add("移至黑名单|_webIM.Data.editUserGroup(" + uid + "," + gFixGroups.fgMyBlackList + ");_webIM.Data.blacklistFriend(" + uid + "," + null + ");");
                    arr.add("删除好友|_webIM.CMD.delFriend(" + uid + ")");
                    if ((u.GroupID != gFixGroups.fgMyStranger)) arr.add(u.CustomName == u.UserNick ? "添加备注|_webIM.CMD.editCustomName(" + uid + ")" : "编辑备注|_webIM.CMD.editCustomName(" + uid + ")");
                    //arr.add(u.IsBlocked?"取消阻止好友|_webIM.CMD.blockFriend("+uid+",2)":"阻止好友|_webIM.CMD.blockFriend("+uid+",1)");
                    //arr.add("消息记录|_webIM.CMD.showMsgHistory("+uid+")");	
                    arr.add("查看资料|_webIM.CMD.showCard(" + uid + ",this)");
                }
                else if (gid) {
                    var arr = [];
                    switch (parseInt(gid)) {
                        case 0: case 1: case 2:
                            {
                                arr.add("新建组|_webIM.CMD.addGroup()");
                                break;
                            }
                        default:
                            {
                                arr.add("新建组|_webIM.CMD.addGroup()");
                                arr.add("");
                                arr.add("删除组|_webIM.CMD.delGroup(" + gid + ")");
                                arr.add("修改组名称|_webIM.CMD.editGroup(" + gid + ")");
                                break;
                            }
                    }
                }
                else if (tid) {
                    var arr = [];
                    arr.add("发送消息|_webIM.CMD.openTeamChatWindow(" + tid + ",true)");
                    //arr.add("加为好友|_webIM.Data.editUserGroup("+uid+","+gFixGroups.fgMyFriends+");_webIM.Data.addFriend("+uid+","+null+");");
                    //arr.add("成员管理|_webIM.CMD.delGroup("+gid+")");
                    //arr.add("");
                    //arr.add("群设置|_webIM.CMD.addGroup()");
                    //arr.add("退出该群|_webIM.CMD.addGroup()");
                }
                else if (cuid) {
                    var arr = [];
                    arr.add("发送消息|_webIM.CMD.openTeamChatWindow(" + o.getAttribute("cuid") + ",true)");
                    arr.add("加为好友|_webIM.Data.editUserGroup(" + uid + "," + gFixGroups.fgMyFriends + ");_webIM.Data.addFriend(" + uid + "," + null + ");");
                    //arr.add("成员管理|_webIM.CMD.delGroup("+gid+")");
                    //arr.add("");
                    //arr.add("群设置|_webIM.CMD.addGroup()");
                    //arr.add("退出该群|_webIM.CMD.addGroup()");
                }
                else if (uList) //点在现有分组以外
                {
                    var arr = [];
                    arr.add("新建组|_webIM.CMD.addGroup()");
                }
                else {
                    return;
                }
                var m = MenuManager.IsInstance().GetPopMenu(t, id);
                m.Data = arr;
                m.E = e;
                m.Width = 130;
                m.Show();
            }
        },
        //显示内容操作菜单
        showContentMenu: function (o, evt) {
            var e = evt || event;
            if (parseInt(e.button) == 2)//右键菜单
            {
                gClipBoardData = document.selection.createRange().text;
                var tid = o.getAttribute("tid");
                var uid = o.getAttribute("uid");

                var arr = [];
                //arr.add("剪切|_webIM.Data.cutContent("+tid+","+uid+")");
                arr.add("复制|_webIM.Data.copyContent()");
                arr.add("粘贴|_webIM.Data.pasteContent()");
                arr.add("全部选择|_webIM.Data.selectAllContent()");
                var m = MenuManager.IsInstance().GetPopMenu(tid, uid);
                m.Data = arr;
                m.Width = 130;
                m.E = e;
                m.Show();
            }
        },
        //插入一个Team头,obj:对象,title:标题,no:编号
        addTeamHeader: function (obj, title, no) {
            var id = _me.Win.id;
            var t = _me.Win.type;
            var group = WebElement.New("div", "wMainGroupHeaderId" + id + "Type" + t + "No" + no, "wMainGroupHeaderEx", title); //默认为展开样式
            group.onmouseover = function () {
                this.className = "wMainGroupHeaderEx wMainGroupHeaderExHover";
            };
            group.onmouseout = function () {
                this.className = "wMainGroupHeaderEx";
            };
            //group.onmousedown = function(e){_me.Common.listItemChange(this,e,1)};
            group.onmouseup = function (e) { _me.Common.showContextMenu(this, e, t, id) };
            group.onclick = function () {
                if (this.className == "wMainGroupHeader wMainGroupHeaderHover") {
                    this.className = "wMainGroupHeaderEx wMainGroupHeaderExHover";
                    this.onmouseout = function () { this.className = "wMainGroupHeaderEx"; };
                    this.onmouseover = function () { this.className = "wMainGroupHeaderEx wMainGroupHeaderExHover"; };
                    WebElement.Show(this.nextSibling);
                }
                else {
                    this.className = "wMainGroupHeader wMainGroupHeaderHover";
                    this.onmouseout = function () { this.className = "wMainGroupHeader"; };
                    this.onmouseover = function () { this.className = "wMainGroupHeader wMainGroupHeaderHover"; };
                    WebElement.Hid(this.nextSibling);
                }
            };
            var container = WebElement.New("div", "wMainGroupContainerId" + id + "Type" + t + "No" + no, "wMainUserContainer");
            WebElement.Add(obj, group, container);
            return group;
        },
        //插入一个TeamUser头,obj:对象,title:标题,no:编号
        addTeamUserHeader: function (obj, title, no) {
            var team = WebElement.New("div", "wTeamUserHeaderId" + no + "Type" + gFixForm.ffTeamChat, "wTeamUserHead", title);
            var container = WebElement.New("div", "wTeamUserContainerId" + no + "Type" + gFixForm.ffTeamChat, "wTeamUserContainer");
            WebElement.Add(obj, team, container);
            return team;
        },
        //插入一个Team u:Team对象
        createTeamItem: function (Team) {
            return HeadManager.IsInstance().GetTeamHeadByTeam(Team, 1).GetHtmlSource();
        },
        //插入一个TeamUser u:TeamUser对象
        createTeamUserItem: function (TeamUser) {
            //var ban = TeamUser.IsBlocked?"b":"";
            var str = new StringBuilder();
            str.add("<div id=\"wMainUserItemType" + gFixForm.ffTeamChat + "Id" + TeamUser.UserID + "\" class=\"wTeamUserItem\" cuid=\"" + TeamUser.UserID + "\" onmousedown=\"_webIM.Common.listItemChange(this,event,1,'#EFEFEF');\" onmouseup=\"_webIM.Common.showContextMenu(this,event," + gFixForm.ffTeamChat + "," + TeamUser.TeamID + ")\" oncontextmenu=\"function(){return !!0;}\" ondblclick=\"var cuid = this.getAttribute('cuid');if(cuid)_webIM.CMD.openChatWindow(" + TeamUser.UserID + ",true);\">");
            str.add("<div class=\"wMainListButton\" onmouseover=\"this.className='wMainListButton wMainListButtonHover'\" onmouseout=\"this.className='wMainListButton'\">");
            str.add("<img src=\"" + gStyleManager.getStyleResPath(gStyle) + "1" + TeamUser.UserStatus + ".gif\" title=\"聊天室成员\" style=\"height:20px;width:20px;padding:1px;\" onclick=\"_webIM.CMD.showCard(" + TeamUser.UserID + ",this.parentNode)\"/>");
            str.add("</div>");
            str.add("<div class=\"wMainUserItemText\">" + TeamUser.UserNick + "</div>");
            str.add("</div>");
            return str.toString();
        },
        //插入一个item头,obj:对象,title:标题,no:编号
        addUserHeader: function (obj, expand, title, no) {
            var id = _me.Win.id;
            var t = _me.Win.type;
            var header = WebElement.New("div", "wMainUserHeaderId" + id + "Type" + t + "No" + no, "wMainUserHeaderEx", title);
            header.onmouseover = function () {
                this.className = "wMainUserHeaderEx wMainUserHeaderExHover";
            };
            header.onmouseout = function () {
                this.className = "wMainUserHeaderEx";
            };
            if (_me.Config.OrderType == 2) header.setAttribute("gid", no);  //只有在使用自定义分组时，才允许管理分组
            header.onmouseup = function (e) { _me.Common.showContextMenu(this, e, t, id) };
            header.onclick = function () {
                if (this.className == "wMainUserHeader wMainUserHeaderHover") {
                    this.className = "wMainUserHeaderEx wMainUserHeaderExHover";
                    this.onmouseout = function () { this.className = "wMainUserHeaderEx"; };
                    this.onmouseover = function () { this.className = "wMainUserHeaderEx wMainUserHeaderExHover"; };
                    WebElement.Show(this.nextSibling);
                    _me.Common.updateGroup(no, title, true);
                }
                else {
                    this.className = "wMainUserHeader wMainUserHeaderHover";
                    this.onmouseout = function () { this.className = "wMainUserHeader"; };
                    this.onmouseover = function () { this.className = "wMainUserHeader wMainUserHeaderHover"; };
                    WebElement.Hid(this.nextSibling);
                    _me.Common.updateGroup(no, title, false);
                }
            };
            var container = WebElement.New("div", "wMainUserContainerId" + id + "Type" + t + "No" + no, "wMainUserContainer");
            WebElement.Add(obj, header, container);
            if (!expand) { header.onclick() };
        },
        //插入一个item,n:1简要2详细3大头像,u:User对象
        createUserItem: function (n, u) {
            return HeadManager.IsInstance().GetUserHeadByUser(u, n).GetHtmlSource();
        },
        //显示一个警告框
        showAlert: function (msg, title, closecb, url, icon, loadedcb) {
            var wAlert = gWindowManager.GetWindow(gFixForm.ffAlert[1], gFixForm.ffAlert[0]); //一次只允许出现一个警告框
            if (wAlert) {
                if (wAlert.isMin) wAlert.win.Minimize();
                wAlert.win.Focus();
                return;
            }
            msg = msg || "";
            var w = new WebForm();
            w.Title = title || "注意";
            w.Icon = icon || gImgServer + "/Images/warning.gif";
            w.Type = gFixForm.ffAlert[0];
            w.FormID = gFixForm.ffAlert[1];
            w.Position = gFormPosition.fpWindowCenter;
            w.Height = msg.length < 30 ? 140 : 240;
            w.Width = msg.length < 30 ? 210 : 310;
            w.CanControl = !!0;
            w.Resizeable = !!0;
            w.ShowCorner = !!0;
            w.ContentUrl = url || gUITempletServer + "/UITemplete/Alert.htm";
            w.Left = (WebElement.Width() - 210) / 2;
            w.Top = WebElement.Top() + (WebElement.Height() - 130) / 2;
            w.OnPaint = function (w, h, id, t) { $("wOtherMainId" + id + "Type" + t).style.height = (h - 30) + "px"; if ($("divMsgId" + id + "Type" + t)) WebElement.Value("divMsgId" + id + "Type" + t, msg); };
            w.OnClose = closecb || null;
            w.OnLoaded = function (id, t) {
                $("btnSelId" + id + "Type" + t).focus();
                if (loadedcb) loadedcb(id, t);
            };
            w.ShowModal();
            w.Focus();
            return w;
        },
        //显示一个确认框,回传cb的第三个参数含义:点确定为true;取消为false
        showConfirm: function (msg, title, cb) {
            _me.Common.showAlert(msg, title, cb, gUITempletServer + "/UITemplete/Confirm.htm", gImgServer + "/Images/Confirm.gif");
        },
        //显示一个文本输入框,回传cb的第三个参数含义:输入内容
        showPrompt: function (value, title, cb) {
            _me.Common.showAlert(value, title, cb, gUITempletServer + "/UITemplete/Prompt.htm");
        },
        //显示一个下拉框
        showSelect: function (title, loadedcb, closecb) {
            _me.Common.showAlert(null, title, closecb, gUITempletServer + "/UITemplete/Select.htm", null, loadedcb);
        },
        //显示一个窗口，窗口里用iframe的形式显示所给网页
        showLink: function (_w, _h, _t, _l, _id, _title, _url, _icon, _ismodal, _closecb) {
            var wLink = gWindowManager.GetWindow(_id, gFixForm.ffLink); //存在
            if (wLink) {
                if (wLink.isMin) wLink.win.Minimize();
                wLink.win.Focus();
                return;
            }
            var wLink = new WebForm();
            wLink.Title = _title || "WebIM";
            wLink.Icon = _icon || gImgServer + "/Images/DefaultIcon.gif";
            wLink.Type = gFixForm.ffLink;
            wLink.FormID = _id;
            wLink.Height = _h;
            wLink.Width = _w;
            wLink.Left = _l || ((WebElement.Width() / 2) - _w / 2);
            wLink.Top = _t || (((WebElement.Height() / 2) - _h / 2) - 50);
            wLink.Resizeable = !!0;
            wLink.ShowCorner = !!0;
            wLink.Content = "<iframe scrolling='no' id='link[id]Type[type]' name='link[id]Type[type]' frameborder='0' src='" + AddUrlParam(_url, "Type=" + wLink.Type + "&Id=" + _id) + "'></iframe>";

            wLink.OnPaint = function (w, h, id, t) {
                var iframe = $("link" + id + "Type" + t);
                iframe.name = "link" + id + "Type" + t;
                iframe.style.height = h + "px";
                iframe.width = w - 2 + "px";
                if (iframe.attachEvent)
                    iframe.attachEvent('onload', function () { wLink.HideLoading(); });
                else
                    iframe.onload = function () { wLink.HideLoading(); }
            }
            if (_closecb) wLink.OnClose = _closecb;
            wLink.OnLoaded = function () { wLink.ShowLoading(); }
            if (_ismodal) { wLink.ShowModal(); } else { wLink.Show(); }
            //alert(AddUrlParam(_url, "Type=" + wLink.Type + "&Id=" + _id));
            wLink.Focus();
        },
        //播放声音
        playSound: function (soundname) {
            var oDiv = $("divSound");
            if (!oDiv) return;
            WebElement.Value(oDiv, "<embed id=\"sound\" name=\"sound\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" src=\"sound/" + soundname + ".swf\" width=\"1\" height=\"1\" type=\"application/x-shockwave-flash\" autoplay=\"true\" quality=\"high\" loop=\"False\"></embed>");
        },
        //将字符表情替换成图片
        replaceFaceFromStr: function (str) {
            var Emotions = [];
            Emotions = _me.Data.getEmotionInfo();
            for (var i = 0; i < Emotions.length; i++) {
                var Emotion = Emotions[i];
                str = str.replaceAll(Emotion[2], "<img style='height:24px;width:24px' src='" + gImgServer + "/Emotion/" + EmotionFolder + Emotion[0] + ".gif'/>");
                if (Emotion.length > 3) str = str.replaceAll(Emotion[3], "<img style='height:24px;width:24px' src='Emotion/" + EmotionFolder + Emotion[0] + ".gif'/>");
            }
            return str;
        },
        //根据UserStatus返回对应字符串
        getUserStatusStr: function (n) {
            if (n == -1) n = 4;
            if (n == 0) n = 1;
            return ["在线", "忙碌", "离开", "隐身"][n - 1];
        },
        gb2312ToUTF8: function (str) {
            return str;
            //if(str.length<1)return;
            var UTF8Value = null
            new Ajax().get(gDeployPath + gCTGb2312ToUTF8Url + "?AString=" + str,
            function (o) {
                if (!o) return null;
                if (!$T(o.responseXML, "list")) return null;
                UTF8Value = Xml.First($T(o.responseXML, "list").item(0), "s");
            }, false);
            if (Platform == "PHP") {
                return decodeURIComponent(UTF8Value).replace("+", " ");
            }
            return UTF8Value;
        },
        showFontFlag: function (id, value) {
            switch (value) {
                case "1":
                    $(id).style.fontStyle = "normal";
                    $(id).style.fontWeight = "bold";
                    break;
                case "2":
                    $(id).style.fontStyle = "italic";
                    $(id).style.fontWeight = "normal";
                    break;
                case "3":
                    $(id).style.fontStyle = "italic";
                    $(id).style.fontWeight = "bold";
                    break;
                default:
                    $(id).style.fontStyle = "normal";
                    $(id).style.fontWeight = "normal";
                    break;
            }
        }
    };
    //模型
    this.Model = {
        //用户模型
        User: function (_face, _id, _usernick, _email, _sign, _userStatus, _groupId, _isBlocked, _customname, _gender) {
            this.UserAvatar = _face;
            this.UserID = _id;
            this.UserNick = _usernick;
            this.UserEmail = _email;
            this.UserSign = _sign;
            this.OnlineStatus = _userStatus;
            this.UserStatus = _userStatus;
            this.GroupID = _groupId;
            this.IsBlocked = _isBlocked;
            this.CustomName = _customname || _usernick;
            this.UserGender = _gender;
        },
        //用户详细资料
        UserDetailInfo: function (_id, _age, _birthday_type, _birthday_month, _birthday_day, _home_region, _home_city, _home_province,
					 		_local_region, _local_city, _local_province, _college, _company, _carrier, _home_page, _e_mail, _mobile_num, _telphone,
					 		_auth_type, _question, _answer) {
            this.id = _id;
            this.age = _age;
            this.birthday_type = _birthday_type;
            this.birthday_month = _birthday_month;
            this.birthday_day = _birthday_day;
            this.home_region = _home_region;
            this.home_city = _home_city;
            this.home_province = _home_province;
            this.local_region = _local_region;
            this.local_city = _local_city;
            this.local_province = _local_province;
            this.college = _college;
            this.company = _company;
            this.carrier = _carrier;
            this.home_page = _home_page;
            this.e_mail = _e_mail;
            this.mobile_num = _mobile_num;
            this.telphone = _telphone;
            this.auth_type = _auth_type;
            this.question = _question;
            this.answer = _answer;
        },
        //分组模型
        Group: function (_expand, _name, _id) {
            this.Expand = _expand;
            this.Name = _name;
            this.ID = _id;
        },
        //群用户模型
        TeamUser: function (_teamid, _isboss, _isadmin, _userid, _usernick, _userstatus) {
            this.TeamID = _teamid;
            this.IsBoss = _isboss;
            this.IsAdmin = _isadmin;
            this.UserID = _userid;
            this.UserNick = _usernick;
            this.UserStatus = _userstatus;
        },
        //群组模型		
        Team: function (_teamid, _teamname, _teamicon, _teamdescript, _isboss, _isadmin, _isblocked) {
            this.TeamID = _teamid;
            this.TeamName = _teamname;
            this.TeamIcon = _teamicon;
            this.TeamDescript = _teamdescript;
            this.IsBoss = _isboss;
            this.IsAdmin = _isadmin;
            this.IsBlocked = _isblocked;
        },
        //用户配置
        Config: function (_dis, _order, _side, _teamside, _sendkey, _showtime, _userpower, _defaulpannel, _autoshowmsg) {
            this.DisType = _dis; //显示方式1默认2大图标
            this.OrderType = _order; //排序方式1状态2分组
            this.ChatSide = _side; //聊天窗口是否显示图片1是2不是
            this.TeamChatSide = _teamside; //群聊天的侧边是否显示
            this.MsgSendKey = _sendkey; //发送消息热键,1enter2ctrl+enter
            this.MsgShowTime = _showtime; //发送时间,1显示2不显示
            this.UserPower = _userpower; //用户权限
            this.DefaultPannel = _defaulpannel; //默认显示的面板(好友或群)
            this.AutoShowMsg = _autoshowmsg; //是否自动弹出信息
        },
        //消息
        Msg: function (_f, _fn, _t, _c, _type, _code, _status, _i, _time, _proccessed) {
            this.From = _f; //消息来源
            this.FromNick = _fn;  //来源方Nick
            this.To = _t; //消息去向
            this.Content = _c; //消息主体
            this.Type = _type; //消息类型
            this.Code = _code; //消息代码
            this.Status = _status; //来源方状态
            this.IsConfirm = _i; //消息是否确认
            this.AddTime = _time; //消息发布时间
            this.Proccessed = _proccessed; //是否已经显示
            //this.FontFamily = ((_fontFamily == null) || (_fontFamily == "")) ? "黑体" : _fontFamily; //消息字体
        },
        //群消息
        TeamMsg: function (_f, _fn, _t, _c, _type, _i, _time, _proccessed) {
            this.From = _f; //消息来源
            this.FromNick = _fn;  //来源方Nick
            this.To = _t; //消息去向
            this.Content = _c; //消息主体
            this.Type = _type; //1文本2特殊3添加好友4删除好友5状态改变6广告
            this.IsConfirm = _i; //消息是否确认
            this.AddTime = _time; //消息发布时间
            this.Proccessed = _proccessed; //是否已经显示
        },
        //MINI成员
        MiniTeam: function (_id, _nick, _gender, _guest) {
            this.UserID = _id;
            this.TeamNick = _nick;
            this.TeamGender = _gender;
            this.IsGuest = _guest;
        },
        //系统
        Sys: function (_c) {
            this.Code = _c; //校验码
            this.IntervalTime = 3000; //循环间隔
            this.MessagesTimer = null; //消息定时器
            this.TimeOutCount = 0;
            this.klIntervalTime = 10000; //心跳间隔
            this.KeepLiveTimer = null;
            this.DataTimer = null;
            this.dtIntervalTime = 300000;
        }
    };
    //数据
    this.Data = {
        cutContent: function (tid, uid) {

        },
        copyContent: function () {
            clipboardData.setData("text", gClipBoardData);
        },
        pasteContent: function (o) {

        },
        selectAllContent: function (o) {

        },
        getEmotionInfo: function () {
            return [["01", "微笑", "[E1]"], ["02", "大笑", "[E2]"], ["03", "扮酷", "[E3]"], ["04", "吐舌头", "[E4]"],
                    ["05", "调皮", "[E5]"], ["06", "得瑟", "[E6]"], ["07", "眨眼", "[E7]"], ["08", "害羞", "[E8]"],
                    ["09", "谢谢", "[E9]"], ["10", "激动", "[E10]"], ["11", "满足", "[E11]"], ["12", "偷笑", "[E12]"],
				    ["13", "天使", "[E13]"], ["14", "撒娇", "[E14]"], ["15", "得意", "[E15]"], ["16", "红唇", "[E16]"],
				    ["17", "好色", "[E17]"], ["18", "喜欢", "[E18]"], ["19", "恩恩", "[E19]"], ["20", "坏笑", "[E20]"],
                    ["21", "阴险", "[E21]"], ["22", "委屈", "[E22]"], ["23", "大哭", "[E23]"], ["24", "大怒", "[E24]"],
                    ["25", "生气", "[E25]"], ["26", "骂人", "[E26]"], ["27", "敲打", "[E27]"], ["28", "掌嘴", "[E28]"],
                    ["29", "打拳", "[E29]"], ["30", "找打", "[E30]"], ["31", "炸弹", "[E31]"], ["32", "鄙视", "[E32]"],
                    ["33", "哼哼", "[E33]"], ["34", "嘘", "[E34]"], ["35", "闭嘴", "[E35]"], ["36", "琢磨", "[E36]"],
                    ["37", "发呆", "[E37]"], ["38", "纠结", "[E38]"], ["39", "再见", "[E39]"], ["40", "献花", "[E40]"],
			    	["41", "掌声", "[E41]"], ["42", "唱歌", "[E42]"], ["43", "拍砖", "[E43]"], ["44", "吃饭了", "[E44]"],
                    ["45", "虎", "[E45]"], ["46", "疑问", "[E46]"], ["47", "人呢", "[E47]"], ["48", "悄悄话", "[E48]"],
                    ["49", "大喊", "[E49]"], ["50", "烧香", "[E50]"], ["51", "挖鼻", "[E51]"], ["52", "流鼻血", "[E52]"],
                    ["53", "低落", "[E53]"], ["54", "汗", "[E54]"], ["55", "囧", "[E55]"], ["56", "冷汗", "[E56]"],
                    ["57", "热啊", "[E57]"], ["58", "害怕", "[E58]"], ["59", "吃惊", "[E59]"], ["60", "抽", "[E60]"],
				    ["61", "关门", "[E61]"], ["62", "累", "[E62]"], ["63", "晕", "[E63]"], ["64", "叹气", "[E64]"],
                    ["65", "吐", "[E65]"], ["66", "无聊", "[E66]"], ["67", "打哈欠", "[E67]"], ["68", "睡觉", "[E68]"],
                    ["69", "隐身", "[E69]"], ["70", "潜水", "[E70]"], ["71", "飘过", "[E71]"], ["72", "难过", "[E72]"],
                    ["73", "抓狂", "[E73]"], ["74", "撞墙", "[E74]"], ["75", "投降", "[E75]"], ["76", "生病", "[E76]"],
                    ["77", "衰", "[E77]"], ["78", "便便", "[E78]"], ["79", "抱抱", "[E79]"], ["80", "亲亲", "[E80]"],
                    ["81", "猪", "[E81]"], ["82", "虎", "[E82]"], ["83", "太阳", "[E83]"], ["84", "月亮", "[E84]"],
                    ["85", "花开", "[E85]"], ["86", "花谢", "[E86]"], ["87", "爱心", "[E87]"], ["88", "心碎", "[E88]"],
                    ["89", "刀", "[E89]"], ["90", "西瓜", "[E90]"], ["91", "食物", "[E91]"], ["92", "生日", "[E92]"],
                    ["93", "礼品", "[E93]"], ["94", "音乐", "[E94]"], ["95", "咖啡", "[E95]"], ["96", "干杯", "[E96]"],
                    ["97", "强", "[E97]"], ["98", "弱", "[E98]"], ["99", "握手", "[E99]"], ["100", "抱拳", "[E100]"],
                    ["101", "勾手指", "[E101]"], ["102", "NO", "[E102]"], ["103", "胜利", "[E103]"], ["104", "OK", "[E104]"]];
        },
        getFontTypeInfo: function () {
            return ["宋体", "黑体", "楷体_GB2312", "幼圆", "新宋体", "微软雅黑", "隶书", "Arial", "Arial Black", "Times New Roman", "Verdana",
                "@宋体", "@黑体", "@楷体_GB2312", "@幼圆", "@新宋体", "@微软雅黑", "@隶书"];
        },
        getFontSizeInfo: function () {
            return ["12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"];
        },
        getFontFlagInfo: function () {
            return [["0", "常规"], ["1", "粗体"], ["2", "斜体"], ["3", "粗斜体"]];
        },
        getFontColorInfo: function () {
            return [["#000000", "黑色"], ["#800000", "深红色"], ["#808000", "橄榄色"], ["#000080", "藏青色"], ["#008080", "绿蓝"],
                ["#800080", "紫色"], ["#808080", "灰色"], ["#c0c0c0", "银色"], ["#0000ff", "蓝色"], ["#ff0000", "红色"],
                ["#00ff00", "浅绿色"], ["#00ffff", "浅蓝色"], ["#ffff00", "黄色"], ["#ff00ff", "紫红色"], ["#ffffff", "白色"]];
        },
        keepLive: function () {
            _me.MessageHandler.keepLive();
        },
        getMyUserInfo: function () {
            _me.MessageHandler.getMyUserInfo();
        },
        getFriendUserInfo: function (uid, cb) {
            _me.MessageHandler.getFriendUserInfo(uid, cb);
        },
        getFriendUserFullInfo: function (uid, cb) {
            _me.MessageHandler.getFriendUserFullInfo(uid, cb);
        },
        getMyConfig: function () {
            _me.MessageHandler.getMyConfig();
        },
        getMyFriendBriefList: function (cb) {
            _me.MessageHandler.getMyFriendBriefList(cb);
        },
        getMyFriendList: function (cb) {
            _me.MessageHandler.getMyFriendList(cb);
        },
        getFriendsAlias: function () {
            _me.MessageHandler.getFriendsAlias();
        },
        getMyGroupList: function (cb) {
            _me.MessageHandler.getMyGroupList(cb);
        },
        setGroupOrder: function (order) {
            _me.MessageHandler.setGroupOrder(order);
        },
        getUserGroupID: function (uid) {
            if (!_me.Friend) { return null; }
            for (var i = 0; i < _me.Friend.length; i++) {
                if (_me.Friend[i].UserID == uid) {
                    return _me.Friend[i].GroupID;
                };
            }
            return null;
        },
        //兼容分组顺序
        fixGroupOrder: function () {
            if (!_me.Group) { return; }
            _me.GroupOrder = [];
            var Pos = 0;
            for (var i = 0; i < _me.Group.length; i++) {
                if (!(_me.GroupOrder.contains(_me.Group[i].ID))) {
                    switch (_me.GroupOrder.length) {
                        case 0: { Pos = 0; break; }
                        case 1: { Pos = 3; break; }
                        case 2: { Pos = 2; break; }
                        default: { Pos = _me.GroupOrder.length - 2; break; }
                    }
                    _me.GroupOrder = _me.GroupOrder.insertAt(Pos, _me.Group[i].ID);
                };
            }
        },
        openGroup: function (gid) {
            if (!_me.Group) { return false; }
            for (var i = 0; i < _me.Group.length; i++) {
                if (_me.Group[i].ID == gid) {
                    _me.Group[i].Expand = true;
                    return true;
                };
            }
            return false;
        },
        killCometClient: function (nick, cb) {
            _me.MessageHandler.killCometClient(nick, function(){
                Other.SetCookie("WebIM_ChatRoom", "",-1000);
            });
        },
        getMyTeamList: function (cb) {
            _me.MessageHandler.getMyTeamList(cb);
        },
        getMyTeamUserList: function (tid, cb) {
            _me.MessageHandler.getMyTeamUserList(tid, cb);
        },
        getMyMsgList: function (cb) {
            _me.MessageHandler.getMyMsgList(cb);
        },
        addUserIntoMiniTeamUsers: function (tid, userid, nick, cb) {
            _me.MessageHandler.addUserIntoMiniTeamUsers(tid, userid, nick, cb);
        },
        addGuestIntoMiniTeamUsers: function (tid, cb) {
            _me.MessageHandler.addGuestIntoMiniTeamUsers(tid, cb);
        },
        getOfflineMessage: function (num) {
            _me.MessageHandler.getOfflineMessage(num);
        },
        getMyDetailInfo: function (cb) {
            _me.MessageHandler.getMyDetailInfo(cb);
        },
        updateUserSign: function () {
            _me.MessageHandler.updateUserSign();
        },
        changeUserStatus: function (us) {
            _me.MessageHandler.changeUserStatus(us);
        },
        updateUserProfile: function (NAME, GENDER, AGE, LOCAL_PROVINCE, LOCAL_CITY, SIGNATURE, AUTHER_SET, AUTHER_QUESTION, AUTHER_ANSWER) {
            //更新本地数据
            _me.Common.updateProfile(gStyleManager.getBigDefaultHeadPath(GENDER), _me.Profile.UserID, NAME, null, SIGNATURE, null, null, null, null, GENDER);
            _me.Common.updateProfileEx(_me.Profile.UserID, AGE, null, null, null, null, null, null,
							       null, LOCAL_CITY, LOCAL_PROVINCE, null, null, null, null, null, null, null,
							       AUTHER_SET, AUTHER_QUESTION, AUTHER_ANSWER);
            _me.CMD.renderMyUserInfo();
            //更新服务器数据
            _me.MessageHandler.updateUserProfile(0, NAME, 7, GENDER, 8, AGE,
											 12, LOCAL_PROVINCE, 13, LOCAL_CITY,
											 2, SIGNATURE, 27, AUTHER_SET, 29, AUTHER_QUESTION, 30, AUTHER_ANSWER);
        },
        setUserLogin: function (UserName, uid, pass, us, cb) {
            _me.MessageHandler.setUserLogin(UserName, uid, pass, us, 0, cb);
        },
        addFriend: function (uid, cb) {
            _me.MessageHandler.addFriend(uid, cb);
        },
        blacklistFriend: function (uid, cb) {
            _me.MessageHandler.blacklistFriend(uid, cb);
        },
        sendAddFriendRequest: function (uid, requestMsg) {
            _me.MessageHandler.sendAddFriendRequest(uid, requestMsg);
        },
        sendAddFriendAnswer: function (uid, answer) {
            _me.MessageHandler.sendAddFriendAnswer(uid, answer);
        },
        sendMessage: function (msg) {
            _me.MessageHandler.sendMessage(msg);
        },
        sendTeamMessage: function (msg) {
            _me.MessageHandler.sendTeamMessage(msg);
        },
        acceptAddFriend: function (uid, cb) {
            _me.MessageHandler.acceptAddFriend(uid, cb);
        },
        acceptJoinTeam: function (tid, uid, cb) {
            _me.MessageHandler.acceptJoinTeam(tid, uid, cb);
        },
        deleteFriend: function (uid) {
            _me.MessageHandler.deleteFriend(uid);
        },
        blockFriend: function (uid, status) {
            _me.MessageHandler.blockFriend(uid, status);
        },
        editCustomName: function (uid, name) {
            _me.MessageHandler.editCustomName(uid, name);
        },
        editUserGroup: function (uid, gid) {
			var u = _me.Common.getUserFromArr(uid, _me.Friend);
			if (!u) return;
			if(u.GroupID==gFixGroups.fgMyBlackList) //从黑名单中加回好友
			{
				_webIM.Data.addFriend(uid);
			}
			else
			{
				//由于服务器修改分组通知不下发uid和gid，因此修改分组回调和参数注册到事件队列中
				EventQueue.IsInstance().RegisterEvent(uid, gEventID.eiEditUserGroup,
												  function () {
													  u.GroupID = gid;
													  _me.CMD.renderMyFriend(null, null, !0);
												  });
				_me.MessageHandler.editUserGroup(uid, gid);	
			}
        },
        //生成一个不重复的分组ID
        genGroupId: function () {
            for (var i = 10; i < 254; i++) {
                if (!_me.Common.getGroupById(i)) return i;
            }
            return null; //waring 只支持最大255个分组			
        },
        addGroup: function (gname, cb) {
            _me.MessageHandler.addGroup(gname, cb);
        },
        delGroup: function (gid, cb) {
            _me.MessageHandler.delGroup(gid, cb);
        },
        editGroup: function (gid, gname, cb) {
            _me.MessageHandler.editGroup(gid, gname, cb);
        },
        updateMyPosition: function (p1, p2, status) {
            _me.MessageHandler.updateMyPosition(p1, p2, status);
        },
        enterTeam: function (p1, p2) {
            _me.MessageHandler.enterTeam(p1, p2, _me.Profile.UserStatus, _me.Profile.UserNick);
        }
    };
    this.WebService = {
        getProfile: function () { return _me.Profile; },
        getProfileEx: function () { return _me.ProfileEx; },
        getConfig: function () { return _me.Config; },
        getGroup: function () { return _me.Group; },
        //添加好友
        addFriend: function (uid) {
            _me.Data.addFriend(uid, null);
            //var wLink = gWindowManager.GetWindow(gFixFormID.fiAddFriend,gFixForm.ffLink);
            //if(wLink){wLink.win.Close();}
        },
        //发送添加好友请求
        sendAddFriendRequest: function (uid, requestMsg, gid) {
            //将通过对方认证的回调注册进队列
            EventQueue.IsInstance().RegisterEvent(uid, gEventID.eiAuthAcceptedAddReq,
			function () {
			    _me.Data.editUserGroup(uid, gid);
			    _me.CMD.renderMyFriend();
			});
            _me.Data.sendAddFriendRequest(uid, requestMsg);
        },
        sendAddFriendAnswer: function (uid, answer, gid) {
            //将通过对方认证的回调注册进队列
            EventQueue.IsInstance().RegisterEvent(uid, gEventID.eiAuthAnswerCorrect,
			function () {
			    _me.Data.editUserGroup(uid, gid);
			    _me.CMD.renderMyFriend();
			});
            _me.Data.sendAddFriendAnswer(uid, answer);
        },
        //关闭自身
        closeForm: function (_type, _id) {
            var wLink = gWindowManager.GetWindow(_id, gFixForm.ffLink); //存在
            if (wLink) { wLink.win.Close(); };
        },
        updateUserProfile: function (_nick, _gender, _age, _local_province, _local_city, _sign, _auth_type, _question, _answer) {
            _me.Data.updateUserProfile(_nick, _gender, _age, _local_province, _local_city, _sign, _auth_type, _question, _answer);
            _me.CMD.renderMyUserInfo();
        },
        //显示用户详细资料
        showUserProfile: function (uid, nick, age, gender, sign) {
            EventQueue.IsInstance().RegisterEvent(uid, gEventID.eiQueryOnlineUserInfo,
			function (Data) {			
			    _me.Common.showLink(280, 220, null, null, uid, "好友 " + nick + " 详细资料",
									_me.MessageHandler.getProcessUrl("ShowUserInfoUrl") + "?domain=" + gRunerDomain + "&uid=" + Data['uid'] +
                                    "&nick=" + encodeURIComponent(nick) + "&age=" + age + "&gender=" + encodeURIComponent(gender) +
                                    "&Province=" + encodeURIComponent(Data['Province']) + "&City=" + encodeURIComponent(Data['City']) + "&sign=" + encodeURIComponent(sign),
									gImgServer + "/Images/ToolAddFriend.gif");
			});
            _me.Data.getFriendUserFullInfo(uid);
        },
        enterTeam: function (tid) {
            _me.Profile == null ? _me.CMD.intMiniLoginWindow(function () { _me.CMD.directTeamChat(tid); }) : _me.CMD.directTeamChat(tid);
        }
    };
    //操作
    this.CMD = {
        //添加分组
        addGroup: function (cb) {
            _me.Common.showPrompt("", "请输入组名",
			function () {
			    var gname = arguments[2].enCodeHTML(); //.enCodeHTML(); //strip();
			    if (gname == "") return;
			    if (gname.len() > 16) {
			        _me.Common.showAlert("填写的组名过长！", "提示");
			        return;
			    }
			    if (!_me.Common.getGroupByName(gname))//不存在
			    {
			        _me.Data.addGroup(gname,
					function (o) {
					    _me.Data.getMyGroupList(
							function (o) {
							    _me.CMD.setGroupOrder(); //更新分组顺序							    
							    _me.Data.getMyFriendList(function () { _me.CMD.renderMyFriend(null, null, !0); });
							});
					    _me.CMD.notifyWebPageGroup(null, gname);
					});
			    }
			    else {
			        _me.Common.showAlert("这个组名已经存在!", "提示");
			    }
			});
        },
        //编辑分组
        editGroup: function (gid) {
            if ((gid == 0) || (gid == 1) || (gid == 2)) {
                _me.Common.showAlert("不能重命名默认分组!", "提示");
                return;
            }
            var g = _me.Common.getGroupById(gid);
            if (!g) return;
            _me.Common.showPrompt(g.Name.deCodeHTML(), "请输入组名",
			function () {
			    var gname = arguments[2].enCodeHTML(); //.enCodeHTML(); //strip();
			    if (gname == g.Name || gname == "") return;
			    if (gname.len() > 16) {
			        _me.Common.showAlert("填写的组名过长！", "提示");
			        return;
			    }
			    if (!_me.Common.getGroupByName(gname))//不存在
			    {
			        g.Name = gname;
			        _me.Data.editGroup(gid, gname, _me.CMD.notifyWebPageGroup(gid, null));
			        _me.CMD.renderMyFriend();
			    }
			    else {
			        _me.Common.showAlert("这个组名已经存在!", "提示");
			    }
			});
        },
        //删除分组
        delGroup: function (gid) {
            if ((gid == 0) || (gid == 1) || (gid == 2)) {
                _me.Common.showAlert("不能删除默认分组!", "提示");
                return;
            }
            var g = _me.Common.getGroupById(gid);
            _me.Common.showConfirm("确定要删除“" + g.Name + "”这个分组(该组好友会移至默认组)？", "提示",
			function () {
			    if (arguments[2]) {
			        _me.Data.delGroup(gid,
						function (o) {
						    _me.Data.getMyGroupList(
								function (o) {
								    _me.Data.getMyFriendList(function () { _me.CMD.renderMyFriend(null, null, !0); });
								});
						    _me.CMD.notifyWebPageGroup(null, null);
						});
			    }
			});
        },
        setGroupOrder: function () {
            _me.Data.fixGroupOrder();
            _me.Data.setGroupOrder(_me.GroupOrder);
        },
        openGroup: function (gid) {
            if (_me.Data.openGroup(gid)) { _me.CMD.renderMyFriend(); }
        },
        getOnlineCount: function (sid) {
            if (!_me.Sys.DataTimer) {
                var cb = function () {
                    $("divOnlines").innerHTML = "在线人数更新中";
                    _me.ReqQueue.add({ method: 'GET',
                        uri: gDeployPath + gAjaxProxyNoEncodeUrl + gTeamServer + "/AjaxHandler/GetOnlineCount.asp",
                        callback: function (o) {
                            if (!o) return;
                            $("divOnlines").innerHTML = "IM在线 " + o.responseText + " 人";
                        },
                        retry: 1,
                        timeout: 3000
                    });
                    _me.ReqQueue.flush();
                }
                cb();
                _me.Sys.DataTimer = setInterval(cb, _me.Sys.dtIntervalTime);
            }
        },
        showFriendBar: function () {
            if (_me.Friend) {
                $("imgAddFriend").src = gStyleManager.getStyleResPath(gStyle) + "myfriend.gif";
                $("btnAddFriend").onmouseover = null;
                var Onlines = 0;
                for (var i = 0; i < _me.Friend.length; i++) //将好友保存到临时对象
                {
                    var u = _me.Friend[i];
                    if (u.UserID != -1) {
                        if ((u.UserStatus != gUserStatus.usHide) && (u.UserStatus != gUserStatus.usOffline)) Onlines++;  //更新数量
                    }
                }
                $("myFriendCount").innerHTML = "好友(" + Onlines + ")";
            }
            else {
                $("imgAddFriend").src = gStyleManager.getStyleResPath(gStyle) + "sidebar_add_friend.gif";
                $("btnAddFriend").onmouseover = function () { this.scrollTop = '24'; };
                $("btnAddFriend").onclick = function () { _me.CMD.intMiniLoginWindow(); };
                $("myFriendCount").innerHTML = "";
            }
        },
        //通知外部网页刷新分组
        notifyWebPageGroup: function (gid, gname) {
            var iframe = $("link" + gFixFormID.fiAddFriendSucess + "Type" + gFixForm.ffLink);
            if (iframe) {
                //FF和ie下取iframe方式不同
                var ifr = document.frames ? document.frames("link" + gFixFormID.fiAddFriendSucess + "Type" + gFixForm.ffLink) :
                                            document.getElementById("link" + gFixFormID.fiAddFriendSucess + "Type" + gFixForm.ffLink).contentWindow;
                ifr.ShowGroup(gid, gname);
            }
        },
        //创建群
        showCreateTeam: function (uid) {
            var url = "WebIMPage/CreateTeam.asp?v=" + Math.random();
            if (uid) url += "&uid=" + uid;
            _me.Common.showLink(350, 160, 50, (WebElement.Width() - 450) / 2, 14, "创建群组", url, gImgServer + "/Images/toolmsghistory.gif");
        },
        //查找群
        searchTeam: function (uid) {
            var url = "WebIMPage/SearchTeam.asp?v=" + Math.random();
            if (uid) url += "&uid=" + uid;
            _me.Common.showLink(350, 160, 50, (WebElement.Width() - 450) / 2, 15, "查找群组", url, gImgServer + "/Images/toolmsghistory.gif");
        },
        //显示好友卡片
        showCard: function (uid, o) {
            var u = uid == _me.Profile.FormID ? _me.Profile : _me.Common.getUserFromArr(uid, _me.Friend);
            if (!u) return;
            var wWin = _me.Win;
            var top = WebElement.GetY($(o), $("windowContainerType" + gFixForm.ffMain[0] + "Id" + gFixForm.ffMain[1])) + 2 - $(o).parentNode.parentNode.parentNode.scrollTop;
            var left = wWin.win.Left < 233 ? wWin.win.Left + wWin.win.Width + 2 : wWin.win.Left - 230;
            var oCard = $("divCardBorder");
            if (!oCard) {
                oCard = WebElement.New("div", "divCardBorder", "wCardBorder");
                WebElement.Add("windowContainerType" + gFixForm.ffMain[0] + "Id" + gFixForm.ffMain[1], oCard);
            }
            oCard.setAttribute("top", top - WebElement.scrollHeight());
            var os = oCard.style;
            os.top = top + "px";
            os.left = left + "px";
            WebElement.Value(oCard);
            var oCardContent = WebElement.New("div", "", "wCardContainer", _me.Common.createUserItem(3, u));
            var ban = u.IsBlocked ? "b" : "";
            var img = WebElement.New("img", "", "wCardCloseImage");
            img.src = gImgServer + "/Images/Close.gif";
            img.title = "关闭";
            img.onmouseover = function () { this.src = gImgServer + "/Images/CloseHover.gif"; };
            img.onmouseout = function () { this.src = gImgServer + "/Images/Close.gif"; };
            img.onclick = function () { WebElement.Hid("divCardBorder"); };
            WebElement.Add(oCardContent, img);
            WebElement.Add(oCard, oCardContent);
            document.onmousedown = function (e) {
                var ex = Evt.Left(e);
                var ey = Evt.Top(e);
                if (!(ex > parseInt(os.left) && ex < parseInt(os.left) + 228 + 2 && ey > parseInt(os.top) && ey < parseInt(os.top) + 160 + 2)) {
                    WebElement.Hid("divCardBorder");
                    document.onmousedown = null;
                }
            };
            WebElement.Show(oCard);
        },
        //修改用户分组
        eidtUserGroup: function (uid) {
            var u = _me.Common.getUserFromArr(uid, _me.Friend);
            if (!u) return;
            _me.Common.showSelect("请选择分组",
			function (id, t) {
			    var oSel = $("divSelectId" + id + "Type" + t);
			    for (var i = 0; i < _me.Group.length; i++) {
			        var g = _me.Group[i];
			        if ((g.ID == 1) || (g.ID == 2)) { continue; }
			        var opt = WebElement.New("option");
			        opt.value = g.ID;
			        opt.text = g.Name.deCodeHTML();
			        oSel.options.add(opt);
			    }
			    WebElement.Value(oSel, u.GroupID);
			},
			function () {
			    var gid = arguments[2];
			    if (gid == "" || gid == u.GroupID) return; //无改变
			    _me.Data.editUserGroup(uid, gid);
			});
        },
        //修改好友别名
        editCustomName: function (uid) {
            var u = _me.Common.getUserFromArr(uid, _me.Friend);
            if (!u) return;
            _me.Common.showPrompt(u.CustomName, "请输入别名",
			function () {
			    var name = arguments[2].trim();
			    if (name != "") {
			        if (name == u.CustomName) return; //无改变
			        _me.Data.editCustomName(uid, name);
			        u.CustomName = name;
			        _me.CMD.renderMyFriend();
			    }
			    else//清除别名
			    {
			        _me.Data.editCustomName(uid, "");
			        u.CustomName = u.UserNick;
			        _me.CMD.renderMyFriend();
			    }
			});
        },
        //显示聊天记录
        showMsgHistory: function (uid) {
            var url = "WebIMPage/Message.asp?v=" + Math.random();
            if (uid) url += "&id=" + uid;
            _me.Common.showLink(550, 450, 50, (WebElement.Width() - 450) / 2, 9, "聊天记录", url, gImgServer + "/Images/toolmsghistory.gif");
        },
        //显示群聊天记录
        showTeamMsgHistory: function (tid) {
            var url = "WebIMPage/TeamMessage.asp?v=" + Math.random();
            if (tid) url += "&tid=" + tid;
            _me.Common.showLink(550, 450, 50, (WebElement.Width() - 450) / 2, 9, "聊天记录", url, gImgServer + "/Images/toolmsghistory.gif");
        },
        //显示管理页面
        showManage: function (uid) {
            var url = "WebIMPage/usermanage.asp?v=" + Math.random();
            _me.Common.showLink(550, 450, 50, (WebElement.Width() - 450) / 2, 12, "管理", url, gImgServer + "/Images/toolmanage.gif");
        },
        //屏蔽好友
        blockFriend: function (uid, isblock) {
            _me.Data.blockFriend(uid, isblock);
            var users = [];
            users = _me.Friend;
            var u = _me.Common.getUserFromArr(uid, users);
            u.IsBlocked = isblock == 1;
            if ($("wChatButtonBlockId" + uid + "Type1")) {
                var oBtn = $("wChatButtonBlockId" + uid + "Type1");
                var isblock = 3 - parseInt(oBtn.getAttribute("b"));
                $T(oBtn, "img")[0].src = isblock == 1 ? gImgServer + "/Images/chatbuttoncancelblock.gif" : gImgServer + "/Images/chatbuttonblock.gif";
                $T(oBtn, "img")[0].title = isblock == 1 ? "取消阻止此好友" : "阻止此好友";
                oBtn.setAttribute("b", isblock);
            }
            _me.CMD.renderMyFriend();
        },
        //删除好友
        delFriend: function (uid) {
            var users = [];
            users = _me.Friend;
            var u = _me.Common.getUserFromArr(uid, users);
            _me.Common.showConfirm("确定要删除“" + u.UserNick + "”这位好友吗？", "提示",
		function () {
		    if (arguments[2]) {
		        var wChat = gWindowManager.GetWindow(uid, 1);
		        if (wChat) wChat.win.Close();
		        _me.Friend.remove(u);
		        HeadManager.IsInstance().DelUserHeadByUser(uid);
		        _me.CMD.renderMyFriend();
		        _me.Data.deleteFriend(uid);
		    }
		});
        },
        //循环获取消息
        getMsgInterval: function () {
            if (!_me.Sys.MessagesTimer) {
                _me.Sys.MessagesTimer = setInterval(
				function () {
				    _me.Data.getMyMsgList(
						function (o) {
						    if (!o) return;
						    if (!$T(o.responseXML, "list")) return;
						    var msgs = _me.Common.getMsgFromXml($T(o.responseXML, "list").item(0));
						    if (msgs) {
						        for (var i = 0; i < msgs.length; i++) {
						            MessageQueue.IsInstance().AcceptMessage(msgs[i]); //将消息存入队列
						            _me.CMD.ProcessMessage(msgs[i].From, msgs[i]); //处理
						        }
						    }
						});
				}
				, _me.Sys.IntervalTime);
            }
        },
        //响应消息
        OnRecvMessages: function (responseXML) {
            if (!responseXML) return;
            if (!$T(responseXML, "list")) return;
            var msgs = _me.Common.getMsgFromXml($T(responseXML, "list").item(0));
            if (msgs) {
                for (var i = 0; i < msgs.length; i++) {
                    msgs[i].Content = (msgs[i].Content).enCodeHTML();
                    MessageQueue.IsInstance().AcceptMessage(msgs[i]); //将消息存入队列
                    _me.CMD.ProcessMessage(msgs[i].From, msgs[i]); //处理
                }
            }
        },
        //停止循环获取消息
        stopMsgInterval: function () {
            clearInterval(_me.Sys.MessagesTimer);
            _me.Sys.MessagesTimer = null;
        },
        getOfflineMessage: function (num) {
            _me.Data.getOfflineMessage(num);
        },
        updateMyPosition: function () {
            _me.Data.updateMyPosition($U("p1"), $U("p2"), _me.Profile.UserStatus);
        },
        enterTeam: function (p1, p2) {
            _me.Data.enterTeam(p1, p2);
        },
        //显示注册窗体
        showRegWindow: function () {
            var wLogin = gWindowManager.GetWindow(6, 3);
            if (wLogin) wLogin.win.Minimize();
            _me.Common.showLink(350, 320, 50, (WebElement.Width() - 350) / 2, 7, "注册新帐号", "WebIMPage/reg.asp?v=" + Math.random(), null,
			function () {
			    if (wLogin.isMin) wLogin.win.Minimize();
			});
        },
        showUserStatusSwitch: function (e, w, id, t) {
            var m = MenuManager.IsInstance().GetPopMenu(t, id);
            m.Data = ["在线|_webIM.CMD.changeUserStatus(gUserStatus.usOnline)|" + gStyleManager.getStyleResPath(gStyle) + "s1.gif",
				"忙碌|_webIM.CMD.changeUserStatus(gUserStatus.usBusy)|" + gStyleManager.getStyleResPath(gStyle) + "s2.gif",
				"离开|_webIM.CMD.changeUserStatus(gUserStatus.usLeave)|" + gStyleManager.getStyleResPath(gStyle) + "s3.gif",
				"隐身|_webIM.CMD.changeUserStatus(gUserStatus.usHide)|" + gStyleManager.getStyleResPath(gStyle) + "s-1.gif",
            //"离线|_webIM.CMD.changeUserStatus(0)|"+gStyleManager.getStyleResPath(gStyle) + "s4.gif",
				"",
				"注销|_webIM.CMD.logoutWebIM(1,1)|" + gStyleManager.getStyleResPath(gStyle) + "s4.gif"];
            m.E = e;
            m.HasIcon = true;
            m.Width = 80;
            m.ShowCorner = !!1;
            m.ShowLeftBG = !!1;
            m.Show();
        },
        //侧边栏初始化
        initSideBar: function () {
            var wSideBar = gWindowManager.GetWindow(0, gFormSideBar); //存在侧边			
            if (wSideBar) {
                if (wMain.isMin) wMain.win.Minimize();
                wMain.win.Focus();
                return;
            }
            var w = new SideBar();
            w.Width = 276;
            w.Height = 30;
            w.Moveable = 0;
            w.OnPaint = function (w, h, id, t) {
                $("SideBarMainId" + id + "Type" + t).style.height = h + "px";
            };
            w.OnLoaded = function (id, t) {
                $("btnJoinId" + id + "Type" + t).onclick = function () {
                    //_me.CMD.intLoginWindow();
                    _me.CMD.intMiniLoginWindow(showLoginAfter);
                };
                $("btnAddFriend").onclick = function () {
                    _me.Profile == null ? _me.CMD.intMiniLoginWindow() : _me.Common.showLink(450, 385, w.Top, w.Left < 355 ? w.Left + w.Width + 2 : w.Left - 352, gFixFormID.fiAddFriend, "添加好友", _me.MessageHandler.getProcessUrl("OnlineUserSearchUrl") + "?domain=" + gRunerDomain, gImgServer + "/Images/ToolAddFriend.gif"); ;
                };
                $("btnSwitchId" + id + "Type" + t).onclick = function () {
                    w.Switch();
                };
                _me.CMD.getOnlineCount();
                if (Other.GetCookie("pk2_OA_UID")) _me.CMD.loginWebIMBkByUID(Other.GetCookie("pk2_OA_UID"), "dfgerysdfj23474fdsdf");

            };
            w.OnClose = _me.CMD.destroyMainWindow;
            w.Show();
            showLoginBefore();
        },
        //登陆窗口初始化
        intLoginWindow: function () {
            var wMain = gWindowManager.GetWindow(0, 2); //存在主窗口
            if (wMain) {
                if (wMain.isMin) wMain.win.Minimize();
                wMain.win.Focus();
                return;
            }
            var wLogin = gWindowManager.GetWindow(6, 3); //存在登陆窗口
            if (wLogin) {
                if (wLogin.isMin) wLogin.win.Minimize();
                wLogin.win.Focus();
                return;
            }
            var w = new WebForm();
            w.Title = " " + _me.Version;
            w.Icon = gImgServer + "/Images/DefaultIcon.gif";
            w.Type = gFixForm.ffLogin[0];
            w.FormID = gFixForm.ffLogin[1];
            w.Height = 400;
            w.Width = 210;
            w.Left = 350;
            w.ContentUrl = gUITempletServer + "/UITemplete/Login.htm";
            w.Resizeable = !!0;
            w.OnPaint = function (w, h, id, t) { $("wOtherMainId" + id + "Type" + t).style.height = h + "px"; };
            w.OnLoaded = function (id, t) {
                $("divStatusId6Type3").onclick = function (e) {
                    var m = MenuManager.IsInstance().GetPopMenu(t, id);
                    m.Data = ["在线|_webIM.CMD.changeUserStatus(gUserStatus.usOnline)|" + gStyleManager.getStyleResPath(gStyle) + "s1.gif",
						"忙碌|_webIM.CMD.changeUserStatus(gUserStatus.usBusy)|" + gStyleManager.getStyleResPath(gStyle) + "s2.gif",
						"离开|_webIM.CMD.changeUserStatus(gUserStatus.usLeave)|" + gStyleManager.getStyleResPath(gStyle) + "s3.gif",
						"隐身|_webIM.CMD.changeUserStatus(gUserStatus.usHide)|" + gStyleManager.getStyleResPath(gStyle) + "s-1.gif",
                    //"离线|_webIM.CMD.changeUserStatus(0)|"+gStyleManager.getStyleResPath(gStyle) + "s4.gif",
						"",
						"注销|_webIM.CMD.logoutWebIM(1,1)|" + gStyleManager.getStyleResPath(gStyle) + "s4.gif"];
                    m.E = e;
                    m.HasIcon = true;
                    m.Width = 120;
                    m.Show();
                };
                WebElement.Value("tbUserNameId6Type3", Other.GetCookie("stremail"));
                WebElement.Value("tbPassId6Type3", Other.GetCookie("strpass"));
                $("cbSaveUserId6Type3").checked = Other.GetCookie("saveemail") == "1";
                $("cbSavePassId6Type3").checked = Other.GetCookie("savepass") == "1";
                $("cbAutoLoginId6Type3").checked = Other.GetCookie("autologin") == "1";
                if (Other.GetCookie("loginstatus").trim() != "") _webIM.CMD.changeLoginStatus(parseInt(Other.GetCookie("loginstatus")));
                new CheckBox("cbSaveUserId6Type3", "保存我的信息(B)").Render();
                new CheckBox("cbSavePassId6Type3", "记住我的密码(R)").Render();
                new CheckBox("cbAutoLoginId6Type3", "自动为我登陆(N)").Render();
                $("tbPassId6Type3").onkeydown = function (e) {
                    var e = e || event;
                    if (e.keyCode == 13)//回车
                    {
                        _me.CMD.loginWebIM($F("tbUserNameId6Type3").trim(), $F("tbPassId6Type3").trim(), $("divStatusId6Type3").getAttribute("us"));
                    }
                }
                $("btnLoginId6Type3").onclick = function () { _me.CMD.loginWebIM($F("tbUserNameId6Type3").trim(), $F("tbPassId6Type3").trim(), $("divStatusId6Type3").getAttribute("us")); };
                $("linkDelCookieId6Type3").onclick = function () {
                    Other.SetCookie("stremail", "");
                    Other.SetCookie("strpass", "");
                    Other.SetCookie("saveemail", "2");
                    Other.SetCookie("savepass", "2");
                    Other.SetCookie("autologin", "2");
                    Other.SetCookie("loginstatus", "0");
                    _me.CMD.destroyMainWindow();
                    IntWebIM();
                };
                if (Other.GetCookie("autologin") == "1") {
                    _me.CMD.loginWebIM($F("tbUserNameId6Type3").trim(), $F("tbPassId6Type3").trim(), $("divStatusId6Type3").getAttribute("us")); //自动登陆
                }
            };
            w.OnClose = _me.CMD.destroyMainWindow;
            w.Show();
            w.Focus();
        },
        //Mini登陆窗口初始化
        intMiniLoginWindow: function (cb) {
            var wMain = gWindowManager.GetWindow(gFixForm.ffMain[1], gFixForm.ffMain[0]); //存在主窗口
            if (wMain) {
                if (wMain.isMin) wMain.win.Minimize();
                wMain.win.Focus();
                return;
            }
            var wLogin = gWindowManager.GetWindow(gFixForm.ffLogin[1], gFixForm.ffLogin[0]); //存在登陆窗口
            if (wLogin) {
                if (wLogin.isMin) wLogin.win.Minimize();
                wLogin.win.Focus();
                return;
            }
            var w = new WebForm();
            w.Title = "用户登录" + _me.Version;
            w.TitleXOffset = 10;
            w.Icon = gImgServer + "/Images/DefaultIcon.gif";
            w.Type = gFixForm.ffLogin[0];
            w.FormID = gFixForm.ffLogin[1];
            w.Height = 185;
            w.Width = 260;
            w.Left = 450;
            w.Position = gFormPosition.fpWindowCenter;
            w.ContentUrl = gUITempletServer + "/UITemplete/MiniLogin.htm";
            w.Resizeable = !!0;
            w.OnPaint = function (w, h, id, t) { $("wOtherMainId" + id + "Type" + t).style.height = h + "px"; };
            w.OnLoaded = function (id, t) {
                $("divStatusId" + id + "Type" + t).onclick = function (e) {
                    var m = MenuManager.IsInstance().GetPopMenu(t, id);
                    m.Data = ["在线|_webIM.CMD.changeLoginStatus(gUserStatus.usOnline)|" + gStyleManager.getStyleResPath(gStyle) + "s1.gif",
						"忙碌|_webIM.CMD.changeLoginStatus(gUserStatus.usBusy)|" + gStyleManager.getStyleResPath(gStyle) + "s2.gif",
						"离开|_webIM.CMD.changeLoginStatus(gUserStatus.usLeave)|" + gStyleManager.getStyleResPath(gStyle) + "s3.gif",
						"隐身|_webIM.CMD.changeLoginStatus(gUserStatus.usHide)|" + gStyleManager.getStyleResPath(gStyle) + "s-1.gif"];
                    m.E = e;
                    m.HasIcon = true;
                    m.Width = 80;
                    m.ShowLeftBG = !!1;
                    m.Show();
                };
                WebElement.Value("tbUserNameId6Type3", Other.GetCookie("stremail"));
                /*WebElement.Value("tbPassId6Type3",Other.GetCookie("strpass"));
                $("cbSaveUserId6Type3").checked = Other.GetCookie("saveemail")=="1";
                $("cbSavePassId6Type3").checked = Other.GetCookie("savepass")=="1";
                $("cbAutoLoginId6Type3").checked = Other.GetCookie("autologin")=="1";*/
                //_webIM.CMD.changeLoginStatus(gUserStatus.usOnline);
                if (Other.GetCookie("loginstatus").trim() != "") _webIM.CMD.changeLoginStatus(parseInt(Other.GetCookie("loginstatus")));
                new CheckBox("cbSaveUserId6Type3", "保存我的信息(B)").Render();
                new CheckBox("cbSavePassId6Type3", "记住我的密码(R)").Render();
                new CheckBox("cbAutoLoginId6Type3", "自动为我登陆(N)").Render();
                $("tbPassId6Type3").onkeydown = function (e) {
                    var e = e || event;
                    if (e.keyCode == 13)//回车
                    {
                        _me.CMD.loginWebIM($F("tbUserNameId6Type3").trim(), $F("tbPassId6Type3").trim(), $("divStatusId6Type3").getAttribute("us"), cb);
                    }
                }
                $("btnLoginId6Type3").onclick = function () { _me.CMD.loginWebIM($F("tbUserNameId6Type3").trim(), $F("tbPassId6Type3").trim(), $("divStatusId6Type3").getAttribute("us"), cb); };
                $("linkDelCookieId6Type3").onclick = function () {
                    Other.SetCookie("stremail", "");
                    Other.SetCookie("strpass", "");
                    Other.SetCookie("saveemail", "2");
                    Other.SetCookie("savepass", "2");
                    Other.SetCookie("autologin", "2");
                    Other.SetCookie("loginstatus", "0");
                    _me.CMD.destroyMainWindow();
                };
                if (Other.GetCookie("autologin") == "1") {
                    _me.CMD.loginWebIM($F("tbUserNameId6Type3").trim(), $F("tbPassId6Type3").trim(), $("divStatusId6Type3").getAttribute("us"), cb); //自动登陆
                }
                if (Other.GetCookie("stremail")) { $("tbPassId6Type3").focus(); }
            };
            //w.OnClose = _me.CMD.destroyMainWindow;
            w.ShowModal();
            w.Focus();
        },
        loginWebIMBkByUID: function (uid, pass) {
            var loginTimer = setInterval(
			function () {
			    clearInterval(loginTimer);
			    _me.Data.setUserLogin(0, uid, pass, gUserStatus.usOnline);
			}
			, 1000);
        },
        //登陆
        loginWebIM: function (username, password, status, cb) {
            //var w = gWindowManager.GetWindow(6, 3).win;
            var user = username;
            var pass = password;
            if (user == "") {
                _me.Common.showAlert("请输入你的用户名!", "提示",
				function () {
				    if (w) {
				        w.Focus();
				        $("tbUserNameId6Type3").focus();
				    }
				});
                return;
            }
            if (pass == "") {
                _me.Common.showAlert("请输入你的密码!", "提示",
				function () {
				    if (w) {
				        w.Focus();
				        $("tbPassId6Type3").focus();
				    }
				});
                return;
            }
            var us = status;
            if (cb) { EventQueue.IsInstance().RegisterEvent("userLogin", gEventID.eiLoginSuccess, cb); };
            _me.Data.setUserLogin(user, 0, pass, us);
        },
        loginCallBack: function (sid, responseXML) {
            var w = gWindowManager.GetWindow(gFixForm.ffLogin[1], gFixForm.ffLogin[0]);
            var result = parseInt(Xml.First($T(responseXML, "list").item(0), "code"));
            if ((result == 0) || (result == 3)) {
                //Other.SetCookie("chatroomUser", _me.Profile.UserNick,-1000);
                if (!w) //没有登录窗体，则视为后台登录	
                {
                    _me.Sys.Code = parseInt(Xml.First($T(responseXML, "list").item(0), "code"));
                    Other.SetCookie("loginstatus", gUserStatus.usOnline);
                    _me.ServerSessionID = sid;
                    _me.Profile.UserStatus = gUserStatus.usOnline;
                    _me.CMD.intMainWindow();
                    _me.EventQueue.ProcessEvent("userLogin", gEventID.eiLoginSuccess, null);
                    return;
                }
                w = w.win;
                var user = $F("tbUserNameId6Type3").trim();
                var pass = $F("tbPassId6Type3").trim();
                var us = $("divStatusId6Type3").getAttribute("us");
                Other.SetCookie("loginstatus", us);
                Other.SetCookie("stremail", $("cbSaveUserId6Type3").checked ? user : "");
                Other.SetCookie("strpass", $("cbSavePassId6Type3").checked ? pass : "");
                Other.SetCookie("saveemail", $("cbSaveUserId6Type3").checked ? "1" : "2");
                Other.SetCookie("savepass", $("cbSavePassId6Type3").checked ? "1" : "2");
                Other.SetCookie("autologin", $("cbAutoLoginId6Type3").checked ? "1" : "2");
                w.Close();
                //Other.SetCookie("pk2_OA_UID","822442282");
                _me.Sys.Code = parseInt(Xml.First($T(responseXML, "list").item(0), "code"));
                _me.ServerSessionID = sid;
                _me.Profile.UserStatus = us;
                //_me.CMD.enterTeam($U("p1"),$U("p2"));  //进入临时群
                _me.CMD.intMainWindow();
                _me.EventQueue.ProcessEvent("userLogin", gEventID.eiLoginSuccess, null);
            }
            else {
                if (w) {
                    if (w) w.win.Focus();
                    $("tbUserNameId6Type3").focus();
                    _me.Common.showAlert("登陆失败,请检查输入的帐户信息是否正确!", "提示");
                    //WebElement.Toggle("divLoginId6Type3","divLoginingId6Type3");
                }
                else {
                    _me.Common.showAlert("登陆失败,请检查输入的帐户信息是否正确!", "提示");
                }
            }
        },
        //主窗体初始化
        intMainWindow: function (top, left) {
            if (gWindowManager.GetWindow(gFixForm.ffMain[1], gFixForm.ffMain[0])) return;
            var w = new WebForm();
            w.Title = "<span style='color:#000'>" + _me.Version + "</span>";
            w.TitleXOffset = 10;
            w.Icon = gImgServer + "/Images/MainIcon.gif";
            w.Type = gFixForm.ffMain[0];
            w.FormID = gFixForm.ffMain[1];
            w.Height = 400;
            w.Width = 254;
            w.Top = top || WebElement.Height() - 440;
            w.Left = left || WebElement.Width() - 260;
            w.MinHeight = 250;
            w.MinWidth = 254;
            w.OnPaint = function (w, h, id, t) {
                //设置好友面板
                $("wMainMainId" + id + "Type" + t).style.height = (h - 154) + "px";   //设置主区域高度
                $("wMainUserInfoRightId" + id + "Type" + t).style.width = (w - 82) + "px";
                $("wMainSearchUserId" + id + "Type" + t).style.width = (w - 65) + "px";
                $("inputSearchId" + id + "Type" + t).style.width = (w - 60) + "px";
                $("txtUserSignId" + id + "Type" + t).style.width = (w - 85) + "px";

                //设置群面板
                $("wMainTeamGroupId" + id + "Type" + t).style.height = (h - 154) + "px";   //设置主区域高
            };
            w.OnBeforeClose = function () {
                _me.Common.showConfirm("真的要退出？", "提示",
			function () {
			    if (arguments[2]) {
			        _me.CMD.logoutWebIM(0, 1);
			        _me.Win.win.OnBeforeClose = null;
			        _me.Win.win.Close();
			    }
			});
            };
            w.OnClose = function () {
                _me.CMD.destroyMainWindow();
            };
            w.OnLoaded = function (id, t) //t=2 为主窗口体
            {
                w.ShowLoading();
                _me.Win = gWindowManager.GetWindow(id, t);
                _me.Data.getMyConfig();
                _me.Data.getMyUserInfo();  //在Chunck消息处理器中不处理，个人信息经由登录返回取得
                _me.Data.getMyGroupList();
                _me.Data.getMyFriendList();
                //_me.Data.getMyTeamList();
                _me.Data.getMyDetailInfo();
                var intInteralID = setInterval(
					function () {
					    if (_me.Config && _me.Profile && _me.ProfileEx && _me.Group && _me.GroupOrder && _me.Friend && (_me.FriendLoadedCount == _me.Friend.length)/*&&_me.Team*/) //资料全部下载完成后才开始处理
					    {
					        clearInterval(intInteralID);
					        _me.CMD.showFriendBar();    //更新工具条
					        //_me.Data.getFriendsAlias(); //获取好友别名
					        _me.CMD.renderMyUserInfo(); //显示我的信息
					        _me.CMD.renderMyFriend();   //显示我的好友								
					        //_me.CMD.renderMyTeam();     //显示我的群																	
					        _me.KeepLive.startKeepLive();
					        $("wMainUserSignId" + id + "Type" + t).onclick = function () //设置签名点击事件
					        {
					            WebElement.Show("txtUserSignId" + id + "Type" + t);
					            WebElement.Value("txtUserSignId" + id + "Type" + t, _me.Profile.UserSign);
					            $("txtUserSignId" + id + "Type" + t).focus();
					            $("txtUserSignId" + id + "Type" + t).onblur = function () {
					                var sign = $F(this).trim();
					                if (_me.Profile.UserSign != sign)//签名有更改就需要update
					                {
					                    _me.Profile.UserSign = sign;
					                    _me.CMD.renderMyUserInfo();
					                    _me.Data.updateUserSign();
					                }
					                WebElement.Hid("txtUserSignId" + id + "Type" + t);
					            };
					        };
					        $("wMainMainId" + id + "Type" + t).onclick = function (e) //主窗体点击事件
					        {
					            var e = e || window.event;
					            var tar = e.srcElement || e.target;
					            if (tar) {

					            }
					        };
					        $("btnUserStatusSwitch" + id + "Type" + t).parentNode.onclick = function (e)//在线状态菜单
					        {
					            _me.CMD.showUserStatusSwitch(e, w, id, t);
					        };
					        $("MyUsers").onclick = function ()//好友切换按钮
					        {
					            if (parseInt(this.getAttribute("s")) == 1) return;
					            $("MyTeams").className = "UTNomal";
					            $("MyUsers").className = "UTCurrent";
					            $("MyTeams").setAttribute("s", 0);
					            $("MyUsers").setAttribute("s", 1);
					            WebElement.Show($("wMainMainId" + id + "Type" + t));
					            WebElement.Hid($("wMainTeamGroupId" + id + "Type" + t));
					        };
					        $("MyUsers").onmouseover = function () {
					            if (parseInt(this.getAttribute("s")) == 1) return;
					            $("MyUsers").className = "UTHover";
					        };
					        $("MyUsers").onmouseout = function () {
					            if (parseInt(this.getAttribute("s")) == 1) return;
					            $("MyUsers").className = "UTNomal";
					        };
					        $("MyTeams").onclick = function ()//群组切换按钮
					        {
					            if (parseInt(this.getAttribute("s")) == 1) return;
					            $("MyUsers").className = "UTNomal";
					            $("MyTeams").className = "UTCurrent";
					            $("MyUsers").setAttribute("s", 0);
					            $("MyTeams").setAttribute("s", 1);
					            WebElement.Hid($("wMainMainId" + id + "Type" + t));
					            WebElement.Show($("wMainTeamGroupId" + id + "Type" + t));
					        };
					        $("MyTeams").onmouseover = function () {
					            if (parseInt(this.getAttribute("s")) == 1) return;
					            $("MyTeams").className = "UTHover";
					        };
					        $("MyTeams").onmouseout = function () {
					            if (parseInt(this.getAttribute("s")) == 1) return;
					            $("MyTeams").className = "UTNomal";
					        };
					        $("inputSearchId" + id + "Type" + t).onfocus = function ()//搜索好友
					        {
					            if (parseInt(this.getAttribute("b")) == 1) return;
					            /* var value = $F(this);
					            var IntID = setInterval(
					            function() {
					            if (value != $F("inputSearchId" + id + "Type" + t)) {
					            value = $F("inputSearchId" + id + "Type" + t);
					            _me.CMD.searchMyFriend(value);
					            }
					            }, 50);*/
					            this.setAttribute("b", "1");
					            this.onblur = function () {
					                this.setAttribute("b", "2");
					                // clearInterval(IntID);
					            };
					        };
					        if (/msie/i.test(navigator.userAgent)) {//IE下input文本变化
					            $("inputSearchId" + id + "Type" + t).onpropertychange = function ()//搜索好友
					            {
					                var value = $F("inputSearchId" + id + "Type" + t);
					                _me.CMD.searchMyFriend(value);

					            };
					        }
					        else {//非IE下INPUT文本变化
					            $("inputSearchId" + id + "Type" + t).addEventListener("input",
					                                                                 function () {
					                                                                     var value = $F("inputSearchId" + id + "Type" + t);
					                                                                     _me.CMD.searchMyFriend(value);

					                                                                 }, false);
					        }

					        $("imgSearchId" + id + "Type" + t).onclick = function ()//清除搜索图片
					        {
					            WebElement.Hid("imgSearchId" + id + "Type" + t);
					            WebElement.Value("inputSearchId" + id + "Type" + t);
					            _me.CMD.renderMyFriend();
					        };
					        $("IMConfig" + id + "Type" + t).onclick = function ()//选项窗口
					        {
					            _me.Common.showLink(300, 350, w.Top, w.Left < 305 ? w.Left + w.Width + 2 : w.Left - 302, 8, "选项", _me.MessageHandler.getProcessUrl("UserSettingUrl") + "?domain=" + gRunerDomain, gImgServer + "/Images/ToolOption.gif");
					        };
					        /*$("wMainProfileId"+id+"Type"+t).onclick = function()//个人信息窗口
					        {
					        _me.Common.showLink(350,300,w.Top,w.Left<355?w.Left+w.Width+2:w.Left-352,10,"编辑个人资料","WebIMPage/profile.asp?v="+Math.random(),gImgServer+"/Images/toolprofile.gif");
					        };
					        $("wMainShowHistoryId"+id+"Type"+t).onclick = function()//聊天记录窗口
					        {
					        _me.CMD.showMsgHistory();
					        };*/
					        /*$("wMainShowFocusId"+id+"Type"+t).onclick = function()//今日焦点窗口
					        {
					        _me.Common.showLink(400,400,w.Top,w.Left<405?w.Left+w.Width+2:w.Left-402,2,"今日焦点","WebIMPage/todayfocus.asp?v="+Math.random(),gImgServer+"/Images/toolshowfocus.gif");
					        };*/
					        $("wMainUserFaceId" + id + "Type" + t).onclick = function ()//显示自己的卡片
					        {
					            _me.CMD.showCard(_me.Profile.FormID, this);
					        }
					        $("wMainListTypeId" + id + "Type" + t).onclick = function (e)//添加好友按钮
					        {
					            var mData = ["按状态排序|_webIM.CMD.renderMyFriend(1,_webIM.Config.DisType,true)",
										  "按分组排序|_webIM.CMD.renderMyFriend(2,_webIM.Config.DisType,true)"];
					            mData[_me.Config.OrderType - 1] += "|" + gImgServer + "/Images/Selected.gif|" + gImgServer + "/Images/SelectedHover.gif";
					            var m = MenuManager.IsInstance().GetPopMenu(t, id);
					            m.Data = mData;
					            m.E = e;
					            m.HasIcon = true;
					            m.Width = 120;
					            m.Show();
					        };
					        /*$("wMainChangeContactId"+id+"Type"+t).onclick = function(e) //添加好友切换按钮
					        {
					        WebElement.Show($("wMainMainId"+id+"Type"+t));
					        WebElement.Hid($("wMainTeamGroupId"+id+"Type"+t));									
					        };	
					        $("wMainChangeTeamId"+id+"Type"+t).onclick = function(e) //添加群组切换按钮
					        {
					        WebElement.Hid($("wMainMainId"+id+"Type"+t));
					        WebElement.Show($("wMainTeamGroupId"+id+"Type"+t));										
					        };								
					        if(_me.Config.UserPower<2) //如果是管理员则显示管理按钮
					        {
					        var o = $("wMainManageId"+id+"Type"+t);
					        WebElement.Show(o);
					        o.onclick = _me.CMD.showManage;
					        }*/
					        if (!$("divEmotionList"))//缓存聊天表情
					        {
					            var oDiv = WebElement.New("div", "divEmotionList", "EmotionContainer");
					            oDiv.setAttribute("top", 0);
					            var Emotions = [];
					            Emotions = _me.Data.getEmotionInfo();
					            for (var i = 0; i < Emotions.length; i++) {
					                var Emotion = Emotions[i];
					                var oItem = WebElement.New("div", "EmotionItem", "EmotionItem", "<img f='" + Emotion[2] + "' id='" + "AEmotion" + "' src='" + gImgServer + "/Emotion/" + EmotionFolder + Emotion[0] + ".gif' title='" + Emotion[1] + " " + Emotion[2] + "'/>");
					                oItem.onmouseover = function () { this.className = "EmotionItem EmotionItemHover"; };
					                oItem.onmouseout = function () { this.className = "EmotionItem"; };
					                oItem.onclick = function () {
					                    var chattype = gFixForm.ffChat;
					                    var chatid = parseInt(window.cWindow.replace("winChat", ""));
					                    if (!chatid) {
					                        chattype = gFixForm.ffTeamChat;
					                        chatid = parseInt(window.cWindow.replace("winTeamChat", ""));
					                        //if(!chatid)return;	
					                    }
					                    if (!chatid) {
					                        chattype = gFixForm.ffMiniTeamChat;
					                        chatid = parseInt(window.cWindow.replace("winMiniTeamChat", ""));
					                        //if(!chatid)return;	
					                    }
					                    var EmotionContent = $T(this, "img")[0].getAttribute("f");
					                    //EmotionContent = _me.Common.replaceFaceFromStr(EmotionContent).replace(/{br}/img,"<br />");	
					                    WebElement.Append("wChatInputId" + chatid + "Type" + chattype, EmotionContent);
					                    WebElement.Hid("divEmotionList");
					                    WebElement.Focus("wChatInputId" + chatid + "Type" + chattype);
					                };
					                WebElement.Add(oDiv, oItem);
					            }
					            WebElement.Add(oDiv);
					        }
					        $("MainAddFriend" + id + "Type" + t).onclick = function ()//添加好友
					        {
					            _me.Common.showLink(450, 385, w.Top, w.Left < 355 ? w.Left + w.Width + 2 : w.Left - 452, gFixFormID.fiAddFriend, "添加好友", _me.MessageHandler.getProcessUrl("OnlineUserSearchUrl") + "?domain=" + gRunerDomain, gImgServer + "/Images/ToolAddFriend.gif");
					        };
					        /*$("wMainSearchTeam"+id+"Type"+t).onclick = function()//查找群链接
					        {
					        _me.CMD.searchTeam();
					        };	
					        $("wMainCreateTeam"+id+"Type"+t).onclick = function()//查找群链接
					        {
					        _me.CMD.showCreateTeam();
					        };*/
					        w.HideLoading();

					        _me.CMD.getOfflineMessage(10); //取得离线消息
					    }
					}, 500);
            };
            w.Show();
        },
        //注销操作
        logoutWebIM: function (ReLogin, Initiative) {
            _me.KeepLive.stopKeepLive(); //停止心跳
            _me.CMD.stopMsgInterval(); //暂停消息定时器
            Other.SetCookie("pk2_OA_UID", ""); //清除单点登录cookie 
            try {
                showLoginBefore();
            }
            catch (e) {
            }
            if (Initiative == 1) _me.CMD.changeUserStatus(gUserStatus.usOffline); //下线
            _me.UnInitialize();
            _me.CMD.destroyMainWindow();
            _me.Win.win.OnBeforeClose = null;
            _me.CMD.showFriendBar();
            if (ReLogin == 1) _me.CMD.intMiniLoginWindow();
            //_me.Win.win.Close();
        },
        //显示本人信息
        renderMyUserInfo: function () {
            var user = _me.Profile;
            var id = _me.Win.id;
            var t = _me.Win.type;
            _me.Win.win.setTitle(user.UserID);
            $("wMainUserFaceId" + id + "Type" + t).src = gStyleManager.getBigDefaultHeadPath(user.UserGender);
            // $("MyBigAvatar").src = gStyleManager.getBigDefaultHeadPath(user.UserGender);
            $("UserStatusImg" + id + "Type" + t).src = gStyleManager.getStyleResPath(gStyle) + "s" + user.UserStatus + ".gif";
            $("wMainUserNameId" + id + "Type" + t).innerHTML = "<span style=\"font-size:12px;font-weight:bold;\">" + user.UserNick.FixLength(8) + "</span> <span style=\"font-size:12px;\">(" + _me.Common.getUserStatusStr(user.UserStatus) + ")</span>";
            $("wMainUserSignId" + id + "Type" + t).innerHTML = user.UserSign;
        },
        //修改UserStatus
        changeUserStatus: function (n) {
            if (_me.Profile.UserStatus == n) return; //不用修改
            _me.Profile.UserStatus = n;
            _me.Profile.OnlineStatus = n;
            _me.CMD.renderMyUserInfo();
            _me.Data.changeUserStatus(n);
        },
        //修改登陆状态
        changeLoginStatus: function (n) {
            $("divStatusId6Type3").innerHTML = _me.Common.getUserStatusStr(n);
            $("divStatusId6Type3").setAttribute("us", n);
        },
        //销毁主窗体，并清除子窗体
        destroyMainWindow: function () {
            clearInterval(gWindowManager.WinListInteralID);
            for (var f = 0; f < gWindowManager.GetWindowsList().length; f++) {
                var w = gWindowManager.GetWindowsList()[f];
                WebElement.Del(gWindowManager.GetObjByWindow(w));
            }
            gWindowManager.GetWindowsList().clear();
        },
        //搜索好友
        searchMyFriend: function (k) {
            var id = _me.Win.id;
            var t = _me.Win.type;
            if (k == "") {
                WebElement.Hid("imgSearchId" + id + "Type" + t);
                _me.CMD.renderMyFriend();
            }
            else {
                var obj = $("wMainMainId" + id + "Type" + t);
                WebElement.Show("imgSearchId" + id + "Type" + t);
                WebElement.Value(obj); //清空
                var result = _me.Common.searchFriendList(k);
                if (result && result.length > 0) {
                    var title = WebElement.New("div", "", "", "<div class=\"wMainUserItemText\" style=\"padding-left:20px;width:100%;color:#aca899\">找到:</div>");
                    title.style.height = "20px";
                    var strHtml = new StringBuilder();
                    for (var i = 0; i < result.length; i++) {
                        var u = result[i];
                        strHtml.add(_me.Common.createUserItem(_me.Config.DisType, u));
                    }
                    var container = WebElement.New("div", "wMainUserContainerId" + id + "Type" + t + "Search", "wMainUserContainer", strHtml.toString());
                    WebElement.Add(obj, title, container);
                }
                else {
                    WebElement.Value(obj, "<div class=\"wMainUserItemText\" style=\"padding:10px 0 0 10px;width:100%;\">没有符合您的搜索条件的好友。</div>");
                }
            }
        },
        //填充好友列表
        renderMyFriend: function (orderType, disType, clearSearch) {
            if (!_me.Group) { return; }; //没有分组信息则无法处理

            var id = _me.Win.id;
            var t = _me.Win.type;
            var searchKey = $F("inputSearchId" + id + "Type" + t);
            if (searchKey != "") //搜索框不为空
            {
                if (clearSearch) //清除
                {
                    WebElement.Value("inputSearchId" + id + "Type" + t);
                    WebElement.Hid("imgSearchId" + id + "Type" + t);
                }
                else {
                    _me.CMD.searchMyFriend(searchKey);
                    return;
                }
            }
            if (orderType && disType) {
                if (orderType != _me.Config.OrderType || disType != _me.Config.DisType) {
                    _me.Config.OrderType = orderType;
                    _me.Config.DisType = disType;
                }
                else {
                    return; //不需要重新填充
                }
            }
            _me.Common.sortFriendList();

            var obj = $("wMainMainId" + id + "Type" + t);
            var nullGroupMsg = "<div class=\"wMainUserItemText\" style=\"padding-left:25px;width:100%;color:#aca899\">此组中没有好友</div>";
            WebElement.Value(obj);
            var itemHeight = _me.Config.DisType == 1 ? gSmallHeadItemHeight : 63;
            var groupUsers = {};

            if (_me.Config.OrderType == 1) //按状态排序
            {
                _me.Common.addUserHeader(obj, true, "在线", 0);
                _me.Common.addUserHeader(obj, true, "离线", 1);
                var num = [0, 0]; //记录联机和脱机的好友数量
                for (var i = 0; i < _me.Friend.length; i++) //将好友保存到临时对象
                {
                    var u = _me.Friend[i];
                    if (u.UserID != -1) {
                        var no = ((u.UserStatus != gUserStatus.usHide) && (u.UserStatus != gUserStatus.usOffline)) ? 0 : 1;
                        if (!(no in groupUsers)) groupUsers[no] = []; //为联机、脱机分配两个存储对象
                        groupUsers[no].add(u);  //分别将两种状态的好友放入到对象中
                        num[no]++;  //更新数量
                    }
                }
                for (var i = 0; i < num.length; i++) {
                    if (num[i] == 0) //如果此组中好友数量为0
                    {
                        WebElement.Value("wMainUserContainerId" + id + "Type" + t + "No" + i, nullGroupMsg);
                        $("wMainUserContainerId" + id + "Type" + t + "No" + i).style.height = "20px";
                        continue;
                    }
                    var strHtml = new StringBuilder();
                    for (var j = 0; j < groupUsers[i].length; j++) {
                        strHtml.add(_me.Common.createUserItem(_me.Config.DisType, groupUsers[i][j]));
                    }
                    WebElement.Value($("wMainUserContainerId" + id + "Type" + t + "No" + i), strHtml.toString());
                    $("wMainUserHeaderId" + id + "Type" + t + "No" + i).innerHTML += " (" + num[i] + ") ";  //显示好友数量
                    $("wMainUserContainerId" + id + "Type" + t + "No" + i).style.height = num[i] * itemHeight + "px";
                }
            }
            else  //按分组排序
            {
                _me.Data.fixGroupOrder();
                var num1 = {}, num2 = {};
                for (var i = 0; i < _me.GroupOrder.length; i++) {
                    var g = _me.Common.getGroupById(_me.GroupOrder[i]);
                    if (!g) continue;
                    _me.Common.addUserHeader(obj, g.Expand, g.Name, g.ID);
                    groupUsers[g.ID] = [];
                    num1[g.ID] = 0;
                    num2[g.ID] = 0;
                }

                for (var i = 0; i < _me.Friend.length; i++) {
                    var u = _me.Friend[i];
                    if ((u) || (u.UserID != -1)) {
                        if (!(groupUsers[u.GroupID]) && (groupUsers[u.GroupID] == undefined))
                            u.GroupID = gFixGroups.fgMyFriends; //如果分组不存在，则放入默认分组
                        groupUsers[u.GroupID].add(u);
                        num1[u.GroupID]++;
                        if ((u.UserStatus != gUserStatus.usHide) && (u.UserStatus != gUserStatus.usOffline)) num2[u.GroupID]++;
                    }
                }
                //alert(_me.Friend.length);
                for (var i = 0; i < _me.GroupOrder.length; i++) {
                    var gid = _me.GroupOrder[i];
                    if ((num1[gid] == 0) /*|| (_me.Friend[0].UserID == -1)*/) {

                        WebElement.Value("wMainUserContainerId" + id + "Type" + t + "No" + gid, nullGroupMsg);
                        $("wMainUserContainerId" + id + "Type" + t + "No" + gid).style.height = "20px";
                        continue;
                    }


                    var strHtml = new StringBuilder();
                    if (!groupUsers[gid]) continue;
                    for (var j = 0; j < groupUsers[gid].length; j++) {
                        strHtml.add(_me.Common.createUserItem(_me.Config.DisType, groupUsers[gid][j]));
                    }

                    WebElement.Value($("wMainUserContainerId" + id + "Type" + t + "No" + gid), strHtml.toString());
                    $("wMainUserHeaderId" + id + "Type" + t + "No" + gid).innerHTML += " (" + num2[gid] + "/" + num1[gid] + ") ";
                    $("wMainUserContainerId" + id + "Type" + t + "No" + gid).style.height = num1[gid] * itemHeight + "px";
                }
            }

            _me.CMD.showFriendBar();
        },
        //填充群组列表
        renderMyTeam: function (orderType, disType) {
            var id = _me.Win.id;
            var t = _me.Win.type;

            //_me.Common.sortFriendList();
            var obj = $("wMainTeamGroupId" + id + "Type" + t);
            var nullTeamMsg = "<div class=\"wMainUserItemText\" style=\"padding-left:25px;width:100%;color:#aca899\">您没有加入任何群</div>";
            WebElement.Value(obj);
            var itemHeight = _me.Config.DisType == 1 ? 20 : 63;
            var Teams = {};

            _me.Common.addTeamHeader(obj, "群/聊天室", id);
            if ((_me.Team.length == 1) && (_me.Team[0].TeamID == -1)) {
                WebElement.Value("wMainGroupContainerId" + id + "Type" + t + "No" + id, nullTeamMsg);
                $("wMainGroupContainerId" + id + "Type" + t + "No" + id).style.height = "20px";
                return;
            }
            var strHtml = new StringBuilder();
            for (var i = 0; i < _me.Team.length; i++) {
                var tid = _me.Team[i].TeamID;
                strHtml.add(_me.Common.createTeamItem(_me.Team[i]));
            }
            WebElement.Value($("wMainGroupContainerId" + id + "Type" + t + "No" + id), strHtml.toString());
            //$("wMainGroupContainerId"+id+"Type"+t+"No"+id).style.height = _me.Team.length*itemHeight+"px";			
            $("wMainGroupHeaderId" + id + "Type" + t + "No" + id).innerHTML += " ( " + _me.Team.length + " ) ";
        },
        //填充群组成员列表
        renderMyTeamUser: function (tid) {
            _me.Common.sortTeamUserList();
            var obj = $("wTeamUserHeaderId" + tid + "Type" + gFixForm.ffTeamChat);
            var itemHeight = _me.Config.DisType == 1 ? gSmallHeadItemHeight : 63;

            if (_me.TeamUser.length == 0) {
                var nullTeamMsg = "<div class=\"wMainUserItemText\" style=\"padding-left:25px;width:50px;color:#aca899\">此群中没有成员</div>";
                WebElement.Value("wTeamUserContainerId" + tid + "Type" + gFixForm.ffTeamChat, nullTeamMsg);
                $("wTeamUserContainerId" + tid + "Type" + gFixForm.ffTeamChat).style.height = "20px";
            }
            var strHtml = new StringBuilder();
            var teamCount = 0;
            for (var i = 0; i < _me.TeamUser.length; i++) {
                if (_me.TeamUser[i].TeamID == tid) {
                    strHtml.add(_me.Common.createTeamUserItem(_me.TeamUser[i]));
                    teamCount++;
                }
            }
            WebElement.Value($("wTeamUserContainerId" + tid + "Type" + gFixForm.ffTeamChat), strHtml.toString());
            //$("wTeamUserContainerId"+tid+"Type"+gFixForm.ffTeamChat).style.height = _me.TeamUser.length*itemHeight+"px";			
            $("wTeamUserHeaderId" + tid + "Type" + gFixForm.ffTeamChat).innerHTML += " ( " + teamCount + " ) ";
        },
        //发送消息
        sendMessage: function (uid) {
            var objContent = $("wChatInputId" + uid + "Type1");
            var msg = "";
            if ($F(objContent).trim() == "") {
                msg = "不能发送空消息!";
            }
            var users = [];
            users = _me.Friend;
            var userTo = _me.Common.getUserFromArr(uid, users);
            if (userTo && userTo.IsBlocked) {
                msg = "您不能向被屏蔽好友发送消息。";
                WebElement.Value(objContent);
            }
            if (msg != "") {
                _me.Common.showAlert(msg, "提示", function () {
                    objContent.focus();
                });
                return;
            }
            gFontFamily = objContent.style.fontFamily;
            gFontSize = objContent.style.fontSize.replace("px", "");
            gFontColor = objContent.style.color.colorHex();
            gFontFlag = $("wChatFontFlagSelect" + uid + "Type1")[$("wChatFontFlagSelect" + uid + "Type1").selectedIndex].value;
            var msg = new _me.Model.Msg(_me.Profile.UserID, _me.Profile.UserNick, uid, $F(objContent).enCodeHTML(), gMessagesType.mtChat, 2, _me.Profile.UserStatus, true);

            _webIM.CMD.ProcessMessage(uid, msg);
            _webIM.Data.sendMessage(msg);
            WebElement.Value(objContent);
            objContent.focus();
        },
        //发送群消息
        sendTeamMessage: function (tid) {
            var objContent = $("wChatInputId" + tid + "Type4");

            var msg = "";
            if ($F(objContent).trim() == "") {
                msg = "不能发送空消息!";
            }
            if (msg != "") {
                _me.Common.showAlert(msg, "提示", function () {
                    objContent.focus();
                });
                return;
            }
            var msg = new _me.Model.TeamMsg(_me.Profile.UserID, _me.Profile.UserNick, tid, $F(objContent).escapeHTML(), 9, 2, false); //类型9 群消息
            _webIM.CMD.ProcessMessage(tid, msg);
            _webIM.Data.sendTeamMessage(msg);
            WebElement.Value(objContent);
            objContent.focus();
        },
        //闪屏振动
        sendFlashMsg: function (uid, o) {
            var msg = "";
            if (o.getAttribute("b") == "1") {
                msg = "您不能频繁地发送闪屏振动。";
            }
            var users = [];
            users = _me.Friend;
            var userTo = _me.Common.getUserFromArr(uid, users);
            if (userTo && (userTo.UserStatus == gUserStatus.usOffline || userTo.UserStatus == gUserStatus.usHide)) {
                msg = "您的好友处于脱机状态，因此您不能发送闪屏振动。";
            }
            if (userTo && userTo.IsBlocked) {
                msg = "您不能向被屏蔽好友发送闪屏振动。";
            }
            if (msg != "") {
                _me.Common.showAlert(msg, "提示");
                return;
            }
            o.setAttribute("b", "1");
            var msg = new _me.Model.Msg(_me.Profile.UserID, _me.Profile.UserNick, uid, "", gMessagesType.mtFlash, 2, _me.Profile.UserStatus, false);
            _webIM.CMD.ProcessMessage(uid, msg);
            _webIM.Data.sendMessage(msg);
            setTimeout(function () {
                o.setAttribute("b", "0");
            }, 10000);
        },
        //显示表情列表
        showFaceList: function (id, t, e) {
            var el = $("divEmotionList");
            WebElement.Visible(el, !WebElement.IsHide(el));
            var wChat = gWindowManager.GetWindow(id, t);
            var os = el.style;
            var ev = window.event || e;
            var mousePos = WebElement.MousePos(ev);
            os.top = (mousePos.y - 285) + "px"; /*WebElement.scrollHeight() +(wChat.win.Top + wChat.win.Height - 240 - parseInt($("wChatInputId" + id + "Type" + t).style.height)) + "px"; */
            //alert(mousePos.y + "|" + WebElement.scrollHeight());
            os.left = mousePos.x + 10 + "px"; /*(wChat.win.Left + 9) + "px"; */
            os.width = "457px";
            os.height = "285px";
            el.setAttribute("top", parseInt(os.top)/* - WebElement.scrollHeight()*/);
            document.onmousedown = function (e) {
                var _e = window.event || e;
                var obj = _e.srcElement ? _e.srcElement : _e.target;
                if ((obj.id != "AEmotion") && (obj.id != "EmotionItem")) {
                    WebElement.Hid("divEmotionList");
                    document.onmousedown = null;
                }
            }
        },
        refreshChatWindowHint: function (uid) {
            var winChat = gWindowManager.GetWindow(uid, 1);
            if (winChat)//窗口已存在
            {
                var t = winChat.type;
                var u = _me.Common.getUserFromArr(uid, _me.Friend);
                if ((u.UserStatus != gUserStatus.usHide) && (u.UserStatus != gUserStatus.usOffline)) {
                    if ($("wChatHintBoder" + uid + "Type" + t).style.display == "block") {
                        $("ChatHintClose" + uid + "Type" + t).onclick();
                    }
                }
                else {
                    WebElement.Show($("wChatHintBoder" + uid + "Type" + t));
                    var ChatViewHeight = parseInt($("wChatViewId" + uid + "Type" + t).style.height);
                    ChatViewHeight -= 21;
                    $("wChatViewId" + uid + "Type" + t).style.height = ChatViewHeight + "px";
                    $("wChatViewId" + uid + "Type" + t).scrollTop = $("wChatViewId" + uid + "Type" + t).scrollHeight;
                }
            }
        },
        //通过聊天室id直接打开聊天窗口
        directTeamChat: function (tid) {
            _me.CMD.enterTeam(tid, $U("p2"));
            _me.Data.getMyTeamList(
			function (o) {
			    if (!o) return;
			    if (!$T(o.responseXML, "list")) return;
			    _me.CMD.renderMyTeam();
			    _me.CMD.openTeamChatWindow(tid);
			});
        },
        //打开和tid的Mini聊天窗口
        openMiniTeamChatWindow: function () {                 
            var w = new Chat();
            w.Show();
        },
        //打开和tid的聊天窗口
        openTeamChatWindow: function (tid, isfocus) {
            var winChat = gWindowManager.GetWindow(tid, 4);
            var wWin = _me.Win;
            if (winChat)//窗口已存在
            {
                if (isfocus) {
                    if (winChat.isMin) winChat.win.Minimize(); //如果窗口处于最小化状态则还原
                    winChat.win.Focus(); //激活
                }
            }
            else//窗口不存在则创建
            {
                var team = _me.Common.getTeamFromArr(tid, _me.Team);
                if (!team) return;
                var w = new WebForm();
                w.Title = team.TeamName;
                w.Icon = gImgServer + "/Images/Team.gif";
                w.TitleBGUrl = gImgServer + "/Images/TeamTitleBG.gif";
                w.TitleHeight = 35;
                w.Type = gFixForm.ffTeamChat;
                w.FormID = tid;
                w.DefaultMRight = true;
                w.Height = 600;
                w.Width = 850;
                w.MinWidth = 400;
                w.MinHeight = 600;
                var lastTeamChatWin = gWindowManager.GetLastWindow(4);
                if (!lastTeamChatWin) {
                    w.Left = wWin.win.Left < 425 ? wWin.win.Left + wWin.win.Width + 2 : wWin.win.Left - 422;
                    w.Top = wWin.win.Top;
                }
                else {
                    w.Left = lastTeamChatWin.win.Left + 15;
                    w.Top = lastTeamChatWin.win.Top + 15;
                }
                w.OnBeforeClose = function () {
                    $("wTeamChatViewId" + tid + "Type4").innerHTML = "";
                    return true;
                }
                w.OnClose = function () {
                    _me.CMD.stopMsgInterval();
                };
                w.OnPaint = function (w, h, id, t) {
                    /*$("wTeamCenterTopId"+id+"Type"+t).style.height = (h-200)+"px";
                    $("wChatSideId"+id+"Type"+t).style.height = (h-30)+"px";
                    $("wChatSideBarId"+id+"Type"+t).style.height = (h-32)+"px";
                    _me.Config.TeamChatSide==1?WebElement.Show($("wChatSideId"+id+"Type"+t)):WebElement.Hid($("wChatSideId"+id+"Type"+t));
                    $("wChatMainId"+id+"Type"+t).style.width = _me.Config.TeamChatSide==1?(w-145)+"px":(w-20)+"px";
                    $("wChatResizeBarId"+id+"Type"+t).style.width = _me.Config.TeamChatSide==1?(w-145)+"px":(w-20)+"px";
                    $("wChatInputId"+id+"Type"+t).style.width = _me.Config.TeamChatSide==1?(w-150)+"px":(w-25)+"px";
                    $("wChatSignHolderId"+id+"Type"+t).style.width = (w-60)+"px";
                    var viewHeight = (h-parseInt($("wChatInputId"+id+"Type"+t).style.height)-141);
                    if(viewHeight<10)
                    {
                    $("wChatInputId"+id+"Type"+t).style.height = (h-parseInt($("wChatViewId"+id+"Type"+t).style.height)-141)+"px";
                    }
                    else
                    {
                    $("wChatViewId"+id+"Type"+t).style.height = viewHeight+"px";
                    }*/
                };
                w.OnLoaded = function (id, t) {
                    var obj = $("wTeamUserListId" + id + "Type" + t);
                    _me.Common.addTeamUserHeader(obj, "用户列表", id);
                    w.ShowLoading();
                    _me.Data.getMyTeamUserList(tid, function () {
                        _me.CMD.renderMyTeamUser(tid);
                        _me.CMD.getMsgInterval(); //开始轮询群消息
                        w.HideLoading();
                    });

                    //$T("wChatButtonBlockId"+id+"Type"+t,"img")[0].src = team.IsBlocked?gImgServer+"/Images/chatbuttoncancelblock.gif":gImgServer+"/Images/chatbuttonblock.gif";
                    //$T("wChatButtonBlockId"+id+"Type"+t,"img")[0].title = team.IsBlocked?"取消阻止此群消息":"阻止此群消息";
                    //$("wChatButtonBlockId"+id+"Type"+t).setAttribute("b",team.IsBlocked?"1":"2");
                    //$("wChatSignId"+id+"Type"+t).innerHTML = team.TeamDescript+" &lt;"+team.TeamDescript+">";
                    /*$("wChatButtonBlockId"+tid+"Type"+t).onclick = function()
                    {
                    var isblock = 3-parseInt(this.getAttribute("b"));
                    _me.CMD.blockFriend(tid,isblock);
                    };
                    $("wChatButtonHistoryId"+tid+"Type"+t).onclick = function()
                    {
                    _me.CMD.showTeamMsgHistory(tid);
                    };*/
                    $("wTeamChatResizeId" + id + "Type" + t).onmousedown = function () {
                        var resizeBar = $("wTeamChatResizeBarId" + id + "Type" + t);
                        var rs = resizeBar.style;
                        rs.top = (40 + parseInt($("wTeamChatViewId" + id + "Type" + t).style.height)) + "px";
                        WebElement.Show(resizeBar);
                        document.onmousemove = function (e) {
                            var _top = parseInt(rs.top) + (Evt.Top(e) - (parseInt(rs.top) + w.Top + 5));
                            _top = _top < 380 ? 380 : _top;
                            //_top = _top>w.Height-240?w.Height-240:_top;
                            rs.top = _top + "px";
                        };
                        document.onmouseup = function (e) {

                            $("wTeamChatViewId" + id + "Type" + t).style.height = (parseInt(rs.top) - 40) + "px";
                            $("wChatInputId" + id + "Type" + t).style.height = (w.Height - parseInt(rs.top) - 75) + "px";
                            document.onmousemove = document.onmouseup = null;
                            WebElement.Hid(resizeBar);
                        };
                    };
                    $("wChatInputId" + id + "Type" + t).onkeydown = function (e) {
                        var e = e || event;
                        if (e.keyCode == 13)//回车
                        {
                            if (_me.Config.MsgSendKey == 1)//enter发送
                            {
                                if (!e.ctrlKey && !e.shiftKey)//没有同时按下了ctrl则发送消息
                                {
                                    _me.CMD.sendTeamMessage(id);
                                    return false;
                                }
                            }
                            else//ctrl+enter发送
                            {
                                if (e.ctrlKey)//同时按下了ctrl则发送消息
                                {
                                    _me.CMD.sendTeamMessage(id);
                                    return false;
                                }
                            }
                        }
                        if (_me.Config.MsgSendKey == 1 && (!e.ctrlKey && e.keyCode == 13)) {
                        }
                        else if (_me.Config.MsgSendKey == 2 && (e.ctrlKey && e.keyCode == 13)) {
                            _me.CMD.sendTeamMessage(id);
                            return false;
                        }
                    };
                    $("wTeamChatBtnSendId" + id + "Type" + t).onclick = function () {
                        _me.CMD.sendTeamMessage(id);
                    };
                    $("wChatFaceButtonFaceId" + tid + "Type" + t).onclick = function (e) {
                        _me.CMD.showFaceList(id, t, e);
                    };
                    $("wChatBtnOptionId" + id + "Type" + t).onclick = function (e) {
                        var mData = ["哦|WebElement.Value('wChatInputId" + id + "Type" + t + "','哦');_webIM.CMD.sendTeamMessage(" + id + ")",
									"好吧|WebElement.Value('wChatInputId" + id + "Type" + t + "','好吧');_webIM.CMD.sendTeamMessage(" + id + ")",
									"我得下了，拜拜!|WebElement.Value('wChatInputId" + id + "Type" + t + "','我得下了，拜拜！');_webIM.CMD.sendTeamMessage(" + id + ")",
									"",
									"按Enter 键发送消息|_webIM.Config.MsgSendKey=1",
									"按Ctrl+Enter 键发送消息|_webIM.Config.MsgSendKey=2"];
                        mData[_me.Config.MsgSendKey + 3] += "|" + gImgServer + "/Images/Selected.gif|" + gImgServer + "/Images/SelectedHover.gif";
                        var m = MenuManager.IsInstance().GetPopMenu(t, id);
                        m.Data = mData;
                        m.E = e;
                        m.HasIcon = true;
                        m.ShowLeftBG = !!1;
                        m.Width = 190;
                        m.Show();
                    };
                    $("wChatInputId" + id + "Type" + t).focus();

                    MessageQueue.IsInstance().ClearMessageByType(9); //清理掉群消息队列
                    //_me.CMD.showTeamQueueTextContent(tid); //当窗体打开后，处理掉消息队列						
                };
                w.Show();
                w.Focus();
            }
        },
        //打开和uid的聊天窗口
        openChatWindow: function (uid, isfocus, cb) {
            var winChat = gWindowManager.GetWindow(uid, 1);
            var wWin = _me.Win;
            if (winChat)//窗口已存在
            {
                if (isfocus) {
                    if (winChat.isMin) winChat.win.Minimize(); //如果窗口处于最小化状态则还原
                    winChat.win.Focus(); //激活	
                }
                if (cb) cb();
            }
            else//窗口不存在则创建
            {
                var u = _me.Common.getUserFromArr(uid, _me.Friend);
                if (!u) return;
                var w = new WebForm();
                w.Title = u.CustomName || u.UserNick;
                w.TitleXOffset = 15;
                w.Type = gFixForm.ffChat;
                w.MinimizeIcon = "../Images/minIcon0.gif";
                w.FormID = uid;
                w.DefaultMRight = true;
                w.Height = 400;
                w.Width = 420;
                w.MinWidth = 330;
                w.MinHeight = 350;
                var lastChatWin = gWindowManager.GetLastWindow(1);
                if (!lastChatWin) {
                    w.Left = wWin.win.Left < 425 ? wWin.win.Left + wWin.win.Width + 2 : wWin.win.Left - 422;
                    w.Top = wWin.win.Top;
                }
                else {
                    w.Left = lastChatWin.win.Left + 15;
                    w.Top = lastChatWin.win.Top + 15;
                }
                w.OnBeforeClose = function () {
                    $("wChatViewId" + uid + "Type1").innerHTML = "";
                    return true;
                }
                w.OnPaint = function (w, h, id, t) {
                    $("wChatMainId" + id + "Type" + t).style.height = (h - 33) + "px";
                    $("wChatSideId" + id + "Type" + t).style.height = (h - 30) + "px";
                    $("wChatSideBarId" + id + "Type" + t).style.height = (h - 32) + "px";
                    _me.Config.ChatSide == 1 ? WebElement.Show($("wChatSideId" + id + "Type" + t)) : WebElement.Hid($("wChatSideId" + id + "Type" + t));
                    $("wChatMainId" + id + "Type" + t).style.width = _me.Config.ChatSide == 1 ? (w - 145) + "px" : (w - 20) + "px";
                    $("wChatResizeBarId" + id + "Type" + t).style.width = _me.Config.ChatSide == 1 ? (w - 145) + "px" : (w - 20) + "px";
                    $("wChatInputId" + id + "Type" + t).style.width = _me.Config.ChatSide == 1 ? (w - 150) + "px" : (w - 25) + "px";
                    $("wChatInputId" + id + "Type" + t).style.color = "#000000";
                    $("wChatSignHolderId" + id + "Type" + t).style.width = (w - 60) + "px";
                    $("ChatUserFace" + id + "Type" + t).src = gStyleManager.getChatUserHeadPath(_me.Common.getUserGenderFromUserID(uid));
                    var viewHeight = (h - parseInt($("wChatInputId" + id + "Type" + t).style.height) - 110);
                    switch (parseInt(u.UserStatus)) {
                        case gUserStatus.usOffline: { WebElement.Show($("wChatHintBoder" + id + "Type" + t)); viewHeight -= 18; break; }; //用户离线时，减去显示提示的高度
                    }
                    if (viewHeight < 10) {
                        $("wChatInputId" + id + "Type" + t).style.height = (h - parseInt($("wChatViewId" + id + "Type" + t).style.height) - 110) + "px";
                    }
                    else {
                        $("wChatViewId" + id + "Type" + t).style.height = ($("wChatViewFont" + id + "Type" + t).style.display == "block") ? (viewHeight - 21) + "px" : viewHeight + "px"
                    }
                };
                w.OnLoaded = function (id, t) {
                    //$T("wChatButtonBlockId"+id+"Type"+t,"img")[0].src = u.IsBlocked?gImgServer+"/Images/chatbuttoncancelblock.gif":gImgServer+"/Images/chatbuttonblock.gif";
                    //$T("wChatButtonBlockId"+id+"Type"+t,"img")[0].title = u.IsBlocked?"取消阻止此好友":"阻止此好友";
                    //$("wChatButtonBlockId"+id+"Type"+t).setAttribute("b",u.IsBlocked?"1":"2");                

                    //填充字体
                    var fontTypeSelectStr = '<select id="wChatFontTypeSelect' + id + 'Type' + t + '">';
                    var fontTypes = _me.Data.getFontTypeInfo();
                    for (var i = 0; i < fontTypes.length; i++) {
                        fontTypeSelectStr += '<option value="' + fontTypes[i] + '">' + fontTypes[i] + '</option>';
                    }
                    fontTypeSelectStr += '</select>';
                    $("wChatViewFont" + id + "Type" + t).innerHTML += fontTypeSelectStr;

                    //填充字体大小
                    var fontSizeSelectStr = '<select id="wChatFontSizeSelect' + id + 'Type' + t + '">';
                    var fontSizes = _me.Data.getFontSizeInfo();
                    for (var i = 0; i < fontSizes.length; i++) {
                        fontSizeSelectStr += '<option value="' + fontSizes[i] + '">' + fontSizes[i] + '</option>';
                    }
                    fontSizeSelectStr += '</select>';
                    $("wChatViewFont" + id + "Type" + t).innerHTML += fontSizeSelectStr;

                    //填充字体颜色
                    var fontFontColorStr = '<select id="wChatFontColorSelect' + id + 'Type' + t + '">';
                    var fontColors = _me.Data.getFontColorInfo();
                    for (var i = 0; i < fontColors.length; i++) {
                        fontFontColorStr += '<option value="' + fontColors[i][0] + '" style="color:' + fontColors[i][0] + '">' + fontColors[i][1] + '</option>';
                    }
                    fontFontColorStr += '</select>';
                    $("wChatViewFont" + id + "Type" + t).innerHTML += fontFontColorStr;

                    //填充字形
                    var fontFlagSelectStr = '<select id="wChatFontFlagSelect' + id + 'Type' + t + '">';
                    var fontFlags = _me.Data.getFontFlagInfo();
                    for (var i = 0; i < fontFlags.length; i++) {
                        fontFlagSelectStr += '<option value="' + fontFlags[i][0] + '">' + fontFlags[i][1] + '</option>';
                    }
                    fontFlagSelectStr += '</select>';
                    $("wChatViewFont" + id + "Type" + t).innerHTML += fontFlagSelectStr;

                    var wchatinput = $("wChatInputId" + id + "Type" + t);
                    /*字体事件控制--开始*/
                    $("wChatFontTypeSelect" + id + "Type" + t).onchange = function () {
                        wchatinput.style.fontFamily = this[this.selectedIndex].value;
                        wchatinput.focus();
                    }
                    $("wChatFontSizeSelect" + id + "Type" + t).onchange = function () {
                        wchatinput.style.fontSize = this[this.selectedIndex].value + "px";
                        wchatinput.focus();
                    }
                    $("wChatFontFlagSelect" + id + "Type" + t).onchange = function () {
                        _me.Common.showFontFlag("wChatInputId" + id + "Type" + t, this[this.selectedIndex].value);
                        wchatinput.focus();
                    }
                    $("wChatFontColorSelect" + id + "Type" + t).onchange = function () {
                        wchatinput.style.color = this[this.selectedIndex].value;
                        wchatinput.focus();
                    }
                    /*字体事件控制--结束*/


                    $("wChatInputId" + id + "Type" + t).focus();
                    $("wChatSignId" + id + "Type" + t).innerHTML = u.UserSign + " &lt;" + u.UserEmail + ">";
                    var sideBtn = $T("wChatSideBarId" + id + "Type" + t, "img")[0];
                    sideBtn.src = _me.Config.ChatSide == 1 ? gImgServer + "/Images/chatsidebutton.gif" : gImgServer + "/Images/ChatSideButton1.gif";
                    sideBtn.title = _me.Config.ChatSide == 1 ? "隐藏侧边" : "显示侧边";
                    sideBtn.onclick = function () {
                        return;
                        _me.Config.ChatSide = _me.Config.ChatSide == 1 ? 2 : 1;
                        w.OnPaint(w.Width, w.Height - 28, w.FormID, w.Type);
                        this.src = _me.Config.ChatSide == 1 ? "Images/chatsidebutton.gif" : "Images/ChatSideButton1.gif";
                        this.title = _me.Config.ChatSide == 1 ? "隐藏侧边" : "显示侧边";
                    };
                    /*$("wChatButtonBlockId"+id+"Type"+t).onclick = function()
                    {
                    var isblock = 3-parseInt(this.getAttribute("b"));
                    _me.CMD.blockFriend(id,isblock);
                    };
                    $("wChatButtonHistoryId"+id+"Type"+t).onclick = function()
                    {
                    _me.CMD.showMsgHistory(id);
                    };*/
                    $("wChatResizeId" + id + "Type" + t).onmousedown = function () {
                        var resizeBar = $("wChatResizeBarId" + id + "Type" + t);
                        var rs = resizeBar.style;
                        if ($("wChatViewFont" + id + "Type" + t).style.display == "block") {
                            rs.top = (56 + 21 + parseInt($("wChatViewId" + id + "Type" + t).style.height)) + "px";
                        }
                        else {
                            rs.top = (56 + parseInt($("wChatViewId" + id + "Type" + t).style.height)) + "px";
                        }
                        WebElement.Show(resizeBar);
                        document.onmousemove = function (e) {
                            var _top = parseInt(rs.top) + (Evt.Top(e) - (parseInt(rs.top) + w.Top + 5));
                            _top = _top < 140 ? 140 : _top;
                            if ($("wChatViewFont" + id + "Type" + t).style.display == "block") {
                                _top = _top > w.Height - 121 ? w.Height - 121 : _top;
                            }
                            else {
                                _top = _top > w.Height - 100 ? w.Height - 100 : _top;
                            }
                            rs.top = _top + "px";
                        };
                        document.onmouseup = function (e) {
                            if ($("wChatViewFont" + id + "Type" + t).style.display == "block") {
                                $("wChatViewId" + id + "Type" + t).style.height = (parseInt(rs.top) - 21 - 56) + "px";
                            }
                            else {
                                $("wChatViewId" + id + "Type" + t).style.height = (parseInt(rs.top) - 56) + "px";
                            }
                            $("wChatInputId" + id + "Type" + t).style.height = (w.Height - parseInt(rs.top) - 82) + "px";
                            document.onmousemove = document.onmouseup = null;
                            WebElement.Hid(resizeBar);
                        };
                    };
                    $("winChat" + id).onfocus = function () {
                    };
                    $("wChatInputId" + id + "Type" + t).onkeydown = function (e) {
                        var e = e || event;
                        if (e.keyCode == 13)//回车
                        {
                            if (_me.Config.MsgSendKey == 1)//enter发送
                            {
                                if (!e.ctrlKey && !e.shiftKey)//没有同时按下了ctrl则发送消息
                                {
                                    _me.CMD.sendMessage(id);
                                    return false;
                                }
                            }
                            else//ctrl+enter发送
                            {
                                if (e.ctrlKey)//同时按下了ctrl则发送消息
                                {
                                    _me.CMD.sendMessage(id);
                                    return false;
                                }
                            }
                        }
                        if (_me.Config.MsgSendKey == 1 && (!e.ctrlKey && e.keyCode == 13)) {
                        }
                        else if (_me.Config.MsgSendKey == 2 && (e.ctrlKey && e.keyCode == 13)) {
                            _me.CMD.sendMessage(id);
                            return false;
                        }
                    };
                    $("wChatBtnSendId" + id + "Type" + t).onclick = function () {
                        _me.CMD.sendMessage(id);
                    };
                    $("wChatFaceButtonFlashId" + id + "Type" + t).onclick = function () {
                        _me.CMD.sendFlashMsg(id, this);
                    };
                    $("ChatHintClose" + id + "Type" + t).onclick = function () {
                        WebElement.Hid($("wChatHintBoder" + id + "Type" + t));
                        var ChatViewHeight = parseInt($("wChatViewId" + id + "Type" + t).style.height);
                        ChatViewHeight += 21;
                        $("wChatViewId" + id + "Type" + t).style.height = ChatViewHeight + "px";
                        $("wChatViewId" + id + "Type" + t).scrollTop = $("wChatViewId" + id + "Type" + t).scrollHeight;
                    };
                    $("wChatFaceButtonFaceId" + id + "Type" + t).onclick = function (e) {
                        _me.CMD.showFaceList(id, t, e);
                    };
                    $("wChatFontId" + id + "Type" + t).onclick = function (e) {//显示字体控制栏，控制上方DIV尺寸
                        var status = $("wChatViewFont" + id + "Type" + t).style.display;
                        var obj = $("wChatViewId" + id + "Type" + t);
                        $("wChatViewFont" + id + "Type" + t).style.display = status == "none" ? "block" : "none";
                        obj.style.height = status == "none" ? (parseInt(obj.style.height) - 21) + "px" : (parseInt(obj.style.height) + 21) + "px";
                        obj.scrollTop = obj.scrollHeight;
                    };
                    $("wChatBtnOptionId" + id + "Type" + t).onclick = function (e) {
                        var mData = ["哦|WebElement.Value('wChatInputId" + id + "Type" + t + "','哦');_webIM.CMD.sendMessage(" + id + ")",
								"好吧|WebElement.Value('wChatInputId" + id + "Type" + t + "','好吧');_webIM.CMD.sendMessage(" + id + ")",
								"我得下了，拜拜!|WebElement.Value('wChatInputId" + id + "Type" + t + "','我得下了，拜拜！');_webIM.CMD.sendMessage(" + id + ")",
								"",
								"按Enter 键发送消息|_webIM.Config.MsgSendKey=1",
								"按Ctrl+Enter 键发送消息|_webIM.Config.MsgSendKey=2"];
                        mData[_me.Config.MsgSendKey + 3] += "|" + gImgServer + "/Images/Selected.gif|" + gImgServer + "/Images/SelectedHover.gif";
                        var m = MenuManager.IsInstance().GetPopMenu(t, id);
                        m.Data = mData;
                        m.E = e;
                        m.HasIcon = true;
                        m.ShowLeftBG = !!1;
                        m.Width = 190;
                        m.Show();
                    };
                    $("wChatFaceFriendId" + id + "Type" + t).src = "UserAvatar/" + u.UserAvatar;
                    $("wChatFaceMeId" + id + "Type" + t).src = "UserAvatar/" + _me.Profile.UserAvatar;

                    _me.CMD.showUserQueueTextContent(uid); //当窗体打开后，处理掉消息队列
                    if (cb) cb();
                };
                w.Show();
                w.Focus();
            }
        },
        //显示指定用户发送过来的消息
        showUserTextContent: function (uid, msg) {
            var users = [];
            users = _me.Friend;
            users.add(_me.Profile);
            var userFrom = _me.Common.getUserFromArr(msg.From, users);
            var userTo = _me.Common.getUserFromArr(msg.To, users);
            users.remove(_me.Profile);

            _me.CMD.openChatWindow(uid, false); //打开与uid聊天窗口

            var _interID = setInterval(function () {
                var objChat = $("wChatViewId" + uid + "Type1");
                if (objChat) {
                    clearInterval(_interID);
                    var winChat = gWindowManager.GetWindow(uid, 1);
                    winChat.win.Flash(); //提示有新消息

                    var msgTitle = userFrom.UserNick;
                    if (_me.Config.MsgShowTime == 1) {
                        var temptime;
                        if (!msg.AddTime) {
                            var d = new Date();
                            temptime = d.getHours().toString().padLeft("0", 2) + ":" + d.getMinutes().toString().padLeft("0", 2) + ":" + d.getSeconds().toString().padLeft("0", 2);
                        }
                        msgTitle += (" (" + (!msg.AddTime ? temptime : msg.AddTime) + ")");
                        //var msgTime = !msg.AddTime?new Date():new Date(Date.parse(msg.AddTime.replace(/-/g,"/")));
                        //msgTitle += " (" + msgTime.getHours().toString().padLeft("0", 2) + ":" + msgTime.getMinutes().toString().padLeft("0", 2) + ")";
                    }
                    var titleStyle = msg.From == _me.Profile.UserID ? "wChatMyMsgTitle" : "wChatMsgTitle";
                    WebElement.Add(objChat, WebElement.New("div", null, titleStyle, msgTitle));
                    //alert(msg.Content);
                    var WEmsgcontent = WebElement.New("div", null, "wChatMsgContent", _me.Common.replaceFaceFromStr(msg.Content).replace(/\n/img, "<br />"));
                    /*处理字体样式--开始*/
                    WEmsgcontent.style.fontFamily = gFontFamily;
                    WEmsgcontent.style.fontSize = gFontSize + "px";
                    WEmsgcontent.style.color = (navigator.userAgent.indexOf("Firefox") > 0) ? gFontColor : gFontColor.replace("#", "");
                    _me.Common.showFontFlag(WEmsgcontent, gFontFlag);
                    /*处理字体样式--结束*/
                    WebElement.Add(objChat, WEmsgcontent);
                    if (msg.From != _me.Profile.UserID) _me.Common.playSound("newmessage");
                    objChat.scrollTop = objChat.scrollHeight;
                    MessageQueue.IsInstance().ClearLastMessage(msg);
                }
            }, 100);
        },
        //显示指定群发送过来的消息
        showTeamTextContent: function (tid, msg) {
            _me.CMD.openTeamChatWindow(tid, false); //打开与tid群的聊天窗口				
            var _interID = setInterval(function () {
                var objChat = $("wTeamChatViewId" + tid + "Type4");
                if (objChat) {
                    if (_me.TeamUser) {
                        clearInterval(_interID);
                        var winChat = gWindowManager.GetWindow(tid, 4);
                        winChat.win.Flash(); //提示有新消息

                        var msgTitle = msg.FromNick;
                        if (_me.Config.MsgShowTime == 1) {
                            var msgTime = !msg.AddTime ? new Date() : new Date(Date.parse(msg.AddTime.replace(/-/g, "/")));
                            msgTitle += " (" + msgTime.getHours().toString().padLeft("0", 2) + ":" + msgTime.getMinutes().toString().padLeft("0", 2) + ")";
                        }
                        var titleStyle = msg.From == _me.Profile.UserID ? "wChatMyMsgTitle" : "wChatMsgTitle";
                        WebElement.Add(objChat, WebElement.New("div", null, titleStyle, msgTitle));
                        WebElement.Add(objChat, WebElement.New("div", null, "wChatMsgContent", _me.Common.replaceFaceFromStr(msg.Content).replace(/\n/img, "<br />")));
                        if (msg.From != _me.Profile.UserID) _me.Common.playSound("newmessage");

                        objChat.scrollTop = objChat.scrollHeight;
                    }
                }
            }, 100);
        },
        //将消息队列中的用户消息显示出来
        showUserQueueTextContent: function (uid) {
            var _msg = null;
            var _msgList = MessageQueue.IsInstance().GetMessageList();
            for (var i = 0; i < _msgList.length; i++) {
                if ((_msgList[i].Type == gMessagesType.mtChat) && (_msgList[i].From) == uid && _msgList[i].Processed == false) //普通用户消息
                {
                    _msg = MessageQueue.IsInstance().GetMessageList()[i];
                    _me.CMD.showUserTextContent(_msg.From, _msg);
                    _msgList[i].Processed = true;
                }
            }
            MessageQueue.IsInstance().ClearProcessedMessage();
            HeadManager.IsInstance().GetUserHeadByUserID(uid).StopFlash();
        },
        //将消息队列中的群组消息显示出来
        showTeamQueueTextContent: function (tid) {
            var _msg = null;
            var _msgList = MessageQueue.IsInstance().GetMessageList();
            for (var i = 0; i < _msgList.length; i++) {
                if (_msgList[i].Type == 9) //群组消息
                {
                    _msg = MessageQueue.IsInstance().GetMessageList()[i];
                    _me.CMD.showTeamTextContent(_msg.To, _msg);
                    _msgList[i].Processed = true;
                }
            }
            MessageQueue.IsInstance().ClearProcessedMessage();
            HeadManager.IsInstance().GetTeamHeadByTeamID(tid).StopFlash();
        },
        //消息处理
        ProcessMessage: function (uid, msg) {
            var users = [];
            users = _me.Friend;
            users.add(_me.Profile);  //将自己的信息也放进用户列表，以下处理过程，也包括有对发送本方本身的操作
            var userFrom = _me.Common.getUserFromArr(msg.From, users);
            var userTo = _me.Common.getUserFromArr(msg.To, users);
            users.remove(_me.Profile); //操作完毕，移除自己           

            switch (parseInt(msg.Type)) {
                case gMessagesType.mtChat: //好友文本消息                    
                    if (gWindowManager.GetWindow(uid, 1))  //如果聊天窗口已经是打开的，则直接显示
                    {
                        _me.CMD.showUserTextContent(uid, msg);  //直接弹出并显示
                    }
                    else {
                        if (_me.Config.AutoShowMsg == 1)  //自动弹出消息
                            _me.CMD.showUserTextContent(uid, msg)  //直接弹出并显示
                        else {
                            //如果用户列表中没有此用户，则说明是陌生人发来的消息，为其在陌生人分组中创建一个头像，然后再闪动
                            if (!(HeadManager.IsInstance().GetUserHeadByUserID(msg.From))) {
                                _me.Common.createStranger(msg.From, msg.Status);
                                _me.CMD.renderMyFriend();
                            }
                            HeadManager.IsInstance().GetUserHeadByUserID(msg.From).StartFlash();
                        }
                    }
                    if (msg.From != _me.Profile.UserID) {
                        _me.Common.cancelCheckNewsShow();
                        _me.Common.checkNewMsgShow(_me.Common.getUserFromArr(msg.From, _me.Friend).UserNick);
                    }
                    //分组操作(自动打开)
                    _me.CMD.openGroup(_me.Data.getUserGroupID(msg.From));
                    break;
                case gMessagesType.mtFlash:  //闪屏
                    if (!(HeadManager.IsInstance().GetUserHeadByUserID(msg.From))) {
                        _me.Common.createStranger(msg.From, msg.Status);
                        _me.CMD.renderMyFriend();
                    }
                    userFrom = _me.Common.getUserFromArr(msg.From, users); //有可能是陌生人发来的消息，创建完头像后，要重新获取一下userFrom
                    _me.CMD.openChatWindow(uid, false, function () //打开与uid聊天窗口
                    {
                        var _interID = setInterval(function () {
                            var objChat = $("wChatViewId" + uid + "Type1");
                            if (objChat) {
                                clearInterval(_interID);
                                var oChild = objChat.childNodes;
                                if (oChild.length == 0 || (oChild.length > 0 && oChild[oChild.length - 1].className != "wChatMsgSplit")) WebElement.Add(objChat, WebElement.New("div", null, "wChatMsgSplit"));
                                var temptime;
                                if (!msg.AddTime) {
                                    var d = new Date();
                                    temptime = d.getHours().toString().padLeft("0", 2) + ":" + d.getMinutes().toString().padLeft("0", 2) + ":" + d.getSeconds().toString().padLeft("0", 2);
                                }
                                WebElement.Add(objChat, WebElement.New("div", null, "wChatMsgSpecial", " (" + (!msg.AddTime ? temptime : msg.AddTime) + ")" + userFrom.UserNick + "发送了一个闪屏振动"), WebElement.New("div", null, "wChatMsgSplit"));
                                gWindowManager.FlashWindow(gWindowManager.GetWindow(uid, 1));
                                _me.Common.playSound("flash");
                                objChat.scrollTop = objChat.scrollHeight;
                                if (msg.From != _me.Profile.UserID) {
                                    _me.Common.cancelCheckNewsShow();
                                    _me.Common.checkNewMsgShow(_me.Common.getUserFromArr(msg.From, _me.Friend).UserNick);
                                }
                            }
                        }, 100)
                    });
                    break;
                case gMessagesType.mtAddFriend: //添加好友
                    switch (parseInt(msg.Code)) {
                        case gAddFriendResult.ADD_FRIEND_SUCCESS: //添加成功						
                            {
                                EventQueue.IsInstance().RegisterEvent(msg.From, gEventID.eiAddFriendSuccess,
								function (Data) {
								    _me.CMD.renderMyFriend();
								    _me.Common.showLink(260, 185, null, null, gFixFormID.fiAddFriendSucess, "添加好友成功", _me.MessageHandler.getProcessUrl("AddFriendSuccessUrl") + "?domain=" + gRunerDomain + "&uid=" + Data['dst'] + "&nick=" + encodeURIComponent(Data['UserNick']) + "&gender=" + Data['Gender'], gImgServer + "/Images/ToolAddFriend.gif");

								});
                                _me.Data.getFriendUserFullInfo(msg.From);
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_AUTHER: //需要认证
                            {
                                EventQueue.IsInstance().RegisterEvent(msg.From, gEventID.eiAddFriendAuther,
								function (Data) {
								    _me.Common.showLink(300, 220, null, null, gFixFormID.fiAddFriendSucess, "添加好友-请发送验证信息", _me.MessageHandler.getProcessUrl("AddFriendRequestUrl") + "?domain=" + gRunerDomain + "&uid=" + Data['dst'] + "&nick=" + encodeURIComponent(Data['UserNick']) + "&gender=" + Data['Gender'], gImgServer + "/Images/ToolAddFriend.gif");
								});
                                _me.Data.getFriendUserFullInfo(msg.From);
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_ANSWER: //需要回答问题
                            {
                                EventQueue.IsInstance().RegisterEvent(msg.From, gEventID.eiAddFriendAuther,
								function (Data) {
								    _me.Common.showLink(300, 220, null, null, gFixFormID.fiAddFriendSucess, "添加好友-请回答对方设置的问题", _me.MessageHandler.getProcessUrl("AddFriendAnswerUrl") + "?domain=" + gRunerDomain + "&uid=" + Data['dst'] + "&nick=" + encodeURIComponent(Data['UserNick']) + "&gender=" + Data['Gender'] + "&question=" + encodeURIComponent(msg.Content), gImgServer + "/Images/ToolAddFriend.gif");
								});
                                _me.Data.getFriendUserFullInfo(msg.From);
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_EXIST:
                            {
                                _me.Common.showAlert("这个用户已经是您的好友。", "提示");
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_FORBIDDEN:
                            {
                                _me.Common.showAlert("对方已设置禁止添加好友。", "提示");
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_EXCEED:
                            {
                                _me.Common.showAlert("对方好友数量达到上限。", "提示");
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_TOOFAST:
                            {
                                _me.Common.showAlert("不允许频繁的操作。", "提示");
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_SELF:
                            {
                                _me.Common.showAlert("不能加自己为好友。", "提示");
                                break;
                            }
                        case gAddFriendResult.ADD_FRIEND_BLACKLIST:
                            {
                                _me.Common.showAlert("您在对方黑名单中，不能添加为好友。", "提示");
                                break;
                            }
                    }
                    break;
                case gMessagesType.MSG_AUTH_REQ: //加好友请求
                    //_me.CMD.stopMsgInterval();
                    _me.Common.showConfirm(msg.From + " 请求加您为好友，同意吗？<br><br>验证信息：" + msg.Content, "提示",
					function () {
					    if (arguments[2]) {
					        _me.Data.acceptAddFriend(msg.From);
					    }
					});
                    break;
                case gMessagesType.MSG_YOU_ARE_ADDED: //同意认证，被其它好友添加成功  
                    if (!_me.Common.getUserFromArr(msg.From, _me.Friend)) { //如果已经有此好友，则不能重复增加   
                        _me.Friend.add(new _me.Model.User(null, msg.From, null, msg.From, msg.From, parseInt(msg.Status), 0, 0, null, null));
                    }
                    EventQueue.IsInstance().RegisterEvent(msg.From, gEventID.eiAddFriendAutherSuccess,
                    function (Data) {
                        _me.CMD.renderMyFriend();
                        _me.Common.showLink(250, 185, null, null, gFixFormID.fiAddFriendSucess, "添加好友成功", _me.MessageHandler.getProcessUrl("AddFriendSuccessUrl") + "?domain=" + gRunerDomain + "&uid=" + Data['dst'] + "&nick=" + encodeURIComponent(Data['UserNick']) + "&gender=" + Data['Gender'], gImgServer + "/Images/ToolAddFriend.gif");
                    });
                    _me.Data.getFriendUserFullInfo(msg.From);
                    break;
                case gMessagesType.MSG_AUTH_ACCEPTED_ADD_REQ: //通过认证，添加好友成功  
                    if (!_me.Common.getUserFromArr(msg.From, _me.Friend)) //如果已经有此好友，则不能重复增加
                        _me.Friend.add(new _me.Model.User(null, msg.From, null, msg.From, msg.From, parseInt(msg.Status), 0, 0, null, null));
                    _me.Data.getFriendUserInfo(msg.From);
                    break;
                case gMessagesType.MSG_AUTH_ANSWER_WRONG: //回答认证问题错误
                    _me.Common.showAlert("答案不正确，添加好友失败。", "提示");
                    break;
                case gMessagesType.MSG_AUTH_ANSWER_CORRECT: //回答问题正确，添加好友成功                      
                    _me.Friend.add(new _me.Model.User(null, msg.From, null, msg.From, msg.From, parseInt(msg.Status), 0, 0, null, null));
                    EventQueue.IsInstance().RegisterEvent(msg.From, gEventID.eiAddFriendAutherSuccess,
					function (Data) {
					    _me.CMD.renderMyFriend();
					    _me.Common.showLink(250, 185, null, null, gFixFormID.fiAddFriendSucess, "添加好友成功", _me.MessageHandler.getProcessUrl("AddFriendSuccessUrl") + "?domain=" + gRunerDomain + "&uid=" + Data['dst'] + "&nick=" + encodeURIComponent(Data['UserNick']) + "&gender=" + Data['Gender'], gImgServer + "/Images/ToolAddFriend.gif");
					});
                    _me.Data.getFriendUserInfo(msg.From);
                    break;
                case gMessagesType.mtDelFriend: //删除好友
                    _me.Friend.remove(userFrom);
                    HeadManager.IsInstance().DelUserHeadByUser(userFrom.UserID);
                    _me.CMD.renderMyFriend();
                    break;
                case 5: //用户信息改变
                    _me.Data.getFriendUserInfo(msg.From,
					function (o) {
					    if (!o) return;
					    if (!$T(o.responseXML, "list")) return;
					    var result = _me.Common.getUserFromXml($T(o.responseXML, "list").item(0));
					    if (!result || result.length < 1) return;
					    _me.Common.updateUserInfoEx(result[0]);
					    _me.CMD.renderMyFriend();
					    _me.Common.playSound("friendonline");
					});
                    break;
                case 8: //退出
                    _me.Common.showAlert(msg.Content, "提示",
					function () {
					    _me.CMD.destroyMainWindow();
					});
                    break;
                case 9: //群消息	
                    if (gWindowManager.GetWindow(msg.To, 4))  //如果聊天窗口已经是打开的，则直接显示
                    {
                        _me.CMD.showTeamTextContent(msg.To, msg);  //直接弹出并显示
                    }
                    else {
                        if (_me.Config.AutoShowMsg == 1)  //自动弹出消息
                            _me.CMD.showTeamTextContent(msg.To, msg)  //直接弹出并显示
                        else
                            HeadManager.IsInstance().GetTeamHeadByTeamID(msg.To, 1).StartFlash();
                    }
                    break;
                case 10: //加入群组请求
                    _me.Common.showConfirm(msg.Content + "，同意吗？", "提示",
					function () {
					    if (arguments[2]) {
					        _me.Data.acceptJoinTeam(msg.To, msg.From, function () {
					            _me.Data.getFriendUserInfo(msg.From,
								function (o) {
								    if (!o) return;
								    if (!$T(o.responseXML, "list")) return;
								    _me.Data.getMyTeamUserList(msg.To);
								    /*var profile = _me.Common.getUserFromXml($T(o.responseXML,"list").item(0))[0];
								    _me.Friend.add(profile);
								    _me.CMD.renderMyFriend();*/
								});
					        });
					    }
					});
                    break;
                case 11: //加入群组成功
                    _me.Data.getMyTeamList(
					function (o) {
					    if (!o) return;
					    if (!$T(o.responseXML, "list")) return;
					    _me.CMD.renderMyTeam();
					});
                    break;
                case 12: //退出群组
                    _me.Friend.remove(userFrom);
                    _me.CMD.renderMyFriend();
                    break;
                default: break;
            }

            MessageQueue.IsInstance().RemoveMessage(msg);  //清除消息
        }
    };
    //初始化
    this.Initialize = function () {
        if (!Other.TestCookie()) {
            alert("提示：您的浏览器不支持Cookie，部分功能将无法使用!");
        }
        //alert(Other.GetCookie("webIM_et_ptid"));
        SwitchPlatform(Platform);
        gWindowManager = WindowManager.IsInstance();
        gWindowManager.InitWindowManager();
        switch (_CommunicationCore) {
            case CommunicationCore.ccAjax: _me.MessageHandler = AjaxMessageHandler.IsInstance(_me);
            case CommunicationCore.ccChunk: _me.MessageHandler = ChunkMessageHandler.IsInstance(_me);
        }
        _me.ReqQueue = new DED.Queue;
        _me.Sys = new _me.Model.Sys();
        if (Other.GetCookie("WebIM_ChatRoom") != "" || Other.GetCookie("WebIM_ChatRoom") != null) {
            _me.Data.killCometClient(Other.GetCookie("WebIM_ChatRoom"));
        }
        Comet.IsInstance(_me, gScriptServer).initialize(_me.CMD.initSideBar); //先初化Comet，防止Comet中的脚本在第一次sendRequest时来不及下载完成
        _me.Common.cancelCheckNewsShow();
        _me.CMD.openMiniTeamChatWindow(); 
    }
    this.UnInitialize = function () {
        Other.SetCookie("autologin", "2");
        _me.ServerSessionID = null; //服务器长连接会话Id
        _me.ReqQueue.clear();
        _me.Sys = new _me.Model.Sys();
        _me.Profile = null;  //个人资料
        _me.PrifileEx = null; //个人扩展资料
        _me.Config = null;  //个人配置
        _me.Friend = null;  //好友列表	
        _me.Group = null;  //分组信息
        _me.Team = null;  //群组信息			
        _me.FriendLoadedCount = 0;
		_me.ServerUrl = gConnServer + ":" + gConnServerPort;
    }
}