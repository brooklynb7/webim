//**********************************
//*全局常量
//**********************************
var gDebugLog = false;
var gStyleManager = null;
var gWindowManager = null;      //窗体管理器
var gClipBoardData = null;
var gDeployPath = null;
//var gConnServer = "http://im.sdo.com"; //长连接服务器地址
var gConnServerPort = "8080";  //长连接服务器端口
//var gPageServer = "http://webim.sdo.com"; //内容服务器
var gPageServerPort = "80";
var gTeamServer = "http://webim.sdo.com"; //群服务器
var gAjaxProxyUrl = "/AjaxHandler/GetChunkContent.[ext]?RequestUrl="; //Ajax跨域请求代理地址
var gAjaxProxyNoEncodeUrl = "/AjaxHandler/GetChunkContent.[ext]?IsEncode=0&RequestUrl="; //Ajax跨域请求代理地址(不做二次编码)
var gCTGb2312ToUTF8Url = "/AjaxHandler/GB2312ToUTF8.[ext]"; //编码转换服务器地址(GB2312 to UTF-8)
var gCTUTF8ToGB2312Url = "/AjaxHandler/UTF8ToGB2312.[ext]"; //编码转换服务器地址(UTF-8 to GB2312)
var gFormMain = 1;             //主窗体编号
var gFormLogin = 2;            //登录窗体编号
var gFormSideBar = 30;         //侧边栏编号
var gStyle = 1;                //使用样式
var gSmallHeadItemHeight = 28; //小头像列表方式的项高
var EmotionFolder = "emo_0/";  //表情文件夹
var gFixForm = {ffMain:[2,0],ffLogin:[3,6],ffAlert:[3,4],ffLink:3,ffChat:1,ffTeamChat:4,ffMiniTeamChat:5}; //固定窗体类型
var gFixFormID = { fiAddFriend: 5, fiAddFriendSucess: 6, fiAddFriendRequest: 7, fiAddFriendAnswer: 8, fiChatRoom: 9 }; //固定窗体ID
var gFixMenu = {fmPopMenu:100}; //菜单类 不得与窗体类型重复
var gFormPosition = {fpCustomer:0,fpWindowCenter:1};
var gFixValue = {fvNick:0,fvSign:2};
var gMessagesType = {mtChat:0,mtAddFriend:3,mtDelFriend:999,mtFlash:4,
					 MSG_AUTH_REQ:96,MSG_AUTH_REQ_ACCEPTED:97,MSG_AUTH_REQ_REJECTED:98,MSG_AUTH_ANSWER:99,MSG_AUTH_ACCEPTED_ADD_REQ:103,
					 MSG_AUTH_ANSWER_CORRECT:100,MSG_AUTH_ANSWER_WRONG:101,MSG_YOU_ARE_ADDED:102}; //消息类型
var gAddFriendResult = {ADD_FRIEND_SUCCESS:0,ADD_FRIEND_AUTHER:1,ADD_FRIEND_ANSWER:2,
						ADD_FRIEND_FORBIDDEN:3,ADD_FRIEND_EXIST:4,ADD_FRIEND_EXCEED:5,
						ADD_FRIEND_NO_UIN:6,ADD_FRIEND_TOOFAST:7,ADD_FRIEND_SELF:8,
						ADD_FRIEND_BLACKLIST:9,ADD_FRIEND_UNKNOWN:10}; //添加好好返回类型
var gUserStatus = {usOnline:1,usBusy:2,usLeave:3,usHide:-1,usOffline:0};//用户状态
var gFixGroups = {fgMyFriends:0,fgMyStranger:1,fgMyBlackList:2}; //固定分组ID
var gEventID = {eiAddFriendSuccess:0,eiAddFriendAuther:1,eiAddFriendAutherSuccess:2,eiAuthAcceptedAddReq:3,eiAuthAnswerCorrect:4,
				eiQueryOnlineUserInfo:4,eiEditUserGroup:1000,eiLoginSuccess:2000};//事件ID
var gFontFamily = "宋体"; //消息字体
var gFontSize = 12; //消息字体大小
var gFontFlag = "0"; //消息字形
var gFontColor = "#000000"; //消息颜色
var gBlinkSwitch = 0;//TITLE闪动计数
var gBlinkId = 0; //TITLE闪动代号
var gBrowserTitle = "WebIM Test";
var gMiniTeamChatInterval = 0;
var gMiniTeamChatLastMid = 0;

var gChatRoomNick = null;
				
function SwitchPlatform(_platform)
{
	switch(_platform)
	{
		case "NT":
		{
			gAjaxProxyUrl = gAjaxProxyUrl.replace(/\[ext\]/img,"asp");
			gAjaxProxyNoEncodeUrl = gAjaxProxyNoEncodeUrl.replace(/\[ext\]/img,"asp");
			gCTGb2312ToUTF8Url = gCTGb2312ToUTF8Url.replace(/\[ext\]/img,"asp");
			gCTUTF8ToGB2312Url = gCTUTF8ToGB2312Url.replace(/\[ext\]/img,"asp");
			break;
		}
		case "PHP":
		{
			gAjaxProxyUrl = gAjaxProxyUrl.replace(/\[ext\]/img,"php");
			gAjaxProxyNoEncodeUrl = gAjaxProxyNoEncodeUrl.replace(/\[ext\]/img,"php");
			gCTGb2312ToUTF8Url = gCTGb2312ToUTF8Url.replace(/\[ext\]/img,"php");
			gCTUTF8ToGB2312Url = gCTUTF8ToGB2312Url.replace(/\[ext\]/img,"php");			
			break;
		}
		default:alert("Platform Error.");
	}	
}

/*var LoadBalancing = function()
{
	var uniqueInstance = null;
	var _imServer = null;
	this.IMServerList = ["61.129.44.33","61.129.44.34","61.129.44.35","61.129.44.36","61.129.44.40"];
	function Constructor()
	{
		return{	
			getIMServe:function()
			{	
				return _imServer;
			}
		}
	}
	
	return{
		IsInstance:function()
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
				_imServer = IMServerList[Math.floor(Math.random() * IMServerList.length + 1)-1];
			}
			return uniqueInstance;
		}
	}	
}()
var gServer = LoadBalancing.IsInstance().getIMServe();*/