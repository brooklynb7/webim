//*用户控件

//**********************************
//*头像管理器 对头像使用单例模式
//**********************************
var HeadManager = function()
{
	var uniqueInstance = null;
	_HeadsList = new Array(); //头像列表

	function Constructor()
	{
		return{
			GetUserHeadByUser:function(u,n)//根据用户对象找到对应的头像
			{
				var Finded = false;
				var obj = null;
				for(var i = 0 ;i < _HeadsList.length;i++)
				{
					if (_HeadsList[i].User)
					{			
						if(_HeadsList[i].User.UserID==u.UserID && _HeadsList[i].Type==n)
						{
							obj = _HeadsList[i];
							Finded = true;
							break;
						}
					}
				}
				
				if (!Finded) 
				{
					obj = new UserHead(u,null,n);
					_HeadsList.add(obj);
				}
				return obj;
			},
			
			GetUserHeadByUserID:function(UserID)//根据用户ID找到对应的头像
			{
				var obj = null;
				for(var i = 0 ;i < _HeadsList.length;i++)
				{
					if (_HeadsList[i].User)
					{				
						if(_HeadsList[i].User.UserID==UserID && _HeadsList[i].Type==1)
						{
							obj = _HeadsList[i];
							break;
						}
					}
				}
				//如果没有找到此用户，则为此用户创建一个陌生人头像
				return obj;
			},
			
			GetTeamHeadByTeam:function(t,n) //根据Team对象找到对应的头像
			{
				var Finded = false;
				var obj = null;
				for(var i = 0 ;i < _HeadsList.length;i++)
				{
					if (_HeadsList[i].Team)
					{
						if(_HeadsList[i].Team.TeamID==t.TeamID && _HeadsList[i].Type==2)
						{
							obj = _HeadsList[i];
							Finded = true;
							break;
						}
					}
				}
				
				if (!Finded) 
				{
					obj = new UserHead(null,t,n);
					_HeadsList.add(obj);
				}
				return obj;		
			},
			
			GetTeamHeadByTeamID:function(TeamID)//根据TeamID找到对应的头像
			{
				var obj = null;		
				for(var i = 0 ;i < _HeadsList.length;i++)
				{
					if (_HeadsList[i].Team)
					{			
						if(_HeadsList[i].Team.TeamID==TeamID && _HeadsList[i].Type==2)
						{
							obj = _HeadsList[i];
							break;
						}
					}
				}
				return obj;
			},
			
			DelUserHeadByUser:function(UserID)
			{
				for(var i = 0 ;i < _HeadsList.length;i++)
				{
					if (_HeadsList[i].User)
					{				
						if(_HeadsList[i].User.UserID==UserID)
						{
							_HeadsList[i].StopFlash();
							_HeadsList.remove(_HeadsList[i]);
							break;
						}
					}
				}		
			}
		}
	}
	
	return{
		IsInstance:function()
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
			}
			return uniqueInstance;
		}
	}		
}()

//******************************
//*用户头像
//******************************
function UserHead(_u,_t,n)
{
	var _me = this;				
	var _IsFlash = false; //是否在闪动
	this.FlashTimer = null;
	this.Type = _t==null?1:2;  //头像类型：1用户2群组
	this.Mode = n;      //头像模式：1简要2详细	
	this.User = _u;	
	this.Team = _t;
	this.GetHtmlSource = function()
	{		
		var str = new StringBuilder();
		switch(_me.Type)
		{
			case 1:
				var ban = _u.IsBlocked?"b":"";	
				switch(_me.Mode)
				{
					case 1:
						str.add("<div id=\"wMainUserItemType"+_me.Type+"Id"+_me.User.UserID+"\" class=\"wMainUserItem\" uid=\""+_me.User.UserID+"\" onmouseover=\"_webIM.Common.listItemChange(this,event,2)\" onmouseout=\"_webIM.Common.listItemChange(this,event,3)\" onmousedown=\"_webIM.Common.listItemChange(this,event,1)\" onmouseup=\"_webIM.Common.showContextMenu(this,event,"+gFixForm.ffMain[0]+","+gFixForm.ffMain[1]+")\" oncontextmenu=\"function(){return !!0;}\" ondblclick=\"var uid = this.getAttribute('uid');if(uid)_webIM.CMD.openChatWindow(uid,true);\">");
						str.add("<div class=\"wMainListButton\" onmouseover=\"this.className='wMainListButton wMainListButtonHover'\" onmouseout=\"this.className='wMainListButton'\">");
						str.add("<img id=\"wMainUserItemImgType"+_me.Type+"Id"+_me.User.UserID+"\" src=\"" + gStyleManager.getStyleResPath(gStyle) + _me.User.UserGender + _me.User.UserStatus+ban+".gif\" title=\"查看此人的详细信息\" style=\"height:20px;width:20px;padding:1px;\" onclick=\"_webIM.CMD.showCard("+_me.User.UserID+",this.parentNode)\"/>");
						str.add("</div>");
						str.add("<div class=\"wMainUserItemText\">"+(_me.User.CustomName||_me.User.UserNick)+"&nbsp;-&nbsp;<span style=\"color:#777\">"+_me.User.UserSign+"</span></div>");
						str.add("</div>");
						break;
					case 2:
						str.add("<div id=\"wMainUserItemType"+_me.Type+"Id"+_me.User.UserID+"\" class=\"wMainUserItemBig\" uid=\""+_me.User.UserID+"\" onmousedown=\"_webIM.Common.listItemChange(this,event,1)\" onmouseup=\"_webIM.Common.showContextMenu(this,event,"+gFixForm.ffMain[0]+","+gFixForm.ffMain[1]+")\" oncontextmenu=\"function(){return !!0;}\" ondblclick=\"var uid = this.getAttribute('uid');if(uid)_webIM.CMD.openChatWindow(uid,true);\">");
						str.add("<div id=\"wMainUserItemImgType"+_me.Type+"Id"+_me.User.UserID+"\" class=\"wMainUserItemFaceBG\"><img style=\"width:46px;height:45px\" src=\""+_me.User.UserAvatar+"\" /></div>");
						str.add("<div class=\"wMainUserItemBigRight\">");
						//str.add("<div class=\"wMainUserItemStatus\" ><img style=\"height:19px;width:19px\" src=\"" + gStyleManager.getStyleResPath(gStyle) + _me.User.UserGender +_me.User.UserStatus+ban+".gif\"/></div>");
						str.add("<div style=\"padding-top:2px\">"+(_me.User.CustomName||_me.User.UserNick)+"</div>");
						str.add("<div style=\"padding-top:2px;color:#777\">"+_me.User.UserSign+"</div>");
						str.add("<div style=\"padding-top:2px\">"+_me.User.UserEmail+"</div>");
						str.add("</div>");
						str.add("</div>");
						break;
					case 3:
						str.add("<div class=\"wMainUserItemBig\" style=\"padding-left:0\">");
						str.add("<div class=\"wMainUserItemFaceBG\"><img  style=\"width:44px;height:44px;padding:2px 2px\" src=\""+_me.User.UserAvatar+"\" /></div>");
						str.add("<div class=\"wMainUserItemBigRight\">");
						str.add("<div class=\"wMainUserItemStatus\" ><img style=\"height:19px;width:19px\" src=\"" + gStyleManager.getStyleResPath(gStyle) + _me.User.UserGender + _me.User.UserStatus+ban+".gif\"/></div>");
						str.add("<div style=\"padding-top:2px\">" + (_me.User.CustomName||_me.User.UserNick).FixLength(18) + "</div>");
						str.add("<div style=\"padding-top:2px;color:#777\">"+_me.User.UserSign+"</div>");
						str.add("<div style=\"padding-top:2px\">"+_me.User.UserEmail+"</div>");
						str.add("</div>");
						str.add("</div>");
						break;
				}
				break;
			case 2:
				var ban = _t.IsBlocked?"b":"";	
				str.add("<div id=\"wMainUserItemType"+_me.Type+"Id"+_me.Team.TeamID+"\" class=\"wMainUserItem\" tid=\""+_me.Team.TeamID+"\" onmousedown=\"_webIM.Common.listItemChange(this,event,1)\" onmouseup=\"_webIM.Common.showContextMenu(this,event,"+gFixForm.ffMain[0]+","+gFixForm.ffMain[1]+")\" oncontextmenu=\"function(){return !!0;}\" ondblclick=\"var tid = this.getAttribute('tid');if(tid)_webIM.CMD.openTeamChatWindow(tid,true);\">");
				str.add("<div class=\"wMainListButton\" onmouseover=\"this.className='wMainListButton wMainListButtonHover'\" onmouseout=\"this.className='wMainListButton'\">");
				str.add("<img id=\"wMainUserItemImgType"+_me.Type+"Id"+_me.Team.TeamID+"\" src=\""+gImgServer+"/Images/Team.gif\" title=\"查看此群信息\" style=\"height:20px;width:20px;padding:1px;\" onclick=\"_webIM.CMD.showCard("+_me.Team.TeamID+",this.parentNode)\"/>");
				str.add("</div>");
				str.add("<div class=\"wMainUserItemText\">"+_me.Team.TeamName+"&nbsp;─&nbsp;<span style=\"color:#777\">"+_me.Team.TeamDescript+"</span></div>");
				str.add("</div>");			
				break;
		}
		return str.toString();
	}	
	
	this.StartFlash = function()
	{				
		if (_IsFlash) return;		
		_me.FlashTimer = setInterval(
		function()
		{		
			var o = null;
			switch(_me.Type)
			{
				case 1:o=$("wMainUserItemImgType"+_me.Type+"Id"+_me.User.UserID);break;
				case 2:o=$("wMainUserItemImgType"+_me.Type+"Id"+_me.Team.TeamID);break;
			}			
			if (!o) return;			
			WebElement.Visible(o,!WebElement.IsHide(o));			
		},400);
		_IsFlash = true;
	}
	
	this.StopFlash = function()
	{
		clearInterval(_me.FlashTimer);
		var o = null;
		switch(_me.Type)
		{
			case 1:o=$("wMainUserItemImgType"+_me.Type+"Id"+_me.User.UserID);break;
			case 2:o=$("wMainUserItemImgType"+_me.Type+"Id"+_me.Team.TeamID);break;
		}
		WebElement.Show(o);
		_IsFlash = false;
	}	
	//_me.StartFlash();
}

//**********************************
//*菜单管理器
//**********************************
var MenuManager = function()
{
	var uniqueInstance = null;
	_MenuList = new Array();

	function Constructor()
	{
		return{
			GetPopMenu:function(t,id)//根据用户对象找到对应的头像
			{
				var Finded = false;
				var obj = null;
				for(var i = 0 ;i < _MenuList.length;i++)
				{			
					if(_MenuList[i].Type==t && _MenuList[i].Id==id)
					{
						obj = _MenuList[i];
						Finded = true;
						break;
					}
				}
				
				if (!Finded) 
				{
					obj = new PopMenu("PopMenu",t,id);
					_MenuList.add(obj);
				}
				return obj;
			},
			ScrollMenu:function()
			{
				for(var i = 0 ;i < _MenuList.length;i++)
				{
					var _Menu = $("PopMenuId"+_MenuList[i].Id+"Type"+_MenuList[i].Type);
					if(_Menu)
					{
						_Menu.style.top = (_MenuList[i].Top + WebElement.scrollHeight()) + "px";
						_Menu.style.left = (_MenuList[i].Left + WebElement.scrollWidth()) + "px";
						if(parseInt(_Menu.style.top)+_MenuList[i].Height>WebElement.Height()+WebElement.scrollHeight())
						{
							_Menu.style.top = (WebElement.Height()+WebElement.scrollHeight()-_MenuList[i].Height-6)+"px";
						}
					}
				}				
			}
		}
	}
	
	return{
		IsInstance:function()
		{
			if(uniqueInstance == null)
			{
				uniqueInstance = Constructor();
			}
			return uniqueInstance;
		}
	}		
}()

//******************************
//*右键弹出菜单 用法 new PopMenu(_sid,_t,_id).Show()
//******************************
function PopMenu(_sid,_t,_id)
{
	var _me = this;
	this.Type = _t;
	this.Id = _id;
	this.MenuId = _sid+"Id"+_id+"Type"+_t;
	this.E = null;
	this.Top = null;
	this.Left = null;
	this.Parent = null;
	this.Data = new Array();
	this.HasIcon = !!0;
	this.Width = 100;
	this.ItemHeight = 19;
	this.ShowCorner = !!0;
	this.ShowLeftBG = !!0;
	this.Show = function()
	{		
		this.Left = Evt.Left(this.E);
		this.Top = Evt.Top(this.E);
		//this.Top = this.Top>(WebElement.Height()-height-10)?(this.Top-height-6):this.Top;
		//this.Left = this.Left>(WebElement.Width()-this.Width-10)?(this.Left-this.Width-6):this.Left;
		$(this.MenuId)&&WebElement.Del($(this.MenuId));
		var height = 0;
		var sb = new StringBuilder();
		for(var f=0;f<this.Data.length;f++)
		{
			sb.add("<div onselectstart=\"return false;\"");
			if(this.Data[f]!="")
			{
				var arrT=this.Data[f].split('|');
				var itemClassName=this.HasIcon?"popMenuItemWithIcon":"popMenuItem";
				sb.add("class=\""+itemClassName+"\" ");
				if(arrT.length>3)
				{
					sb.add("onmouseover=\"this.className='"+itemClassName+" popMenuItemHover';this.style.backgroundImage='url("+arrT[3]+")'\" onmouseout=\"this.className='"+itemClassName+"';this.style.backgroundImage='url("+arrT[2]+")'\"");
				}
				else
				{
					sb.add("onmouseover=\"this.className='"+itemClassName+" popMenuItemHover'\" onmouseout=\"this.className='"+itemClassName+"'\"");
				}
				if(arrT.length>2)sb.add("style=\"background-image:url("+arrT[2]+")\"");
				sb.add(" onclick=\"Evt.NoBubble(event);WebElement.Hid('"+_sid+"Id"+_id+"Type"+_t+"');"+arrT[1]+"\" ");		
				sb.add(">"+arrT[0]+"</div>");
				height+=_me.ItemHeight;
			}
			else
			{
				sb.add("class=\"popMenuItemDisabled\"></div>");
				height+=7;
			}
		}		
		_me.Height = height;
		var divMenu = WebElement.New("div",_me.MenuId,"popMenu",sb.toString());
		if(this.ShowLeftBG)
		{
			var divLeftBG=WebElement.New("div","","MenuLeftBG");
			divLeftBG.style.top = "-" + height +"px";
			//divLeftBG.style.left = this.Left+"px";
			divLeftBG.style.height=height+"px";
			WebElement.Add(divMenu,divLeftBG);
		}
		if(this.ShowCorner)
		{
			WebElement.Add(divMenu,
				divMenu.appendChild(WebElement.New("div","","wRound_lt")),
				divMenu.appendChild(WebElement.New("div","","wRound_rt")),
				divMenu.appendChild(WebElement.New("div","","wRound_lb")),
				divMenu.appendChild(WebElement.New("div","","wRound_rb")));
		}
		WebElement.Add(this.Parent||"windowContainerType"+_t+"Id"+_id,divMenu);		
		var ms = divMenu.style;		
		ms.width = this.Width+"px";
		ms.height = height+"px";
		ms.top = this.Top+"px";
		ms.left = this.Left+"px";
		//ms.zIndex = ++window.zIndex;
		WebElement.Show(divMenu);
		Evt.NoBubble(this.E);
		divMenu.oncontextmenu = function(){return !!0;};		
		document.onmousedown = function(e)
		{
			var _e = window.event || e;
			var obj = _e.srcElement?_e.srcElement:_e.target;
			obj = obj.parentElement||obj.parentNode;
			if((obj.className!="popMenu"))
			{
				WebElement.Hid(_me.MenuId);
				document.onmousedown = null;
			}
		}
		MenuManager.IsInstance().ScrollMenu();
	};			
}

//******************************
//*CheckBox，用法 new CheckBox(obj).Render();//obj:一个type="checkbox"并且属于容器里唯一元素的input
//******************************
function CheckBox(_checkobj,_text)
{
	this.checkObj = _checkobj.value?_checkobj:$(_checkobj);
	this.text = _text;
	var _me = this;
	this.Render = function()
	{
		WebElement.Hid(this.checkObj);
		var oImg = WebElement.New("div","",this.checkObj.checked?"cCheckBoxChecked":"cCheckBox");
		ois = oImg.style;
		ois.cursor = "pointer";
		WebElement.Add(this.checkObj.parentNode,oImg);
		oImg.onclick = this.checkedChanged;
		_me.objImg = oImg;
		if(this.text)
		{
			var oSpan = WebElement.New("span","","",this.text);
			oss = oSpan.style;
			oss.paddingLeft = "5px";
			oss.cursor = "pointer";
			oss.fontSize = "12px";
			oSpan.onclick = this.checkedChanged;
			oSpan.onselectstart = function (){return false;};
			WebElement.Add(this.checkObj.parentNode,oSpan);
		}
	}
	this.checkedChanged = function()
	{
		_me.objImg.className=_me.objImg.className.indexOf("Checked")>0?"cCheckBox":"cCheckBoxChecked";
		_me.checkObj.checked=!_me.checkObj.checked;
	}
}

//**********************************
//*dom
//**********************************
var WebElement = {
    New: function (a, d, c, i)//新建element,a:类型,d:id,c:,className,i:innerHTML
    {
        var o = document.createElement(a);
        if (d) o.id = d;
        if (c) o.className = c;
        if (i) o.innerHTML = i;
        return o;
    },
    Add: function (o)//为o元素追加子元素,可一个传进多个子元素
    {
        if (arguments.length == 1) {
            WebElement.Add(document.body, o);
            return;
        }
        var o = $(o);
        if (!o) return;
        for (var i = 1; i < arguments.length; i++) {
            o.appendChild($(arguments[i]));
        }
        return o;
    },
    Del: function ()//删除元素
    {
        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i]) {
                arguments[i].parentNode.removeChild(arguments[i]);
                arguments[i] = null;
            }
        }
    },
    Hid: function ()//隐藏
    {
        for (var i = 0; i < arguments.length; i++) {
            if ($(arguments[i]))
                $(arguments[i]).style.display = "none";
        }
    },
    Focus: function (obj) {//焦点移到最后一个文本
        var tar = $(obj);
        if (tar.setSelectionRange) {
            tar.setSelectionRange(tar.value.length, tar.value.length);
        } else {
            var range = tar.createTextRange();
            range.moveStart("character", tar.value.length);
            range.moveEnd("character", tar.value.length);
            range.select();
        }
        tar.focus();
    },
    Show: function ()//显示
    {
        for (var i = 0; i < arguments.length; i++) {
            if ($(arguments[i]))
                $(arguments[i]).style.display = "block";
        }
    },
    IsHide: function (o)//是否可见
    {
        if ((o.style.display == "none") || (o.style.display == "")) {
            return false;
        }
        else {
            return true;
        }
    },
    Visible: function (o, v) {
        if (v) this.Show(o);
        if (!v) this.Hid(o);
    },
    Value: function (o, s)//赋值
    {
        s = s ? s : "";
        if (!$(o)) return;
        var tag = $(o).tagName.toUpperCase();
        if (tag == "INPUT" || tag == "TEXTAREA" || tag == "SELECT") {
            $(o).value = s;
        }
        else {
            $(o).innerHTML = s;
        }
    },
    Append: function (o, s)//追加内容
    {
        var tag = $(o).tagName.toUpperCase();
        if (tag == "INPUT" || tag == "TEXTAREA") {
            $(o).value += s;
        }
        else {
            $(o).innerHTML += s;
        }
    },
    Toggle: function ()//原本显示则隐藏，反正也如此
    {
        for (var i = 0; i < arguments.length; i++) {
            if ($(arguments[i]))
                $(arguments[i]).style.display = $(arguments[i]).style.display == "none" ? "block" : "none";
        }
    },
    Enable: function ()//可用
    {
        for (var i = 0; i < arguments.length; i++) {
            $(arguments[i]).disabled = "";
        }
    },
    Disable: function ()//不可用
    {
        for (var i = 0; i < arguments.length; i++) {
            $(arguments[i]).disabled = "disabled";
        }
    },
    Child: function (o) {
        return $(o).childNodes;
    },
    MousePos: function (ev) {
        if (ev.pageX || ev.pageY)
        { return { x: ev.pageX, y: ev.pageY }; }
        return { x: ev.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft), y: ev.clientY + (document.documentElement.scrollTop || document.body.scrollTop) };
    },
    GetX: function (o, po) {
        for (var lx = 0; o != po; lx += o.offsetLeft, o = o.offsetParent);
        return lx;
    },
    GetY: function (o, po) {
        for (var ly = 0; o != po; ly += o.offsetTop, o = o.offsetParent);
        return ly;
    },
    Top: function ()//屏幕可视范围离页面顶距离,放在这里是为了好管理
    {
        if (typeof (window.pageYOffset) != 'undefined') {
            return window.pageYOffset;
        }
        else if (typeof (document.compatMode) != 'undefined' && document.compatMode != 'BackCompat') {
            return document.documentElement.scrollTop;
        }
        else if (typeof (document.body) != 'undefined') {
            return document.body.scrollTop;
        }
    },
    Left: function ()//屏幕可视范围离左边距离
    {
        if (typeof (window.pageXOffset) != 'undefined') {
            return window.pageXOffset;
        }
        else if (typeof (document.compatMode) != 'undefined' && document.compatMode != 'BackCompat') {
            return document.documentElement.scrollLeft;
        }
        else if (typeof (document.body) != 'undefined') {
            return document.body.scrollLeft;
        }
    },
    Width: function ()//屏幕可视范围宽
    {
        if (typeof (document.compatMode) != 'undefined' && document.compatMode != 'BackCompat') {
            return document.documentElement.clientWidth;
        }
        else if (typeof (document.body) != 'undefined') {
            return document.body.clientWidth;
        }
    },
    Height: function ()//屏幕可视范围高
    {
        if (typeof (document.compatMode) != 'undefined' && document.compatMode != 'BackCompat') {
            return document.documentElement.clientHeight;
        }
        else if (typeof (document.body) != 'undefined') {
            return document.body.clientHeight;
        }
    },
    screenWidth: function ()//屏幕总宽
    {
        return this.Width() > document.documentElement.scrollWidth ? this.Height() : document.documentElement.scrollWidth;
    },
    screenHeight: function ()//屏幕总高
    {
        return this.Height() > document.documentElement.scrollHeight ? this.Height() : document.documentElement.scrollHeight;
    },
    scrollWidth: function ()//卷去的宽度
    {
        return document.documentElement.scrollLeft;
    },
    scrollHeight: function ()//卷去的高度
    {
        return document.documentElement.scrollTop;
    }
}

//*****************************************
//*Event
//*****************************************
var Evt = {
	NoBubble:function(e)
	{
		e&&e.stopPropagation?e.stopPropagation():event.cancelBubble=true;
	},
	Top:function(e)
	{
		return (e||event).clientY;
	},
	Left:function(e)
	{
		return (e||event).clientX;
	}
}

//******************************************
//other
//******************************************
var Other = {
    Even: function (n)//返回偶数
    {
        return parseInt((parseInt(n)) / 2) * 2;
    },
    Break: function (o, len)//自动换行,用于ff
    {
        var strContent = $F(o);
        var strTemp = "";
        while (strContent.length > len) {
            strTemp += strContent.substr(0, len) + "&#10;";
            strContent = strContent.substr(len, strContent.length);
        }
        strTemp += "&#10;" + strContent;
        return strTemp;
    },
    Browser: function () {
        if (!!window.opera) {
            return "opera";
        }
        else if (navigator.userAgent.toLowerCase().indexOf("safari") > 0) {
            return "safari";
        }
        else if (navigator.userAgent.toLowerCase().indexOf("gecko") > 0) {
            return "firefox";
        }
        else {
            return "ie";
        }
    },
    GetCookie: function (key) {
        var search = key + "=";
        if (document.cookie.length > 0) {
            var offset = document.cookie.indexOf(search);
            if (offset != -1) {
                offset += search.length;
                var end = document.cookie.indexOf(";", offset);
                if (end == -1) end = document.cookie.length;
                return unescape(document.cookie.substring(offset, end));
            }
            return "";
        }
        return "";
    },
    SetCookie: function (key, value, exp) {
        var today = new Date();
        var expires = new Date();
        value = value.toString();
        if (exp) {
            expires.setTime(today.getTime() + exp);
        }
        else {
            expires.setTime(today.getTime() + 1000 * 60 * 60 * 24 * 365);
        }
        document.cookie = key + "=" + value.escapeEx() + ";path=/; expires=" + expires.toGMTString();
    },
    TestCookie: function () {
        Other.SetCookie("test", "");
        var t = "test";
        Other.SetCookie("test", t);
        return Other.GetCookie("test") == t;
    }
}

//**********************************************************
//*StringBuilder
//**********************************************************
function StringBuilder()
{
	this._arr = new Array();
	this.add = function()
	{
		for(var i = 0;i<arguments.length;i++)
			this._arr.push(arguments[i]);
	}
	this.toString = function()
	{
		return this._arr.join("");
	}
}