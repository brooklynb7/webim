//**********************************
//*窗体管理器
//**********************************
var WindowManager = function() {
    var uniqueInstance = null;
    var _WindowsList = new Array(); //窗口列表

    var _GetWindowType = function(n) { return ["Chat", "Main", "Other", "TeamChat", "MiniTeamChat"][n - 1]; }
    var _GetObjByWindow = function(w) { return $("win" + _GetWindowType(w.type) + w.id); }

    function Constructor() {
        return {
            GetWindow: function(id, type)//根据窗口ID,TYPE找到对应窗口
            {
                var obj = null;
                for (var i = 0; i < _WindowsList.length; i++) {
                    if (_WindowsList[i].id == id && _WindowsList[i].type == type) {
                        obj = _WindowsList[i];
                        break;
                    }
                }
                return obj;
            },
            GetLastWindow: function(t)//得到类型为type的最后一个窗口
            {
                var obj = null;
                for (var i = 0; i < _WindowsList.length; i++) {
                    if (_WindowsList[i].type == t) {
                        obj = _WindowsList[i];
                    }
                }
                return obj;
            },
            GetMinWindowNum: function()//得到最小化窗口数量
            {
                var num = 0;
                for (var i = 0; i < _WindowsList.length; i++) {
                    _WindowsList[i].isMin && num++;
                }
                return num;
            },
            GetObjByWindow: function(w)//由窗体得到窗体最外层DIV
            {
                return $("win" + _GetWindowType(w.type) + w.id);
            },
            GetWindowType: function(n)//返回窗口类型
            {
                return _GetWindowType(n);
            },
            FlashWindow: function(w)//震动窗口
            {
                var oWin = _GetObjByWindow(w);
                var times = 24;
                var flag = !0;
                var x = parseInt(oWin.style.left);
                var y = parseInt(oWin.style.top);
                var intId = setInterval(function() {
                    oWin.style.left = (x + (flag ? 4 : 0)) + "px";
                    oWin.style.top = (y + (flag ? 4 : 0)) + "px";
                    flag = !flag;
                    times -= 1;
                    times < 1 && clearInterval(intId);
                }, 50);
            },
            ScrollWindows: function() //滚动窗体容器，使之总在可视范围内
            {
                if ($("windowContainerBorderS")) $("windowContainerBorderS").style.top = WebElement.Top() + "px";
                if ($("windowContainerBorderS")) $("windowContainerBorderS").style.left = WebElement.Left() + "px";

                //   $("windowSideBarContainer").style.top = WebElement.Top() + "px";
                //   $("windowSideBarContainer").style.left = WebElement.Left() + "px";

                var wMain = gWindowManager.GetWindow(gFixForm.ffMain[1], gFixForm.ffMain[0]);
                if (wMain) //存在主窗口
                {
                    var olem = $("winMain" + gFixForm.ffMain[1]);
                    if (wMain.isMin) {
                        olem.style.top = WebElement.scrollHeight() + "px";
                        olem.style.left = "0px";
                    }
                    else {
                        olem.style.top = (wMain.win.Top + WebElement.scrollHeight()) + "px";
                        olem.style.left = (wMain.win.Left + WebElement.scrollWidth()) + "px";
                    }

                    var EmotionList = $("divEmotionList");
                    if (EmotionList) { EmotionList.style.top = (parseInt(EmotionList.getAttribute("top")) + WebElement.scrollHeight()) + "px"; }

                    var card = $("divCardBorder")
                    if (card) { card.style.top = (parseInt(card.getAttribute("top")) + WebElement.scrollHeight()) + "px"; }
                }

                var t1 = 0, t2 = 0;
                var t1Max = parseInt((WebElement.Width() - 5) / 167);
                for (var i = 0; i < _WindowsList.length; i++) {
                    var w = _WindowsList[i];
                    if ((w.type == gFixForm.ffChat) || (w.type == gFixForm.ffTeamChat) || (w.type == gFixForm.ffMiniTeamChat))//处理最小化窗口了
                    {
                        var olem = $("win" + _GetWindowType(w.type) + w.id);
                        if (olem && w.isMin) {
                            var t1row = parseInt(t1 / t1Max);
                            var t1col = t1 - t1Max * t1row;
                            olem.style.top = WebElement.scrollHeight() + (WebElement.Height() - t1row * 25 - 26) + "px";
                            olem.style.left = t1col * 167 + 1 + "px";
                            t1++;
                        }
                        else {
                            olem.style.top = (w.win.Top + WebElement.scrollHeight()) + "px";
                            olem.style.left = (w.win.Left + WebElement.scrollWidth()) + "px";
                        }
                    }
                    if (w.win.ModalCover) {
                        var olem = $("windowCoverType" + w.type + "Id" + w.id);
                        olem.style.height = WebElement.screenHeight() + "px";
                        olem.style.width = WebElement.screenWidth() + "px";
                    }
                    var olem = $("winOther" + w.id);
                    if (olem) {
                        olem.style.top = (w.win.Top + WebElement.scrollHeight()) + "px";
                        olem.style.left = (w.win.Left + WebElement.scrollWidth()) + "px";
                    }

                    if (w.win.Left + w.win.Width > WebElement.Width()) { w.win.Left = WebElement.Width() - w.win.Width - 5; }
                    //if(w.win.Top+w.win.Height>WebElement.Height()){w.win.Top=WebElement.Height()-w.win.Height-5;}
                    //if(w.win.Top<WebElement.scrollHeight()){w.win.Top=WebElement.scrollHeight();}
                }
                MenuManager.IsInstance().ScrollMenu();
            },
            ListWindows: function() //维护窗体列表
            {
                var t1 = 0, t2 = 0;
                for (var i = 0; i < _WindowsList.length; i++) {
                    var w = _WindowsList[i];
                    if (w.type == 2 && w.isMin) {
                        var olem = $("win" + _GetWindowType(w.type) + w.id);
                        olem.style.top = "1px";
                        olem.style.left = t2 * 167 + 1 + "px";
                        t2++;
                    }
                    if (w.type == 6) {
                        var divTop, divLeft = 0;
                        divTop = parseInt(document.body.scrollTop, 10);
                        divLeft = parseInt(document.body.scrollLeft, 10);

                        var olem = $("winSideBar" + w.id);
                        olem.style.position = "absolute";
                        olem.style.top = (divTop + WebElement.Height() - w.win.Height - 2) + "px";
                        olem.style.left = (divLeft + WebElement.Width() - w.win.Width) + "px";
                        olem.style.width = Other.Even(w.win.Width) + "px";
                        olem.style.height = Other.Even(w.win.Height + 2) + "px";
                    }
                }

                gWindowManager.ScrollWindows();
            },
            GetWindowsList: function() { return _WindowsList },
            InitWindowManager: function() {
                window.onscroll = this.ScrollWindows;
                window.onresize = this.ListWindows;
                window.onerror = function() { return true; };
            }
        }
    }

    return {
        IsInstance: function() {
            if (uniqueInstance == null) {
                uniqueInstance = Constructor();
            }
            return uniqueInstance;
        }
    }
} ()