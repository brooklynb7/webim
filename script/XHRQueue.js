﻿var asyncRequest = (function () {
    this.retryCount = 3;
    this.currentRetry = 1;
    this.timeout = 5000;

    function handleReadyState(o, callback) {
        var poll = window.setInterval(function () {
            if (o && o.readyState == 4) {
                window.clearInterval(poll);
                if (callback) { callback(o); }
            }
        },
			100);
    }

    function handleTimeOut(o, retry, timeout) {
        this.retryCount = retry;
        this.timeout = timeout;
        var poll = window.setInterval(function () {
            if (this.currentRetry == this.retryCount) {
                //that.queue.shift();
                o.abort();
            }
            else {
                o.send(null);
                this.currentRetry++;
            }
        },
			this.timeout);
    }

    var getXHR = function () {
        var http;
        try {
            http = new XMLHttpRequest;
            getXHR = function () { return new XMLHttpRequest; };
        }
        catch (e) {
            var msxml = ['MSXML2.XMLHTTP.3.0',
        				 'MSXML2.XMLHTTP',
        				 'Microsoft.XMLHTTP'];
            for (var i = 0, len = msxml.length; i < len; ++i) {
                try {
                    http = new ActiveXObject(msxml[i]);
                    getXHR = function () { return new ActiveXObject(msxml[i]); };
                    break;
                }
                catch (e) { }
            }
        }
        return http;
    };

    return function (queue, request) {
        var http = getXHR();
        http.open(request['method'], request['uri'], true);
        handleReadyState(http, request['callback']);
        handleTimeOut(http, request['retry'], request['timeout']);
        if (request['method'] == "POST") {  
            http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            http.send(request['params']);
        }
        else {
            http.send(null);
        }
        return http;
    };
})();


Function.prototype.method = function(name, fn) 
{
	this.prototype[name] = fn;
	return this;
};

if ( !Array.prototype.forEach ) 
{ 
  	Array.method('forEach', function(fn, thisObj) 
	{
    	var scope = thisObj || window;
    	for ( var i = 0, len = this.length; i < len; ++i ) 
		{
      		fn.call(scope, this[i], i, this);
    	}
  	});
}

if ( !Array.prototype.filter ) 
{
  	Array.method('filter', function(fn, thisObj) 
	{
    	var scope = thisObj || window;
    	var a = [];
    	for ( var i = 0, len = this.length; i < len; ++i ) 
		{
      		if ( !fn.call(scope, this[i], i, this) ) 
			{
        		continue;
      		}
      		a.push(this[i]);
    	}
    	return a;
  	});
}


window.DED = window.DED || {};
DED.util = DED.util || {};
DED.util.Observer = function() { this.fns = [];}
DED.util.Observer.prototype = 
{
  	subscribe: function(fn) 
	{
    	this.fns.push(fn);
  	},
  	unsubscribe: function(fn) 
	{
    	this.fns = this.fns.filter(
      		function(el) 
			{
        		if ( el !== fn ) 
				{
          			return el;
        		}
      		}
    	);
  	},
  	fire: function(o) 
	{
    	this.fns.forEach(
      		function(el) 
			{
        		el(o);
      		}
    	);
  	}
};


DED.Queue = function() 
{
	// Queued requests.
	this.queue = [];
  
	// Observable Objects that can notify the client of interesting moments
	// on each DED.Queue instance.
	this.onComplete = new DED.util.Observer;
	this.onFailure = new DED.util.Observer;
	this.onFlush = new DED.util.Observer;
	  
	// Core properties that set up a frontend queueing system.
	this.retryCount = 3;
	this.currentRetry = 0;
	this.paused = false;
	this.timeout = 5000;
	this.conn = {};
	this.timer = {};
};

DED.Queue.method('flush', function(_A) 
{
	if (!this.queue.length > 0) {return;}
	if (this.paused) 
	{
		this.paused = false;
		return;
	}
	var that = this;
	/*this.currentRetry++;
	var abort = function() 
				{
					that.conn.abort();
					if (that.currentRetry == that.retryCount) 
					{
						that.queue.shift();
						that.onFailure.fire();
						that.currentRetry = 0;
					} 
					else 
					{
						that.flush();
					}
				};
	this.timer = window.setTimeout(abort, this.timeout);
	var _callback = function(o) 
				{
					window.clearTimeout(that.timer);
					that.currentRetry = 0;
					that.queue.shift();
					that.onFlush.fire(o.responseText);	
					if (callback){callback(o);}
					if (that.queue.length == 0) 
					{
						that.onComplete.fire();
						return;
					}
					// recursive call to flush
					that.flush();
				};*/
	//提交最近一个没有被提交过的请求
	var _commit = function()
				{						
					for (var i = 0; i < that.queue.length; i=i+1)
					{
						if(that.queue[i]['flushed']!=true)
						{							
							that.conn = asyncRequest(that,that.queue[i]);
							that.queue[i]['flushed'] = true;
							if(!_A)break;
						}
					}
				}
	_commit();				
}).
method('setRetryCount', function(count) 
{
	this.retryCount = count;
}).
method('setTimeout', function(time) 
{
	this.timeout = time;
}).
method('add', function(o) 
{
	this.queue.push(o);
	//alert(o['uri']);
}).
method('pause', function() 
{
	this.paused = true;
}).
method('dequeue', function() 
{
	this.queue.pop();
}).
method('clear', function() 
{
	this.queue = [];
});
